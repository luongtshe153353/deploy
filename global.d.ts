// Use type safe message keys with `next-intl`
type Messages = typeof import('./src/locales/vi.json');
declare interface IntlMessages extends Messages {}
