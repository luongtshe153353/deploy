/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        BASEPATH: 'http://54.251.139.135:8080/',
    },
    eslint: {
        ignoreDuringBuilds: true,
    },
    plugins: {
        'postcss-preset-mantine': {},
        'postcss-simple-vars': {
            variables: {
                'mantine-breakpoint-xs': '36em',
                'mantine-breakpoint-sm': '48em',
                'mantine-breakpoint-md': '62em',
                'mantine-breakpoint-lg': '75em',
                'mantine-breakpoint-xl': '88em',
            },
        },
    },
    images: {
        domains: ['img.tripi.vn'],
    },
}

module.exports = nextConfig
