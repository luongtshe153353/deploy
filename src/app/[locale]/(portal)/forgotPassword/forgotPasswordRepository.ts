import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_FORGOTPASSWORD_PREIX: string = 'registered-users/';

class ForgotPasswordRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_FORGOTPASSWORD_PREIX;
    }

    public fotgotPassword = (payload?: any) =>
        this.loginPost(kebabCase('forgotPassword'), payload).then((result) => {
            return result;
        });
}

export const forgotPasswordRepository = new ForgotPasswordRepository();
