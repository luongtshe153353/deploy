"use client"
import React from 'react';
import { Button, Form, Input } from 'antd';
import { useRouter } from 'next/navigation';
import { TravelBooContext } from '../layout';
import { useTranslations } from 'next-intl';
import { forgotPasswordRepository } from './forgotPasswordRepository';

const ForgotPasswordForm: React.FC = () => {

    const { openNotification } = React.useContext(TravelBooContext)
    const router = useRouter()
    const translate = useTranslations('General.notification.login')

    const onFinish = React.useCallback(async (values: any) => {

        // Kiểm tra định dạng email
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(values.email)) {
            openNotification('error', 'Error', 'Invalid email format');
            return;
        }

        const request = {
            email: values.email
        }

        forgotPasswordRepository?.fotgotPassword(request).then((resp: any) => {
            if (resp == undefined) {
                openNotification('success', "Thành công", "Vui lòng kiểm tra email")
            } else {
                openNotification('error', "Thất bại", "Xin vui lòng thử lại")
            }
        })

    }, [openNotification, router, translate]);

    return (
        <Form onFinish={onFinish}
        >
            <div className="wrap-input100 validate-input" >
                <span className="label-input100">Email</span>
                <Form.Item name="email" noStyle>
                    <input className="input100" type="email" placeholder="Nhập email" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf15a;"></span>
            </div>

            <div className="text-right p-t-8 p-b-31">

            </div>
            <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn"></div>
                    <Form.Item noStyle>
                        <Button type="primary" htmlType="submit" className="login100-form-btn">
                            Quên mật khẩu
                        </Button>
                    </Form.Item>
                </div>
            </div>

        </Form >
    );
};

export default ForgotPasswordForm;
