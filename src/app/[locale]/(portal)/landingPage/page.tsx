'use client';
import { PermissionContext } from '@/app/permision';
import { Button } from 'antd';
import { useRouter } from 'next/navigation';
import React from 'react';

export default function LandingPage() {
    const router = useRouter()

    const { permission } = React.useContext(PermissionContext)

    console.log("permission: ", permission)
    return (
        <div className='w-100 d-flex flex-row justify-content-center align-items-center'>
            <Button type="primary" size={'large'} onClick={() => router.push('/management')}>
                Management
            </Button>

            <Button type="primary" size={'large'} onClick={() => router.push('/product')}>
                Product
            </Button>
        </div>
    )
}
