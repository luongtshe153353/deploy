'use client';
import type { NotificationPlacement } from 'antd/es/notification/interface';
import { notification } from 'antd';
import React from 'react';

type NotificationType = 'success' | 'info' | 'warning' | 'error';


type ContextType = {
    contextValue: { role: string }
    updateContextValue: (value: any) => void
    openNotification: (type: NotificationType, message?: string, description?: string, placement?: NotificationPlacement) => void
}
export const TravelBooContext = React.createContext<ContextType>({
    contextValue: { role: 'daddyAdmin' }, updateContextValue: (value: any) => { }, openNotification(type, message, description, placement) {
    },
});


const ManagementLayout = ({ children }: { children: React.ReactNode }) => {

    const [contextValue, setContextValue] = React.useState({ role: 'daddyAdmin' });

    const updateContextValue = React.useCallback((value: any) => {
        setContextValue(value)
    }, [])
    const [api, contextHolder] = notification.useNotification();

    const openNotification = (type: NotificationType, message?: string, description?: string, placement?: NotificationPlacement) => {
        api[type]({
            message: message ? `${message}` : `Notification ${placement}`,
            description: description ? `${description}` : <TravelBooContext.Consumer>{({ contextValue }) => `Hello, ${contextValue?.role}!`}</TravelBooContext.Consumer>,
            placement: placement ? placement : 'bottomRight',
        });
    };

    return (
        <TravelBooContext.Provider value={{ contextValue, updateContextValue, openNotification }}>
            {contextHolder}
            {children}
        </TravelBooContext.Provider>
    );
};

export default ManagementLayout;
