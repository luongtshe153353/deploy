import { Repository } from "@/config/repository";
import { kebabCase } from "lodash";


export const API_BUSINESSADMIN_PREIX: string = '';

class LoginRepository extends Repository{
    constructor() {
        super();
        this.BasePath = API_BUSINESSADMIN_PREIX;
    }

    public login = (payload?: any) =>
        this.loginPost(kebabCase('login'), payload).then((result) => {
            return result
        });
}


export const loginRepository =  new LoginRepository()