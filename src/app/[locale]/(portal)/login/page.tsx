"use client"
import React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import { loginRepository } from './loginRepository';
import { setCookie } from 'cookies-next';
import { useRouter } from 'next/navigation';
import { TravelBooContext } from '../layout';
import { useTranslations } from 'next-intl';
import { logIn, logOut } from '@/redux/features/auth-slice';
import { useDispatch } from 'react-redux';
import { AppDispatch, useAppSelector } from '@/redux/store';
import Link from 'next/link';



const LoginForm: React.FC = () => {

    const { openNotification } = React.useContext(TravelBooContext)
    const router = useRouter()
    const translate = useTranslations('General.notification.login')
    const dispatch = useDispatch<AppDispatch>()

    const username = useAppSelector((state) => state.authReducer.value.userName)

    const onFinish = React.useCallback(async (values: any) => {
        const request = {
            userName: values.username,
            passWord: values.password
        }

        const p = await fetch('/api/auth/login', {
            method: 'POST',
            cache: 'no-cache',
            // credentials: "same-origin",
            headers: {
                Accept: '*/*',
                'Content-Type': 'application/json',
                'Accept-Encoding': 'gzip, deflate, br',
                Connection: 'keep-alive',
            },
            body: JSON.stringify(request),
        })

        // if (p) {
        //     router.replace('/management')
        // }
        loginRepository?.login(request).then((resp: any) => {
            if (resp?.code === 200) {
                setCookie('token', resp?.data?.token)
                localStorage.setItem('currentUserInfo', JSON.stringify(resp?.data))
                openNotification('success', translate('success'))
                // router.replace('/landingPage')
                dispatch(logIn(request.userName))
            } else {
                openNotification('error', translate('error'), translate('errorDescription'))
            }
        })
    }, [openNotification, router, translate]);

    return (
        <Form name="normal_login" onFinish={onFinish}
        >
            <div className="wrap-input100 validate-input m-b-23" >
                <span className="label-input100">Tên đăng nhập</span>
                <Form.Item name="username" noStyle>
                    <input className="input100" type="text" placeholder="Nhập tên đăng nhập" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf206;"></span>
            </div>

            <div className="wrap-input100 validate-input" >
                <span className="label-input100">Mật khẩu</span>
                <Form.Item name="password" noStyle>
                    <input className="input100" type="password" placeholder="Nhập mật khẩu" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf190;"></span>
            </div>

            <div className="text-right p-t-8 p-b-31">
                <Link href="forgotPassword">
                    Quên mật khẩu?
                </Link>
            </div>
            <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn"></div>
                    <Form.Item noStyle>
                        <Button type="primary" htmlType="submit" className="login100-form-btn">
                            Đăng nhập
                        </Button>
                    </Form.Item>
                </div>
            </div>

        </Form >
    );
};

export default LoginForm;
