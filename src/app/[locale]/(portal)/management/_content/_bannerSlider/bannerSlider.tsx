'use client';
import React from 'react';
import { Badge, Button, Col, Row, Table } from 'antd';
import { Input } from 'antd';
import BannerSliderPreview from './preview/bannerSliderPreview';
import { useTranslations } from 'next-intl';
import { FilterOutlined } from '@ant-design/icons';
import BannerSliderDetail from './detail/bannerSliderDetail';
import AdvanceFilter from './advanceFilter/advanceFilter';
import useBannerSliderHook from './bannerSliderHook';
import './bannerSlider.scss';

const { Search } = Input;

const BannerSliderContent = () => {
    const {
        columns,
        data,
        loading,
        rowSelection,
        hasSelected,
        start,
        // Search
        onSearch,
        //Drawer
        openPreview,
        setOpenPreview,
        openDetail,
        setOpenDetail,
        idSelected,
    } = useBannerSliderHook();

    const translate = useTranslations('Management.Menu');

    const [openFilter, setOpenFilter] = React.useState<boolean>(false);

    React.useEffect(() => {}, []);

    return (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="bannerSlider m-0 p-0 w-100 h-100 overflow-auto">
            <Col span={24} className="d-flex flex-row mb-5 justify-content-between">
                <h2>{translate('bannerSlider')}</h2>

                <BannerSliderDetail openDetail={openDetail} setOpenDetail={setOpenDetail} idSelected={idSelected} />
            </Col>

            <Col span={16} offset={3} className="d-flex flex-row">
                <Search
                    placeholder="Tìm kiếm"
                    allowClear
                    onSearch={onSearch}
                    style={{ width: '100%' }}
                    addonAfter={
                        <Badge count={4}>
                            <Button
                                className="ant-btn-icon-only ant-input-filter-button"
                                onClick={() => setOpenFilter(true)}
                            >
                                <span className="ant-btn-icon">
                                    <FilterOutlined />
                                </span>
                            </Button>{' '}
                        </Badge>
                    }
                />
            </Col>

            <Col span={24} className="d-flex flex-column">
                <div className="d-flex flex-row mb-2">
                    <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                        Reload
                    </Button>
                </div>

                <Table
                    className="flex-fill"
                    size="small"
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={data}
                    // loading={loading}
                    scroll={{ x: 'auto', y: 'auto' }}
                    bordered
                />
            </Col>

            <BannerSliderPreview openPreview={openPreview} setOpenPreview={setOpenPreview} idSelected={idSelected} />

            <AdvanceFilter openFilter={openFilter} setOpenFilter={setOpenFilter} />
        </Row>
    );
};

export default BannerSliderContent;
