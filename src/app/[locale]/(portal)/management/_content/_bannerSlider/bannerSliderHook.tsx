import React from 'react';
import type { ColumnsType } from 'antd/es/table';
import { useTranslations } from 'next-intl';
import { Dropdown, Image, MenuProps, Popconfirm, Space, Switch, Tooltip, Typography, message } from 'antd';
import { DownOutlined } from '@ant-design/icons';

interface DataType {
    key: React.Key;
    id: number | string;
    image: string;
    type: string;
    createdBy: string;
    createdDate: string;
    status: string;
}

const useBannerSliderHook: any = () => {
    const [selectedRowKeys, setSelectedRowKeys] = React.useState<React.Key[]>([]);
    const [loading, setLoading] = React.useState(false);
    const translate = useTranslations('Management.BannerSlider.listScreen');

    const data: DataType[] = [];
    for (let i = 0; i < 46; i++) {
        data.push({
            key: i,
            id: '00' + i,
            image: "",
            type: `Happy green day ${i}`,
            createdBy: 'Johnson' + i,
            createdDate: '12/05/2023',
            status: i % 2 == 0 ? 'Đang hoạt động' : 'Hết hiệu lực',
        });
    }

    let switchStateContainer: any = {};

    data.map((item) => (switchStateContainer[`switch${item.id}`] = item.status));

    const [switchStates, setSwitchStates] = React.useState(switchStateContainer);

    const handleSwitchChange = (switchName: string) => {
        // Xác nhận trước khi thay đổi trạng thái của Switch
        const onConfirm = () => {
            setSwitchStates((prevState: any) => ({
                ...prevState,
                [switchName]: !prevState[switchName],
            }));
            message.success(`Đã thay đổi trạng thái`);
        };

        // Hủy bỏ thay đổi
        const onCancel = () => {
            message.info(`Đã hủy thay đổi trạng thái`);
        };

        // Hiển thị xác nhận
        const confirmContent = `Bạn có chắc chắn muốn thay đổi trạng thái?`;

        return (
            <Popconfirm
                title={confirmContent}
                onConfirm={onConfirm}
                onCancel={onCancel}
                okText="Xác nhận"
                cancelText="Hủy bỏ"
            >
                <Switch size="small" checked={switchStates[switchName]} />
            </Popconfirm>
        );
    };

    const start = () => {
        setLoading(true);
        // ajax request after empty completing
        setTimeout(() => {
            setSelectedRowKeys([]);
            setLoading(false);
        }, 1000);
    };

    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        setSelectedRowKeys(newSelectedRowKeys);
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
        columnWidth: 40,
    };
    const hasSelected = selectedRowKeys.length > 0;

    // Search
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const onSearch = (value: string) => { };

    // Drawer
    const [openDetail, setOpenDetail] = React.useState<any>({ isDetail: false, isOpen: false });
    const [openPreview, setOpenPreview] = React.useState<boolean>(false);
    const [idSelected, setIdSelected] = React.useState<number | string>(0);

    // Table

    const Action = React.useCallback((id: number | string) => {
        const items: MenuProps['items'] = [
            {
                key: '1',
                label: translate('preview'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenPreview(true);
                },
            },
            {
                key: '2',
                label: translate('update'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenDetail({ isDetail: true, isOpen: true });
                },
            },
        ];

        return (
            <Dropdown
                menu={{
                    items,
                    selectable: true,
                    defaultSelectedKeys: undefined,
                }}
                trigger={['click']}
            >
                <Typography.Link>
                    <Space>
                        {translate('selectable')}
                        <DownOutlined />
                    </Space>
                </Typography.Link>
            </Dropdown>
        );
    }, []);

    const columns: ColumnsType<DataType> = [
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('id')}</div>;
            },
            key: 'id',
            dataIndex: 'id',
            ellipsis: true,
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('image')}</div>;
            },
            key: 'image',
            dataIndex: 'image',
            ellipsis: true,
            width: 200,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Image
                        width={200}
                        height={112.5}
                        src="https://bootravel.s3.ap-southeast-1.amazonaws.com/imagePromotion/1698257092308_image.png"
                    />
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('type')}</div>;
            },
            key: 'type',
            dataIndex: 'type',
            ellipsis: true,
            width: 150,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('createdBy')}</div>;
            },
            key: 'createdBy',
            dataIndex: 'createdBy',
            ellipsis: true,
            width: 150,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('createdDate')}</div>;
            },
            key: 'createdDate',
            dataIndex: 'createdDate',
            ellipsis: true,
            width: 150,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('status')}</div>;
            },
            key: 'status',
            dataIndex: 'status',
            ellipsis: true,
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span style={params[0] == 'Đang hoạt động' ? { color: 'green' } : { color: 'red' }}>
                            {params[0]}
                        </span>
                    </Tooltip>
                );
            },
        },
        {
            title: '',
            key: 'Actions',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return Action(params[1]?.id);
            },
        },
        {
            title: '',
            key: 'toggleStatus',
            width: 50,
            render: (...params: [string, DataType, number]) => {
                return <div className="text-center">{handleSwitchChange(`switch${params[1].id}`)}</div>;
            },
        },
    ];


    return {
        columns,
        data,
        loading,
        rowSelection,
        hasSelected,
        selectedRowKeys,
        start,
        onSearch,
        openPreview,
        setOpenPreview,
        openDetail,
        setOpenDetail,
        idSelected,
    };
};

export default useBannerSliderHook;
