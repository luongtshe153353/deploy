'use client';
import React, { useState } from 'react';
import { ApartmentOutlined, HomeOutlined, InboxOutlined, LinkOutlined, PlusOutlined, StopOutlined, UserOutlined } from '@ant-design/icons';
import {
    Button,
    Col,
    Drawer,
    Form,
    Input,
    Radio,
    RadioChangeEvent,
    Row,
    Select,
    SelectProps,
    Space,
    Upload,
} from 'antd';
import { useTranslations } from 'next-intl';
import { Option } from 'antd/es/mentions';

interface BannerSliderDetailProps {
    idSelected?: number | string;
    openDetail: any;
    setOpenDetail: (b: any) => void;
}

const BannerSliderDetail = (props: BannerSliderDetailProps): React.ReactNode => {
    const { idSelected, openDetail, setOpenDetail } = props;

    const listScreen = useTranslations('Management.BannerSlider.listScreen');
    const translate = useTranslations(
        openDetail?.isDetail ? 'Management.BannerSlider.detailScreen' : 'Management.BannerSlider.addScreen',
    );

    const showDrawer = () => {
        setOpenDetail((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = () => {
        setOpenDetail({
            isDetail: false,
            isOpen: false,
        });
    };

    var data: any;

    const onFinish = (values: any) => {
        data = {

        };
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const handleSubmit = () => { };

    const [fileList, setFileList] = useState([]);

    const onChangeFile = ({ file, fileList }: { file: any; fileList: any }) => {
        setFileList(fileList);
    };

    const [valueAction, setValueAction] = useState("None");

    const handleChangeAction = (value: any) => {
        setValueAction(value)
    };

    const [valueCategory, setValueCategory] = useState("None");

    const handleChangeCategory = (value: any) => {
        setValueCategory(value)
    };

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1000}
                onClose={onClose}
                open={openDetail?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button htmlType="submit" form="myForm" onClick={handleSubmit} type="primary">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Form
                    id="myForm"
                    labelCol={{ span: 6 }}
                    layout="horizontal"
                    hideRequiredMark
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={12}>
                            <Form.Item name="title" label="Tiêu đề" colon={false}
                                rules={[{ required: true, message: 'Vui lòng nhập tiêu đề' }]}>
                                <Input placeholder='Nhập tiêu đề'></Input>
                            </Form.Item>
                            <Form.Item name="action" label="Action" colon={false}>
                                <Select
                                    defaultValue="None"
                                    value={valueAction}
                                    onChange={handleChangeAction}
                                    options={[
                                        {
                                            value: "None",
                                            label: (
                                                <>
                                                    <StopOutlined />{" "}None
                                                </>
                                            ),
                                        },
                                        {
                                            value: "URL", label: (
                                                <>
                                                    <LinkOutlined />{" "}URL
                                                </>
                                            ),
                                        },
                                        {
                                            value: "Hotel", label: (
                                                <>
                                                    <HomeOutlined />{" "}Khách sạn
                                                </>
                                            ),
                                        },
                                        {
                                            value: "Category", label: (
                                                <>
                                                    <ApartmentOutlined />{" "}Danh mục
                                                </>
                                            ),
                                        },
                                    ]}
                                >
                                </Select>
                            </Form.Item>
                            {
                                valueAction === "None" ? null : null
                            }
                            {
                                valueAction === "URL" ? (
                                    <Form.Item name="link" label="Link" colon={false}>
                                        <Input prefix={<LinkOutlined />} width={300} />
                                    </Form.Item>
                                ) : null
                            }
                            {
                                valueAction === "Hotel" ? (
                                    <Form.Item name="hotel" label="Khách sạn">

                                    </Form.Item>
                                ) : null
                            }
                            {
                                valueAction === "Category" ? (
                                    <Form.Item name="category" label="Danh mục" colon={false}>
                                        <Select
                                            defaultValue="mostDiscount"
                                            value={valueCategory}
                                            onChange={handleChangeCategory}
                                            options={[
                                                {
                                                    value: "mostDiscount",
                                                    label: "Ưu đãi tốt nhất"
                                                },
                                                {
                                                    value: "mostStar",
                                                    label: "Chất lượng tốt nhất"
                                                }
                                            ]}
                                        >
                                        </Select>
                                    </Form.Item>
                                ) : null
                            }

                        </Col>
                        <Col span={8} offset={3}>
                            <Form.Item
                                name="image"
                                colon={false}
                                label={translate('info.image')}
                                rules={[{ required: true, message: 'Vui lòng tải lên hình ảnh' }]}
                            >
                                <Upload
                                    action="/"
                                    beforeUpload={() => false}
                                    onChange={onChangeFile}
                                    fileList={fileList}
                                    accept="image/png, image/jpeg"
                                    listType="picture-card"
                                    maxCount={1}
                                >
                                    <div >
                                        <PlusOutlined />
                                        <div style={{ marginTop: 8 }}>Tải lên</div>
                                    </div>
                                </Upload>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
        </div>
    );
};

export default BannerSliderDetail;
