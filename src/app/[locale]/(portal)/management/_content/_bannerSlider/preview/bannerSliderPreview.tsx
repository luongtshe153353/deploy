'use client';
import React from 'react';
import dayjs from 'dayjs';
import { Button, Col, DatePicker, Divider, Drawer, Form, Image, Input, Radio, Row, Space } from 'antd';
import { useTranslations } from 'next-intl';
import { Typography } from 'antd';

const { Text } = Typography;

interface BannerSliderPreviewProps {
    idSelected?: number | string;
    openPreview?: boolean;
    setOpenPreview: (b: boolean) => void;
}

const BannerSliderPreview = (props: BannerSliderPreviewProps): React.ReactNode => {
    const { openPreview, setOpenPreview } = props;
    const translate = useTranslations('Management.BannerSlider.previewScreen');
    const { RangePicker } = DatePicker;

    return (
        <div>
            <Drawer
                title={translate('title')}
                onClose={() => setOpenPreview(false)}
                open={openPreview}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                width={1230}
                extra={
                    <Space>
                        <Button onClick={() => setOpenPreview(false)}>{translate('button.close')}</Button>
                    </Space>
                }
            >
                <Form labelCol={{ span: 5 }} layout="horizontal" hideRequiredMark>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                            <Divider orientation="left" orientationMargin={5}>
                                {translate('info.title')}
                            </Divider>
                            <Col>
                                <Form.Item name="code" label={translate('info.code')}>
                                    <Text mark>ABC123</Text>
                                </Form.Item>
                                <Form.Item name="name" label={translate('info.name')}>
                                    <Text mark>Happy green day</Text>
                                </Form.Item>
                                <Form.Item name="image" label={translate('info.image')} valuePropName="fileList">
                                    <Image
                                        style={{ borderRadius: '15px' }}
                                        width={'100%'}
                                        preview={false}
                                        src="https://images.unsplash.com/photo-1575936123452-b67c3203c357?auto=format&fit=crop&q=80&w=1000&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D"
                                    />
                                </Form.Item>

                                <Form.Item name="description" label={translate('info.description')}>
                                    <Text mark>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industrys standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries
                                    </Text>
                                </Form.Item>
                                <Form.Item name="time" label={translate('info.date')}>
                                    <RangePicker
                                        disabled={true}
                                        defaultValue={[
                                            dayjs('25/11/2023', 'dd/MM/yyyy'),
                                            dayjs('25/12/2023', 'dd/MM/yyyy'),
                                        ]}
                                    />
                                </Form.Item>
                            </Col>
                        </Col>
                        <Col span={16}>
                            <Col>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('mandatoryCondition.title')}
                                </Divider>
                                <Col>
                                    <p className="fw-bolder">{translate('mandatoryCondition.subtitle1')}</p>
                                    <Radio.Group value={1}>
                                        <Space direction="vertical">
                                            <Radio value={1}>
                                                <span style={{ width: 120, display: 'inline-block' }}>
                                                    Tỷ lệ phần trăm
                                                </span>

                                                {1 === 1 ? (
                                                    <>
                                                        <Input
                                                            disabled
                                                            defaultValue="20%"
                                                            placeholder="Phần trăm giảm(%)"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                        />
                                                        <Input
                                                            disabled
                                                            defaultValue="50.000 VNĐ"
                                                            className="ml-5"
                                                            placeholder="Giảm tối đa(VND)"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                        />
                                                    </>
                                                ) : (
                                                    <>
                                                        <Input
                                                            disabled
                                                            placeholder="Phần trăm giảm(%)"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                        />
                                                        <Input
                                                            disabled
                                                            className="ml-5"
                                                            placeholder="Giảm tối đa(VND)"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                        />
                                                    </>
                                                )}
                                            </Radio>
                                            <Radio value={2} disabled>
                                                <span style={{ width: 120, display: 'inline-block' }}>
                                                    Số tiền cố định
                                                </span>
                                                {false ? (
                                                    <Input
                                                        placeholder="Số tiền tối đa"
                                                        style={{ width: 410, marginLeft: 10 }}
                                                    />
                                                ) : (
                                                    <Input
                                                        placeholder="Số tiền tối đa"
                                                        disabled
                                                        style={{ width: 410, marginLeft: 10 }}
                                                    />
                                                )}
                                            </Radio>
                                        </Space>
                                    </Radio.Group>
                                    <p className="fw-bolder mt-5">{translate('mandatoryCondition.subtitle2')}</p>

                                    <Radio.Group value={2}>
                                        <Space direction="vertical">
                                            <Radio value={1} disabled>
                                                <span style={{ width: 200, display: 'inline-block' }}>
                                                    Không giới hạn số lượt sử dụng
                                                </span>
                                            </Radio>
                                            <Radio value={2}>
                                                <span style={{ width: 200, display: 'inline-block' }}>
                                                    Giới hạn số lượt sử dụng
                                                </span>
                                                {2 === 2 ? (
                                                    <Input
                                                        disabled
                                                        defaultValue="1"
                                                        placeholder="Số lượt sử dụng"
                                                        style={{ width: 330, marginLeft: 10 }}
                                                    />
                                                ) : (
                                                    <Input
                                                        disabled
                                                        placeholder="Số lượt sử dụng"
                                                        style={{ width: 330, marginLeft: 10 }}
                                                    />
                                                )}
                                            </Radio>
                                        </Space>
                                    </Radio.Group>
                                </Col>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
        </div>
    );
};

export default BannerSliderPreview;
