import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_BANNER_SLIDER_PREIX: string = 'travel-boo/management/banner-slider';

export class BannerSliderRepository extends Repository {
    constructor() {
        super();
    }

    public login = (payload?: any): any => {
        return this.post(kebabCase('login'), payload);
    };

    public list = (payload?: any): any => {
        return this.post(kebabCase('list'), payload);
    };
}

export const bannerSliderRepository = new BannerSliderRepository();
