import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_BUSINESSADMIN_PREIX: string = 'business-admin/';

export class BusinessAdminRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_BUSINESSADMIN_PREIX;
    }

    public searchBusinessAdmin = (payload?: any) => this.post(kebabCase('searchBusinessAdmin'), payload);

    public createBusinessAdmin = (payload?: any) =>
        this.post(kebabCase('createBusinessAdmin'), payload).then((result) => {
            return result;
        });

    public getBusinessAdmin = (payload?: any) =>
        this.get('get-ba/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const businessAdminRepository = new BusinessAdminRepository();
