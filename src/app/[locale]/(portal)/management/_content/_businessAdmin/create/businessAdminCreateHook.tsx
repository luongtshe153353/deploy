import { Repository } from "@/config/repository"
import React from "react"
import { FormInstance } from "antd"
import { TravelBooContext } from "@/app/[locale]/(portal)/layout"
import { useTranslations } from "next-intl"
import { businessAdminRepository } from "../businessAdminRepository"

interface useBusinessAdminCreateHookProps {
    form?: FormInstance<any>
}

const useBusinessAdminCreateHook = (props: useBusinessAdminCreateHookProps) => {

    const { form } = props
    const { openNotification } = React.useContext(TravelBooContext)

    const translate = useTranslations('General.notification.create')

    const onFinish = React.useCallback((values: any) => {
        const payload = values

        return businessAdminRepository.createBusinessAdmin(payload).then((result: any) => {
            if (result?.code === 200) {
                openNotification('success', translate('success'))
            } else {
                openNotification('error', translate('error'), translate('errorDescription'))
            }
        })
    }, [openNotification, translate])

    // handle province
    const [province, setProvince] = React.useState([])
    const handleProvince = React.useCallback(() => {
        new Repository().listProvince().then((result) => {
            if (Array.isArray(result?.listData)) {
                setProvince(result?.listData)
            }
        })
    }, [])

    // handle distrcit
    const [district, setDistrict] = React.useState([])
    const handleDistrict = React.useCallback(() => {
        new Repository().listDistrict(form?.getFieldValue('provinceId')).then((result) => {
            if (Array.isArray(result?.listData)) {

                setDistrict(result?.listData)
            }
        })
    }, [])

    // handle ward
    const [ward, setward] = React.useState([])
    const handleWard = React.useCallback(() => {
        new Repository().listWard(form?.getFieldValue('districtId')).then((result) => {
            if (Array.isArray(result?.listData)) {

                setward(result?.listData)
            }
        })
    }, [])

    return {
        onFinish,
        handleProvince,
        province,
        handleDistrict,
        district,
        handleWard,
        ward
    }
}

export default useBusinessAdminCreateHook