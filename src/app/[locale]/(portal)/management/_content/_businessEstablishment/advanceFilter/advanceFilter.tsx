'use client';
import React from 'react';
import { Col, DatePicker, Form, Input, Modal, Rate, Row, Select } from 'antd';
import { useTranslations } from 'next-intl';
import { BulbTwoTone } from '@ant-design/icons';
import { GenderFemale, GenderMale } from '@carbon/icons-react';

const { Option } = Select;

interface AdvanceFilterProps {
    openFilter: boolean;
    setOpenFilter: (b: boolean) => void;
}

const AdvanceFilter = (props: AdvanceFilterProps): React.ReactNode => {
    const { openFilter, setOpenFilter } = props;

    const translate = useTranslations('Management.businessEstablishment.listScreen.advanceFilter');

    return (
        <>
            <Modal
                title={translate('title')}
                centered
                open={openFilter}
                onOk={() => setOpenFilter(false)}
                onCancel={() => setOpenFilter(false)}
                width={1000}
            >
                <Form layout="vertical">
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={3}>
                            <Form.Item name="id" label={translate('id')}>
                                <Input placeholder="Nhập ID" />
                            </Form.Item>
                        </Col>

                        <Col span={7}>
                            <Form.Item labelAlign="left" name="name" label={translate('name')}>
                                <Input placeholder="Nhập tên cơ sở kinh doanh" />
                            </Form.Item>
                        </Col>

                        <Col span={7}>
                            <Form.Item name="gender" label={translate('nameBO')}>
                                <Select placeholder="Chọn quản lí cơ sở kinh doanh">
                                    <Option value="BO1">Quản lí 1</Option>
                                    <Option value="BO2">Quản lí 2</Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={7}>
                            <Form.Item name="status" label={translate('status')}>
                                <Select placeholder="Chọn trạng thái hoạt động">
                                    <Option value="xiao">
                                        <BulbTwoTone twoToneColor={'#34f231'} size={16} className="me-2" />
                                        Đang hoạt động
                                    </Option>
                                    <Option value="mao">
                                        <BulbTwoTone twoToneColor={'red'} size={16} className="me-2" />
                                        Dừng hoạt động
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item
                                name="taxCode"
                                label={translate('taxCode')}
                            >
                                <Input placeholder="Nhập mã số thuế" />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item
                                name="starRating"
                                label={translate('rate')}
                            >
                                <Rate />
                            </Form.Item>
                        </Col>

                        <Col span={8}>
                            <Form.Item labelAlign="right" name="province" label={translate('province')}>
                                <Select
                                    showSearch
                                    placeholder="Select a province"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'haNoi',
                                            label: 'Hà Nội',
                                        },
                                        {
                                            value: 'hoChiMinh',
                                            label: 'Hồ Chí Minh',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>

                        <Col span={8}>
                            <Form.Item labelAlign="right" name="district" label={translate('district')}>
                                <Select
                                    showSearch
                                    placeholder="Select a district"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'namTuLiem',
                                            label: 'Nam Từ Liêm',
                                        },
                                        {
                                            value: 'cauGiay',
                                            label: 'Cầu Giấy',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>

                        <Col span={8}>
                            <Form.Item labelAlign="right" name="ward" label={translate('ward')}>
                                <Select
                                    showSearch
                                    placeholder="Select a ward"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'ward1',
                                            label: 'Ward1',
                                        },
                                        {
                                            value: 'ward2',
                                            label: 'Ward2',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};

export default AdvanceFilter;
