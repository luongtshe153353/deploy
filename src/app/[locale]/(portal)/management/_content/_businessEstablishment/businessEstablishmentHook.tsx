import React, { useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
import { useTranslations } from 'next-intl';
import { Dropdown, MenuProps, Modal, Popconfirm, Rate, Space, Switch, Tag, Tooltip, Typography, message } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { businessEstablishmentRepository } from './businessEstablishmentRepository';

interface DataType {
    id: number | string;
    key: React.Key;
    name: string;
    nameBO: string;
    rate: number;
    taxCode: string;
    address: string;
    status: boolean;
}

const useBusinessEstablishmentHook: any = () => {
    const translate = useTranslations('Management.businessEstablishment.listScreen');

    const initialPayload = {
        "searchPaging": {
            "pageNum": 1,
            "pageSize": 10,
            "sortType": "ASC"
        },
        "searchParams": ""
    }

    const [payload, setPayload] = React.useState(initialPayload)
    const [resp, setResp] = React.useState<any>({})
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const [isReload, setIsReload] = React.useState<boolean>(false)

    React.useEffect(() => {
        const loadingList = new Promise((resolve, reject) => {

            setIsLoading(true)
            resolve(businessEstablishmentRepository.searchBusinessEstablishment(payload).then((result) => {
                setIsLoading(false)
                setResp(result)
                setIsReload(false)
            }))

            reject(setIsLoading(false))
        })

    }, [payload, isReload])

    const meta = React.useMemo(() => {
        if (resp?.meta) {
            return resp.meta
        }
    }, [resp])

    const data: DataType[] = React.useMemo(() => {
        if (resp?.data) {
            return resp.data
        }
    }, [resp])

    const onChangePagination = (page: number) => {
        setPayload((prev: any) => ({
            ...prev, searchPaging: {
                ...prev?.searchPaging,
                pageNum: page
            }
        }))
    }

    const [switchStates, setSwitchStates]: [any, any] = useState({});

    React.useEffect(() => {
        if (!isLoading && data) {

            const newSwitchStateContainer: any = {};
            data.forEach((item) => {
                newSwitchStateContainer[`switch${item.id}`] = item.status;
            });

            setSwitchStates(newSwitchStateContainer);
        }
    }, [isLoading, data]);

    const handleSwitchChange = (switchName: string, id: any, status: any) => {
        const onConfirm = () => {

            const newSwitchStates: any = { ...switchStates };
            newSwitchStates[switchName] = !status;
            setSwitchStates(newSwitchStates);

            const payload = {
                id: id,
                status: !status
            }

            businessEstablishmentRepository.updateStatus(payload);

            data.forEach(item => {
                if (item.id == id) {
                    item.status = !status;
                }
            })

            message.success('Đã thay đổi trạng thái');
        };

        const onCancel = () => {
            message.info(`Đã hủy thay đổi trạng thái`);
        };

        const confirmContent = `Bạn có chắc chắn muốn thay đổi trạng thái?`;

        return (
            <Popconfirm
                title={confirmContent}
                onConfirm={onConfirm}
                onCancel={onCancel}
                okText="Xác nhận"
                cancelText="Hủy bỏ"
            >
                <Switch size="small" checked={switchStates[switchName]} />
            </Popconfirm>
        );
    };

    const onSearch = (event: any) => {
        setPayload((prev: any) => ({ ...prev, searchParams: event?.target?.value }))
    }

    // Drawer
    const [openCreate, setOpenCreate] = React.useState<any>({ isCreate: false, isOpen: false });
    const [openPreview, setOpenPreview] = React.useState<boolean>(false);
    const [idSelected, setIdSelected] = React.useState<number | string>(0);

    // Table

    const Action = React.useCallback((id: number | string) => {
        const items: MenuProps['items'] = [
            {
                key: '1',
                label: translate('preview'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenPreview(true);
                },
            },
            // {
            //     key: '2',
            //     label: translate('update'),
            //     onClick: () => {
            //         setIdSelected(id);
            //         setOpenDetail({ isDetail: true, isOpen: true });
            //     },
            // },
        ];

        return (
            <Dropdown
                menu={{
                    items,
                    selectable: true,
                    defaultSelectedKeys: undefined,
                }}
                trigger={['click']}
            >
                <Typography.Link>
                    <Space>
                        {translate('selectable')}
                        <DownOutlined />
                    </Space>
                </Typography.Link>
            </Dropdown>
        );
    }, []);

    const columns: ColumnsType<DataType> = [
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('name')}</div>;
            },
            key: 'name',
            dataIndex: 'name',
            ellipsis: false,
            width: 200,
            align: 'left',
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('nameBO')}</div>;
            },
            key: 'nameBO',
            dataIndex: 'nameBO',
            ellipsis: true,
            width: 150,
            align: 'left',
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('rate')}</div>;
            },
            key: 'rate',
            dataIndex: 'rate',
            ellipsis: true,
            width: 75,
            render: (...params: [string, DataType, number]) => {
                return <Rate style={{ fontSize: 15 }} disabled defaultValue={+params[0]} />;
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('taxCode')}</div>;
            },
            key: 'taxCode',
            dataIndex: 'taxCode',
            ellipsis: false,
            width: 100,
            align: 'left',
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('address')}</div>;
            },
            key: 'address',
            dataIndex: 'address',
            ellipsis: true,
            width: 200,
            align: 'left',
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('status')}</div>;
            },
            key: 'status',
            dataIndex: 'status',
            ellipsis: true,
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return <div className="text-center">{handleSwitchChange(`switch${params[1].id}`, params[1].id, params[0])}</div>;
            },
        },
        {
            title: '',
            key: 'Actions',
            width: 65,
            render: (...params: [string, DataType, number]) => {
                return Action(params[1]?.id);
            },
        },
    ];

    return {
        columns,
        data,
        //loading,
        //selectedRowKeys,
        //start,
        onSearch,
        openPreview,
        setOpenPreview,
        openCreate,
        setOpenCreate,
        idSelected,
        isLoading,
        meta,
        onChangePagination,
        setIsReload
    };
};

export default useBusinessEstablishmentHook;
