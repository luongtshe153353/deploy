import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_BUSINESSESTABLISHMENT_PREIX: string = 'hotel/';

export class BusinessEstablishmentRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_BUSINESSESTABLISHMENT_PREIX;
    }

    public searchBusinessEstablishment = (payload?: any) => this.post(kebabCase('searchHotel'), payload);

    public createBusinessEstablishment = (payload?: any) =>
        this.post(kebabCase('createHotel'), payload).then((result) => {
            return result;
        });

    public getBusinessEstablishment = (payload?: any) =>
        this.get('get-hotel/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const businessEstablishmentRepository = new BusinessEstablishmentRepository();
