'use client';
import React, { useState } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import { LoadingOutlined, PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Drawer, Form, Input, Row, Select, Space, Rate, Checkbox, message, Upload, Spin } from 'antd';
import { useTranslations } from 'next-intl';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import 'react-quill/dist/quill.snow.css';
import TextArea from 'antd/es/input/TextArea';
import useValidate from '@/utilities/validate';
import useBusinessEstablishmentCreateHook from './businessEstablishmentCreateHook';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const { Option } = Select;

interface BusinessEstablishmentCreateProps {
    idSelected?: number | string;
    openCreate: any;
    setOpenCreate: (b: any) => void;
    setIsReload: (b: any) => void;
}

const BusinessEstablishmentCreate = (props: BusinessEstablishmentCreateProps): React.ReactNode => {
    const { idSelected, openCreate, setOpenCreate, setIsReload } = props;
    const {
        provinceRule,
        districtRule,
        wardRule,
        hotelNameRule,
        starRatingRule,
        boRule,
        taxCodeRule,
        descriptionRule,
        servicesRule,
        imagesRule,
    } = useValidate()

    const [form] = Form.useForm();

    const {
        onFinish,
        handleServicesRaw,
        servicesRaw,
        handleBo,
        bo,
        handleProvince,
        province,
        handleDistrict,
        district,
        handleWard,
        ward
    } = useBusinessEstablishmentCreateHook({ form })

    // const [checkedList, setCheckedList] = useState<CheckboxValueType[]>([]);
    const [valueContent, setValueContent] = useState('');
    const [note, setNote] = useState('');
    const [fileList, setFileList] = useState([]);

    const listScreen = useTranslations('Management.businessEstablishment.listScreen');
    const translate = useTranslations('Management.businessEstablishment.addScreen');

    const CheckboxGroup = Checkbox.Group;
    // const checkAll = services.length === checkedList.length;
    // const indeterminate = checkedList.length > 0 && checkedList.length < services.length;
    // const onChangeCustom = (list: CheckboxValueType[]) => {
    //     setCheckedList(list);
    // };
    // const onCheckAllChange = (e: CheckboxChangeEvent) => {
    //     const checkboxValues = services.map((option) => option.value);
    //     setCheckedList(e.target.checked ? checkboxValues : []);
    // };

    const showDrawer = () => {
        setOpenCreate((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    React.useEffect(() => {
        handleServicesRaw();
    }, [])

    const onClose = React.useCallback(() => {
        setOpenCreate({
            isDetail: false,
            isOpen: false,
        });
    }, [setOpenCreate]);

    const handleUpload = () => {
        if (fileList.length !== 3) {
            message.error('Vui lòng tải lên đủ 3 ảnh.');
            return;
        }
        // Thực hiện xử lý tải lên ở đây nếu có 3 ảnh
        // Nếu muốn thực hiện tải lên, bạn có thể gọi một hàm API hoặc làm gì đó với các file trong fileList.
    };

    const onChange = ({ file, fileList }: { file: any; fileList: any }) => {
        setFileList(fileList);
    };

    const handleTextareaChange = (event: any) => {
        setNote(event.target.value);
    };

    const handleSubmit = React.useCallback(() => {

        form.validateFields().then((values) => {
            onFinish(values).then(() => {
                setIsReload(true);
                onClose()
            });
        });

    }, [form, onClose, onFinish])

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={onClose}
                open={openCreate?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button onClick={handleSubmit} type="primary" htmlType="submit" className="login-form-button">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form
                            form={form}
                            layout="vertical"
                            hideRequiredMark
                        >
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="name"
                                        label={translate('info.hotelName')}
                                        rules={hotelNameRule}
                                    >
                                        <Input placeholder="Nhập tên cơ sở kinh doanh" />
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item
                                        name="star"
                                        label={translate('info.starRating')}
                                        rules={starRatingRule}
                                    >
                                        <Rate />
                                    </Form.Item>
                                </Col>
                                <Col span={10}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="businessOwner"
                                        label={translate('info.bo')}
                                        rules={boRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 quản lí"
                                            optionFilterProp="children"
                                            onClick={handleBo}
                                            options={bo?.length > 0 ? bo?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.fullName
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item
                                        name="taxCode"
                                        label={translate('info.taxCode')}
                                        rules={taxCodeRule}
                                    >
                                        <Input placeholder="Nhập mã số thuế" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    {/* <Form.Item name="images" label={translate('info.image')} valuePropName="fileList" rules={imagesRule}>
                                        <Upload
                                            action="/"
                                            beforeUpload={() => false}
                                            onChange={onChange}
                                            fileList={fileList}
                                            listType="picture-card"
                                            accept="image/png, image/jpeg"
                                            maxCount={3}
                                        >
                                            {fileList.length != 3 ? (
                                                <div>
                                                    <PlusOutlined />
                                                    <div style={{ marginTop: 8 }}>Thêm ảnh</div>
                                                </div>
                                            ) : (
                                                ''
                                            )}
                                        </Upload>
                                        <Button type="primary" onClick={handleUpload}>
                                            Kiểm tra và Tải lên
                                        </Button>
                                    </Form.Item> */}
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('location.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="provinceId"
                                        label={translate('location.province')}
                                        rules={provinceRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 tỉnh/thành phố"
                                            optionFilterProp="children"
                                            onClick={handleProvince}
                                            options={province?.length > 0 ? province?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="districtId"
                                        label={translate('location.district')}
                                        rules={districtRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 quận/huyện"
                                            optionFilterProp="children"
                                            onClick={handleDistrict}
                                            options={district?.length > 0 ? district?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('provinceId') ? false : true}
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="wardId"
                                        label={translate('location.ward')}
                                        rules={wardRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 phường/xã"
                                            optionFilterProp="children"
                                            onClick={handleWard}
                                            options={ward?.length > 0 ? ward?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('districtId') ? false : true}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="address1"
                                        label={translate('location.houseNumber')}
                                    >
                                        <Input placeholder="Nhập số nhà" />
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item
                                        name="address2"
                                        label={translate('location.alleyNumber')}
                                    >
                                        <Input placeholder="Nhập số ngõ" />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24}>
                                    {/* <Checkbox
                                        indeterminate={indeterminate}
                                        onChange={onCheckAllChange}
                                        checked={checkAll}
                                    >
                                        Chọn tất cả
                                    </Checkbox>
                                    <Divider /> */}
                                    <Form.Item
                                        name="listHotelService"
                                        rules={servicesRule}
                                    >
                                        <CheckboxGroup
                                            name="services"
                                            // options={services}
                                            options={servicesRaw?.length > 0 ? servicesRaw?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: -1,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            // value={checkedList}
                                            // onChange={onChangeCustom}
                                            className="checkbox-group-custom"
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('description.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="description"
                                        rules={descriptionRule}
                                    >
                                        <ReactQuill theme="snow" value={valueContent} onChange={setValueContent} />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('note.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="note"
                                    >
                                        <TextArea name="note" value={note} onChange={handleTextareaChange} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default BusinessEstablishmentCreate;
