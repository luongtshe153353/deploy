'use client';
import React, { useState } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import { PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Drawer, Form, Input, Row, Select, Space, Rate, Checkbox, message, Upload } from 'antd';
import { useTranslations } from 'next-intl';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import 'react-quill/dist/quill.snow.css';
import TextArea from 'antd/es/input/TextArea';

interface BusinessEstablishmentDetailProps {
    idSelected?: number | string;
    openDetail: any;
    setOpenDetail: (b: any) => void;
}

const BusinessOwnerDetail = (props: BusinessEstablishmentDetailProps): React.ReactNode => {
    var plainOptions = [
        { label: 'Dọn phòng hàng ngày', value: 'donphong' },
        { label: 'Đưa đón đến sân bay', value: 'duadon' },
        { label: 'Giặt là', value: 'giat' },
        { label: 'Wifi riêng mỗi phòng', value: 'wifi' },
    ];

    const [checkedList, setCheckedList] = useState<CheckboxValueType[]>([]);
    const { idSelected, openDetail, setOpenDetail } = props;
    const [valueContent, setValueContent] = useState('');
    const [note, setNote] = useState('');
    const [fileList, setFileList] = useState([]);

    const listScreen = useTranslations('Management.businessEstablishment.listScreen');
    const translate = useTranslations(
        openDetail?.isDetail
            ? 'Management.businessEstablishment.detailScreen'
            : 'Management.businessEstablishment.addScreen',
    );

    const CheckboxGroup = Checkbox.Group;
    const checkAll = plainOptions.length === checkedList.length;
    const indeterminate = checkedList.length > 0 && checkedList.length < plainOptions.length;
    const onChangeCustom = (list: CheckboxValueType[]) => {
        setCheckedList(list);
    };
    const onCheckAllChange = (e: CheckboxChangeEvent) => {
        const checkboxValues = plainOptions.map((option) => option.value);
        setCheckedList(e.target.checked ? checkboxValues : []);
    };

    const showDrawer = () => {
        setOpenDetail((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = () => {
        setOpenDetail({
            isDetail: false,
            isOpen: false,
        });
    };

    const handleUpload = () => {
        if (fileList.length !== 3) {
            message.error('Vui lòng tải lên đủ 3 ảnh.');
            return;
        }
        // Thực hiện xử lý tải lên ở đây nếu có 3 ảnh
        // Nếu muốn thực hiện tải lên, bạn có thể gọi một hàm API hoặc làm gì đó với các file trong fileList.
    };

    const onChange = ({ file, fileList }: { file: any; fileList: any }) => {
        setFileList(fileList);
    };

    const handleTextareaChange = (event: any) => {
        setNote(event.target.value);
    };

    let data: any;
    const onFinish = (values: any) => {
        data = {
            hotelName: values.hotelName,
            starRating: values.starRating,
            bo: values.bo,
            taxCode: values.taxCode,
            images: fileList,
            province: values.province,
            district: values.district,
            ward: values.ward,
            houseNumber: values.houseNumber,
            alleyNumber: values.alleyNumber,
            services: checkedList,
            description: valueContent,
            note: note,
        };
        console.log(data);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={onClose}
                open={openDetail?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button htmlType="submit" form="myForm" onClick={onClose} type="primary">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form
                            id="myForm"
                            layout="vertical"
                            hideRequiredMark
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="hotelName"
                                        label={translate('info.hotelName')}
                                        rules={[{ required: true, message: 'Vui lòng nhập tên cơ sở kinh doanh' }]}
                                    >
                                        <Input placeholder="Nhập tên cơ sở kinh doanh" />
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item
                                        name="starRating"
                                        label={translate('info.starRating')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng chọn xếp hạng sao của cơ sở kinh doanh',
                                            },
                                        ]}
                                    >
                                        <Rate />
                                    </Form.Item>
                                </Col>
                                <Col span={10}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="bo"
                                        label={translate('info.bo')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng chọn 1 quản lí cho cơ sở kinh doanh',
                                            },
                                        ]}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 quản lí"
                                            optionFilterProp="children"
                                            options={[
                                                {
                                                    value: 'ba',
                                                    label: 'BA',
                                                },
                                                {
                                                    value: 'bo1',
                                                    label: 'BO1',
                                                },
                                                {
                                                    value: 'bo2',
                                                    label: 'BO2',
                                                },
                                            ]}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item
                                        name="taxCode"
                                        label={translate('info.taxCode')}
                                        rules={[{ required: true, message: 'Vui lòng nhập mã số thuế' }]}
                                    >
                                        <Input placeholder="Nhập mã số thuế" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <Form.Item name="images" label={translate('info.image')} valuePropName="fileList">
                                        <Upload
                                            action="/"
                                            beforeUpload={() => false}
                                            onChange={onChange}
                                            fileList={fileList}
                                            listType="picture-card"
                                            accept="image/png, image/jpeg"
                                            maxCount={3}
                                        >
                                            {fileList.length != 3 ? (
                                                <div>
                                                    <PlusOutlined />
                                                    <div style={{ marginTop: 8 }}>Thêm ảnh</div>
                                                </div>
                                            ) : (
                                                ''
                                            )}
                                        </Upload>
                                        <Button type="primary" onClick={handleUpload}>
                                            Kiểm tra và Tải lên
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('location.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="province"
                                        label={translate('location.province')}
                                        rules={[{ required: true, message: 'Vui lòng chọn 1 tỉnh/thành phố' }]}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 tỉnh/thành phố"
                                            optionFilterProp="children"
                                            options={[
                                                {
                                                    value: 'haNoi',
                                                    label: 'Hà Nội',
                                                },
                                                {
                                                    value: 'hoChiMinh',
                                                    label: 'Hồ Chí Minh',
                                                },
                                            ]}
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="district"
                                        label={translate('location.district')}
                                        rules={[{ required: true, message: 'Vui lòng chọn 1 quận/huyện' }]}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 quận/huyện"
                                            optionFilterProp="children"
                                            options={[
                                                {
                                                    value: 'namTuLiem',
                                                    label: 'Nam Từ Liêm',
                                                },
                                                {
                                                    value: 'cauGiay',
                                                    label: 'Cầu Giấy',
                                                },
                                            ]}
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="ward"
                                        label={translate('location.ward')}
                                        rules={[{ required: true, message: 'Vui lòng chọn 1 đường' }]}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 đường"
                                            optionFilterProp="children"
                                            options={[
                                                {
                                                    value: 'ward1',
                                                    label: 'Nguyễn Hoàng',
                                                },
                                                {
                                                    value: 'ward2',
                                                    label: 'Phú Mỹ',
                                                },
                                            ]}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="houseNumber"
                                        label={translate('location.houseNumber')}
                                        rules={[{ required: true, message: 'Vui lòng nhập số nhà' }]}
                                    >
                                        <Input placeholder="Nhập số nhà" />
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item
                                        name="alleyNumber"
                                        label={translate('location.alleyNumber')}
                                        rules={[{ required: true, message: 'Vui lòng nhập số ngõ' }]}
                                    >
                                        <Input placeholder="Nhập số ngõ" />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24}>
                                    <Checkbox
                                        indeterminate={indeterminate}
                                        onChange={onCheckAllChange}
                                        checked={checkAll}
                                    >
                                        Chọn tất cả
                                    </Checkbox>
                                    <Divider />
                                    <CheckboxGroup
                                        name="services"
                                        options={plainOptions}
                                        value={checkedList}
                                        onChange={onChangeCustom}
                                        className="checkbox-group-custom"
                                    />
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('description.title')}
                                </Divider>
                                <Col span={24}>
                                    <ReactQuill theme="snow" value={valueContent} onChange={setValueContent} />
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('note.title')}
                                </Divider>
                                <Col span={24}>
                                    <TextArea name="note" value={note} onChange={handleTextareaChange} />
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default BusinessOwnerDetail;
