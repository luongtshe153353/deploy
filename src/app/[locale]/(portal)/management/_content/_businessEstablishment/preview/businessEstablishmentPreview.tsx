'use client';
import React from 'react';
import { AntDesignOutlined, PlusOutlined } from '@ant-design/icons';
import {
    Avatar,
    Button,
    Checkbox,
    Col,
    DatePicker,
    Divider,
    Drawer,
    Form,
    Image,
    Input,
    Rate,
    Row,
    Select,
    Space,
    Tag,
} from 'antd';
import { useTranslations } from 'next-intl';
import type { DatePickerProps } from 'antd';
import { Typography } from 'antd';

const { Text } = Typography;

const { Option } = Select;

interface BusinessEstablishmentPreviewProps {
    idSelected?: number | string;
    openPreview?: boolean;
    setOpenPreview: (b: boolean) => void;
}

const BusinessEstablishmentPreview = (props: BusinessEstablishmentPreviewProps): React.ReactNode => {
    const { openPreview, setOpenPreview } = props;
    const listScreen = useTranslations('Management.businessEstablishment.listScreen');
    const translate = useTranslations('Management.businessEstablishment.previewScreen');

    return (
        <div>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={() => setOpenPreview(false)}
                open={openPreview}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={() => setOpenPreview(false)}>{translate('button.close')}</Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form layout="vertical">
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item name="hotelName" label={translate('info.hotelName')}>
                                        <mark>TravelBoo Hotel</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item name="starRating" label={translate('info.starRating')}>
                                        <Rate disabled defaultValue={3} />
                                    </Form.Item>
                                </Col>
                                <Col span={10}>
                                    <Form.Item labelAlign="right" name="bo" label={translate('info.bo')}>
                                        <mark>Quản lí 1</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item name="taxCode" label={translate('info.taxCode')}>
                                        <mark>123456abcxyz</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <Form.Item name="image" label={translate('info.image')}>
                                        <Row align="middle" gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://go2joy.s3.ap-southeast-1.amazonaws.com/blog/wp-content/uploads/2022/10/26151709/park-hyatt-5-sao-sai-gon.jpg"
                                                />
                                            </Col>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://vungtau.viashotels.com/wp-content/uploads/2022/03/KHACH-SAN-VUNG-TAU-FINAL-11122021_Page_012-3-1.jpg"
                                                />
                                            </Col>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://cdn.alongwalk.info/vn/wp-content/uploads/2022/02/16093757/image-top-6-khach-san-5-sao-sapa-chat-luong-tot-nhat-cho-ky-nghi-duong-164495387794491.jpg"
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('location.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="province"
                                        label={translate('location.province')}
                                    >
                                        <mark>Hà Nội</mark>
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="district"
                                        label={translate('location.district')}
                                    >
                                        <mark>Mỹ Đình</mark>
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item labelAlign="right" name="ward" label={translate('location.ward')}>
                                        <mark>Nguyễn Hoàng</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item name="houseNumber" label={translate('location.houseNumber')}>
                                        <mark>Số 12</mark>
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item name="alleyNumber" label={translate('location.alleyNumber')}>
                                        <mark>Ngõ 180</mark>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24} className="tag-group-custom">
                                    <Tag color="blue">Dọn phòng hàng ngày</Tag>
                                    <Tag color="blue">Đưa đón đến sân bay</Tag>
                                    <Tag color="blue">Giặt là</Tag>
                                    <Tag color="blue">Wifi riêng mỗi phòng</Tag>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('description.title')}
                                </Divider>
                                <Col span={24}>
                                    <div>
                                        <strong style={{ fontSize: '18px' }}>Quill Rich Text Editor</strong>
                                    </div>
                                    <div>
                                        <br />
                                    </div>
                                    <div>
                                        Quill is a free, <a href="https://github.com/quilljs/quill/">open source</a>{' '}
                                        WYSIWYG editor built for the modern web. With its{' '}
                                        <a href="http://quilljs.com/docs/modules/">extensible architecture</a> and a{' '}
                                        <a href="http://quilljs.com/docs/api/">expressive API</a> you can completely
                                        customize it to fulfill your needs. Some built in features include:
                                    </div>
                                    <div>
                                        <br />
                                    </div>
                                    <ul>
                                        <li>Fast and lightweight</li>
                                        <li>Semantic markup</li>
                                        <li>Standardized HTML between browsers</li>
                                        <li>Cross browser support including Chrome, Firefox, Safari, and IE 9+</li>
                                    </ul>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('note.title')}
                                </Divider>
                                <Col span={24}>
                                    <mark>Đây là một khách sạn đẹp</mark>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default BusinessEstablishmentPreview;
