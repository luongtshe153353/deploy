'use client';
import React from 'react';
import { Col, DatePicker, Form, Input, Modal, Row, Select } from 'antd';
import { useTranslations } from 'next-intl';
import { BulbTwoTone } from '@ant-design/icons';
import { GenderFemale, GenderMale } from '@carbon/icons-react';

const { Option } = Select;

interface AdvanceFilterProps {
    openFilter: boolean;
    setOpenFilter: (b: boolean) => void;
}

const AdvanceFilter = (props: AdvanceFilterProps): React.ReactNode => {
    const { openFilter, setOpenFilter } = props;

    const translate = useTranslations('Management.BusinessOwner.listScreen.advanceFilter');

    return (
        <>
            <Modal
                title={translate('title')}
                centered
                open={openFilter}
                onOk={() => setOpenFilter(false)}
                onCancel={() => setOpenFilter(false)}
                width={1000}
            >
                <Form layout="vertical">
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={3}>
                            <Form.Item name="id" label={translate('id')}>
                                <Input placeholder="Please enter user name" />
                            </Form.Item>
                        </Col>

                        <Col span={7}>
                            <Form.Item labelAlign="left" name="email" label={translate('email')}>
                                <Input placeholder="Please enter user name" />
                            </Form.Item>
                        </Col>

                        <Col span={4}>
                            <Form.Item name="gender" label={translate('gender')}>
                                <Select placeholder="Please select an owner">
                                    <Option value="xiao">
                                        <GenderMale color="#227dff" size={16} className="me-2" />
                                        Nam
                                    </Option>
                                    <Option value="mao">
                                        <GenderFemale color="#fd5d92" size={16} className="me-2" />
                                        Nữ
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={5}>
                            <Form.Item labelAlign="right" name="dob" label={translate('createdDate')}>
                                <DatePicker onChange={() => {}} className="w-100" />
                            </Form.Item>
                        </Col>

                        <Col span={5}>
                            <Form.Item name="status" label={translate('status')}>
                                <Select placeholder="Please select an owner">
                                    <Option value="xiao">
                                        <BulbTwoTone twoToneColor={'#34f231'} size={16} className="me-2" />
                                        Hoạt động
                                    </Option>
                                    <Option value="mao">
                                        <BulbTwoTone twoToneColor={'red'} size={16} className="me-2" />
                                        Dừng hoạt động
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={7}>
                            <Form.Item labelAlign="left" name="businessName" label={translate('businessName')}>
                                <Select placeholder="Please select an owner">
                                    <Option value="xiao">Cơ sở 1</Option>
                                    <Option value="mao">Cơ sở 2</Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={5}>
                            <Form.Item labelAlign="right" name="province" label={translate('province')}>
                                <Select
                                    showSearch
                                    placeholder="Select a province"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'haNoi',
                                            label: 'Hà Nội',
                                        },
                                        {
                                            value: 'hoChiMinh',
                                            label: 'Hồ Chí Minh',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>

                        <Col span={6}>
                            <Form.Item labelAlign="right" name="district" label={translate('district')}>
                                <Select
                                    showSearch
                                    placeholder="Select a district"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'namTuLiem',
                                            label: 'Nam Từ Liêm',
                                        },
                                        {
                                            value: 'cauGiay',
                                            label: 'Cầu Giấy',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>

                        <Col span={6}>
                            <Form.Item labelAlign="right" name="ward" label={translate('ward')}>
                                <Select
                                    showSearch
                                    placeholder="Select a ward"
                                    optionFilterProp="children"
                                    options={[
                                        {
                                            value: 'ward1',
                                            label: 'Ward1',
                                        },
                                        {
                                            value: 'ward2',
                                            label: 'Ward2',
                                        },
                                    ]}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};

export default AdvanceFilter;
