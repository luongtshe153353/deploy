'use client';
import React from 'react';
import { Badge, Button, Col, Pagination, Row, Table } from 'antd';
import { Input } from 'antd';
import { useTranslations } from 'next-intl';
import './businessOwner.scss';
import { FilterOutlined } from '@ant-design/icons';
import AdvanceFilter from './advanceFilter/advanceFilter';
import BusinessOwnerPreview from './preview/businessOwnerPreview';
import useBusinessOwnerHook from './businessOwnerHook';
import BusinessOwnerCreate from './create/businessOwnerCreate';

const { Search } = Input;

const BusinessOwnerContent = () => {
    const {
        columns,
        data,
        //loading,
        //start,
        onSearch,
        openPreview,
        setOpenPreview,
        openCreate,
        setOpenCreate,
        idSelected,
        isLoading,
        meta,
        onChangePagination,
        setIsReload
    } = useBusinessOwnerHook();

    const translate = useTranslations('Management.Menu');

    const [openFilter, setOpenFilter] = React.useState<boolean>(false);

    return (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="businessOwner m-0 p-0 w-100 h-100 overflow-auto">
            <Col span={24} className="d-flex flex-row mb-5 justify-content-between">
                <h2>{translate('businessOwner')}</h2>

                <BusinessOwnerCreate openCreate={openCreate} setOpenCreate={setOpenCreate} idSelected={idSelected} setIsReload={setIsReload} />
            </Col>

            <Col span={16} offset={3} className="d-flex flex-row">
                <Search
                    placeholder="Tìm kiếm"
                    allowClear
                    onChange={onSearch}
                    style={{ width: '100%' }}
                    addonAfter={
                        <Badge count={4}>
                            <Button
                                className="ant-btn-icon-only ant-input-filter-button"
                                onClick={() => setOpenFilter(true)}
                            >
                                <span className="ant-btn-icon">
                                    <FilterOutlined />
                                </span>
                            </Button>{' '}
                        </Badge>
                    }
                />
            </Col>

            <Col span={24} className="d-flex flex-column">
                <div className="d-flex flex-row mb-5">
                    {/* <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                        Reload
                    </Button> */}
                </div>

                <Table
                    className="flex-fill"
                    size="small"
                    //rowSelection={rowSelection}
                    columns={columns}
                    dataSource={Array.isArray(data) ? data : []}
                    loading={isLoading}
                    scroll={{ x: 'auto', y: 'auto' }}
                    pagination={false}
                />
            </Col>

            <Col span={24} className='p-0 mt-2 d-flex flex-row justify-content-end'>
                <Pagination
                    defaultCurrent={1}
                    current={meta?.pageNum}
                    onChange={onChangePagination}
                    defaultPageSize={meta?.pageSize}
                    total={meta?.total}
                    size='small'
                />
            </Col>

            <BusinessOwnerPreview openPreview={openPreview} setOpenPreview={setOpenPreview} idSelected={idSelected} />

            <AdvanceFilter openFilter={openFilter} setOpenFilter={setOpenFilter} />
        </Row>
    );
};

export default BusinessOwnerContent;
