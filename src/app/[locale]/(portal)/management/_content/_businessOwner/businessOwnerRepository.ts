import { Repository } from '@/config/repository';

export const API_BUSINESS_OWNER_PREIX: string = 'business-owner/';

export class BusinessOwnerRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_BUSINESS_OWNER_PREIX;
    }

    public searchBusinessOwner = (payload?: any) => this.post('search-business-owner', payload);

    public createBusinessOwner = (payload?: any) =>
        this.post('create-bo', payload).then((result) => {
            return result;
        });

    public getBusinessOwner = (payload?: any) =>
        this.get('get-bo/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const businessOwnerRepository = new BusinessOwnerRepository();
