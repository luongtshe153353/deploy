'use client';
import React from 'react';
import { AntDesignOutlined } from '@ant-design/icons';
import { Avatar, Button, Col, Divider, Drawer, Form, Row, Space } from 'antd';
import { useTranslations } from 'next-intl';
import { Typography } from 'antd';
import useSWR from 'swr';
import { businessOwnerRepository } from '../businessOwnerRepository';

const { Text } = Typography;

interface BusinessOwnerPreviewProps {
    idSelected?: number | string;
    openPreview?: boolean;
    setOpenPreview: (b: boolean) => void;
}

interface DataType {
    key: React.Key;
    id: number,
    userName: string | null;
    email: string | null;
    firstName: string | null;
    lastName: string | null;
    phoneNumber: string | null;
    birthDate: string | null;
    gender: string | null;
    avatar: string | null;
    status: boolean | null;
    roleId: number | null,
    address1: string | null;
    address2: string | null;
    addressId: number | null;
    wardId: number | null;
    districtId: number | null;
    provinceId: number | null;
    wardName: string | null;
    districtName: string | null;
    provinceName: string | null;
    identification: string | null;
}

const BusinessOwnerPreview = (props: BusinessOwnerPreviewProps): React.ReactNode => {
    const { idSelected, openPreview, setOpenPreview } = props;
    const translate = useTranslations('Management.BusinessOwner.previewScreen');

    const { data } = useSWR('get-bo/' + idSelected, () =>
        businessOwnerRepository.getBusinessOwner(idSelected)
    ) as { data: DataType };

    return (
        <div>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={() => setOpenPreview(false)}
                open={openPreview}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={() => setOpenPreview(false)}>{translate('button.close')}</Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24} className="d-flex flex-row justify-content-center mb-3">
                        <Avatar
                            size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
                            icon={<AntDesignOutlined />}
                        />
                    </Col>

                    <Col span={24} className="p-5 pt-0">
                        <Form layout="vertical" hideRequiredMark className="">
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={5}>
                                    <Form.Item name="firstName" label={translate('info.firstName')}>
                                        <Text mark>{data ? data.firstName : ""}</Text>
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item name="lastName" label={translate('info.lastName')}>
                                        <Text mark>{data ? data.lastName : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={4}>
                                    <Form.Item name="gender" label={translate('info.gender')}>
                                        <Text mark>{data ? data.gender : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={10}>
                                    <Form.Item labelAlign="right" name="dob" label={translate('info.dob')}>
                                        <Text mark>{data ? data.birthDate : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={5}>
                                    <Form.Item name="userName" label={translate('info.userName')}>
                                        <Text mark>{data ? data.userName : ""}</Text>
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item labelAlign="right" name="email" label={translate('info.email')}>
                                        <Text mark>{data ? data.email : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={10}>
                                    <Form.Item labelAlign="right" name="role" label={translate('info.role')}>
                                        <Text mark>Chủ cơ sở</Text>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('contact.title')}
                                </Divider>
                                <Col span={12}>
                                    <Form.Item name="phoneNumber" label={translate('contact.phoneNumber')}>
                                        <Text mark>{data ? data.phoneNumber : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item name="identification" label={translate('info.identification')}>
                                        <Text mark>{data ? data.identification : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item labelAlign="right" name="province" label={translate('contact.province')}>
                                        <Text mark>{data ? data.provinceName : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item labelAlign="right" name="district" label={translate('contact.district')}>
                                        <Text mark>{data ? data.districtName : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item labelAlign="right" name="ward" label={translate('contact.ward')}>
                                        <Text mark>{data ? data.wardName : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item name="address1" label={translate('contact.houseNumber')}>
                                        <Text mark>{data ? data.address1 : ""}</Text>
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item name="address2" label={translate('contact.alleyNumber')}>
                                        <Text mark>{data ? data.address2 : ""}</Text>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default BusinessOwnerPreview;
