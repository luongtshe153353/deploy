'use client';
import React from 'react';
import { Col, Form, Row, Select } from 'antd';
import { Input } from 'antd';
import { useTranslations } from 'next-intl';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const { Option } = Select;

const EmailMarketingContent = () => {

    const translate = useTranslations('Management.EmailMarketing.addScreen');
    const [valueContent, setValueContent] = React.useState('');

    React.useEffect(() => { }, []);

    return (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="emailMarketing m-0 p-0 w-100 h-100 overflow-auto">
            <Col span={24} className="d-flex flex-row mb-5 justify-content-between">
                <h2>Gửi email</h2>
            </Col>

            <Col span={20} className="d-flex flex-column">
                <Form
                    id="myForm"
                    labelCol={{ span: 3 }}
                    layout="horizontal"
                    hideRequiredMark
                >
                    <Form.Item name="receiver" label={translate('receiver')} initialValue="all" >
                        <Select placeholder="Lựa chọn người nhận" defaultValue="all">
                            <Option value="all" selected>
                                Tất cả
                            </Option>
                            <Option value="businessAdmin">Chủ doanh nghiệp</Option>
                            <Option value="businessOwner">Chủ cơ sở kinh doanh</Option>
                            <Option value="registeredUser">Người dùng</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="subject"
                        label={translate('subject')}
                        rules={[{ required: true, message: 'Vui lòng nhập tiêu đề email' }]}
                    >
                        <Input placeholder="Nhập tiêu đề email" />
                    </Form.Item>
                    <Form.Item
                        name="content"
                        label={translate('content')}
                        rules={[{ required: true, message: 'Vui lòng nhập nội dung email' }]}
                    >
                        <ReactQuill
                            theme="snow"
                            value={valueContent}
                            onChange={setValueContent}
                            style={{ height: '350px' }}
                        />
                    </Form.Item>
                </Form>
            </Col>


        </Row>
    );
};

export default EmailMarketingContent;
