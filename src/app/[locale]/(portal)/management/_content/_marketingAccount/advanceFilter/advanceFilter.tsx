'use client';
import React from 'react';
import { Col, DatePicker, Form, Input, Modal, Row, Select } from 'antd';
import { useTranslations } from 'next-intl';
import { BulbTwoTone } from '@ant-design/icons';
import { GenderFemale, GenderMale } from '@carbon/icons-react';

const { Option } = Select;

interface AdvanceFilterProps {
    openFilter: boolean;
    setOpenFilter: (b: boolean) => void;
}

const AdvanceFilter = (props: AdvanceFilterProps): React.ReactNode => {
    const { openFilter, setOpenFilter } = props;

    const translate = useTranslations('Management.MarketingAccount.listScreen.advanceFilter');

    return (
        <>
            <Modal
                title={translate('title')}
                centered
                open={openFilter}
                onOk={() => setOpenFilter(false)}
                onCancel={() => setOpenFilter(false)}
                width={1000}
            >
                <Form layout="vertical">
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={7}>
                            <Form.Item labelAlign="left" name="email" label={translate('email')}>
                                <Input placeholder="Nhập email" />
                            </Form.Item>
                        </Col>

                        <Col span={5}>
                            <Form.Item name="gender" label={translate('gender')}>
                                <Select placeholder="Lựa chọn giới tính">
                                    <Option value="xiao">
                                        <GenderMale color="#227dff" size={16} className="me-2" />
                                        Nam
                                    </Option>
                                    <Option value="mao">
                                        <GenderFemale color="#fd5d92" size={16} className="me-2" />
                                        Nữ
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={6}>
                            <Form.Item labelAlign="right" name="dob" label={translate('createdDate')}>
                                <DatePicker placeholder="Chọn ngày" onChange={() => { }} className="w-100" />
                            </Form.Item>
                        </Col>

                        <Col span={6}>
                            <Form.Item name="status" label={translate('status')}>
                                <Select placeholder="Lựa chọn trạng thái">
                                    <Option value="xiao">
                                        <BulbTwoTone twoToneColor={'#34f231'} size={16} className="me-2" />
                                        Hoạt động
                                    </Option>
                                    <Option value="mao">
                                        <BulbTwoTone twoToneColor={'red'} size={16} className="me-2" />
                                        Dừng hoạt động
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};

export default AdvanceFilter;
