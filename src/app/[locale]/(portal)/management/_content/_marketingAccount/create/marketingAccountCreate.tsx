'use client';
import React from 'react';
import { AntDesignOutlined, PlusOutlined } from '@ant-design/icons';
import { Avatar, Button, Col, DatePicker, Divider, Drawer, Form, Input, Row, Select, Space, Spin } from 'antd';
import { useTranslations } from 'next-intl';
import useMarketingAccountCreateHook from './marketingAccountCreateHook';
import useValidate from '@/utilities/validate';
import { LoadingOutlined } from '@ant-design/icons';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const { Option } = Select;

interface MarketingAccountCreateProps {
    idSelected?: number | string;
    openCreate: any;
    setOpenCreate: (b: any) => void;
    setIsReload: (b: any) => void;
}

const MarketingAccountCreate = (props: MarketingAccountCreateProps): React.ReactNode => {
    const { idSelected, openCreate, setOpenCreate, setIsReload } = props;


    const {
        firstNameRule,
        lastNameRule,
        genderRule,
        dobRule,
        userNameRule,
        emailRule,
        phoneNumberRule,
        provinceRule,
        districtRule,
        wardRule,
    } = useValidate()


    const listScreen = useTranslations('Management.MarketingAccount.addScreen');
    const translate = useTranslations('Management.MarketingAccount.addScreen');

    const showDrawer = () => {
        setOpenCreate((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = React.useCallback(() => {
        setOpenCreate({
            isCreate: false,
            isOpen: false,
        });
    }, [setOpenCreate]);

    const [form] = Form.useForm();

    const {
        onFinish,
        handleProvince,
        province,
        handleDistrict,
        district,
        handleWard,
        ward
    } = useMarketingAccountCreateHook({ form })

    const handleSubmit = React.useCallback(() => {

        form.validateFields().then((values) => {
            onFinish(values).then(() => {
                setIsReload(true)
                onClose()
            });
        });

    }, [form, onClose, onFinish])

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.submit')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={onClose}
                open={openCreate?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button onClick={handleSubmit} type="primary" htmlType="submit" className="login-form-button">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24} className="d-flex flex-row justify-content-center mb-3">
                        <Avatar
                            size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
                            icon={<AntDesignOutlined />}
                        />
                    </Col>

                    <Col span={24}>
                        <Form form={form} layout="vertical" hideRequiredMark className="" >
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={5}>
                                    <Form.Item
                                        name="firstName"
                                        label={translate('info.firstName')}
                                        rules={firstNameRule}
                                    >
                                        <Input placeholder="Nhập họ" />
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item
                                        name="lastName"
                                        label={translate('info.lastName')}
                                        rules={lastNameRule}
                                    >
                                        <Input placeholder="Nhập tên" />
                                    </Form.Item>
                                </Col>

                                <Col span={4}>
                                    <Form.Item
                                        name="gender"
                                        label={translate('info.gender')}
                                        rules={genderRule}
                                    >
                                        <Select placeholder="Lựa chọn giới tính">
                                            <Option value="Nam">Nam</Option>
                                            <Option value="Nữ">Nữ</Option>
                                        </Select>
                                    </Form.Item>
                                </Col>

                                <Col span={10}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="birthDate"
                                        label={translate('info.dob')}
                                        rules={dobRule}
                                    >
                                        <DatePicker className="w-100" placeholder="Lựa chọn ngày sinh" />
                                    </Form.Item>
                                </Col>

                                <Col span={5}>
                                    <Form.Item
                                        name="username"
                                        label={translate('info.userName')}
                                        rules={userNameRule}
                                    >
                                        <Input placeholder="Nhập tên đăng nhập" />
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="email"
                                        label="Email"
                                        rules={emailRule}
                                    >
                                        <Input placeholder="Nhập email" />
                                    </Form.Item>
                                </Col>

                                <Col span={10}>
                                    <Form.Item labelAlign="right" name="role" label={translate('info.role')}>
                                        <p>Marketing</p>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('contact.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="phoneNumber"
                                        label={translate('contact.phoneNumber')}
                                        rules={phoneNumberRule}
                                    >
                                        <Input placeholder="Nhập số điện thoại" type='number' />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        name="provinceId"
                                        labelAlign="right"
                                        label={translate('contact.province')}
                                        rules={provinceRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn tỉnh/thành phố"
                                            optionFilterProp="children"
                                            onClick={handleProvince}
                                            options={province?.length > 0 ? province?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        name="districtId"
                                        labelAlign="right"
                                        label={translate('contact.district')}
                                        rules={districtRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn quận/huyện"
                                            optionFilterProp="children"
                                            onClick={handleDistrict}
                                            options={district?.length > 0 ? district?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('provinceId') ? false : true}
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="wardId"
                                        label={translate('contact.ward')}
                                        rules={wardRule}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn xã/phường"
                                            optionFilterProp="children"
                                            onClick={handleWard}
                                            options={ward?.length > 0 ? ward?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('districtId') ? false : true}
                                        />
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item
                                        name="address1"
                                        label={translate('contact.houseNumber')}
                                    >
                                        <Input placeholder="Nhập số nhà" />
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item
                                        name="address2"
                                        label={translate('contact.alleyNumber')}
                                    >
                                        <Input placeholder="Nhập số ngõ" />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default MarketingAccountCreate;
