import { Repository } from '@/config/repository';

export const API_MARKETING_ACCOUNT_PREIX: string = 'marketing/';

export class MarketingAccountRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_MARKETING_ACCOUNT_PREIX;
    }

    public searchMarketingAccount = (payload?: any) => this.post('search-marketing', payload);

    public createMarketingAccount = (payload?: any) =>
        this.post('create-marketing', payload).then((result) => {
            return result;
        });

    public getMarketingAccount = (payload?: any) =>
        this.get('get-marketing/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const marketingAccountRepository = new MarketingAccountRepository();
