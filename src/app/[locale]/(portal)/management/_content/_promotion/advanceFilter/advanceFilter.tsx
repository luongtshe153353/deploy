'use client';
import React from 'react';
import { Col, DatePicker, Form, Input, Modal, Row, Select } from 'antd';
import { useTranslations } from 'next-intl';
import { BulbTwoTone } from '@ant-design/icons';

const { Option } = Select;

interface AdvanceFilterProps {
    openFilter: boolean;
    setOpenFilter: (b: boolean) => void;
}

const AdvanceFilter = (props: AdvanceFilterProps): React.ReactNode => {
    const { openFilter, setOpenFilter } = props;

    const translate = useTranslations('Management.Promotion.listScreen.advanceFilter');
    const { RangePicker } = DatePicker;
    return (
        <>
            <Modal
                title={translate('title')}
                centered
                open={openFilter}
                onOk={() => setOpenFilter(false)}
                onCancel={() => setOpenFilter(false)}
                width={1000}
            >
                <Form layout="vertical">
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={4}>
                            <Form.Item labelAlign="left" name="code" label={translate('code')}>
                                <Input placeholder="Nhập mã ưu đãi" />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item labelAlign="left" name="name" label={translate('name')}>
                                <Input placeholder="Nhập tên ưu đãi" />
                            </Form.Item>
                        </Col>

                        <Col span={8}>
                            <Form.Item labelAlign="right" name="dob" label={translate('createdDate')}>
                                <RangePicker />
                            </Form.Item>
                        </Col>

                        <Col span={5}>
                            <Form.Item name="status" label={translate('status')}>
                                <Select placeholder="Lựa chọn trạng thái">
                                    <Option value="danghoatdong">
                                        <BulbTwoTone twoToneColor={'#34f231'} size={16} className="me-2" />
                                        Đang hoạt động
                                    </Option>
                                    <Option value="hethieuluc">
                                        <BulbTwoTone twoToneColor={'red'} size={16} className="me-2" />
                                        Hết hiệu lực
                                    </Option>
                                    <Option value="tamdung">
                                        <BulbTwoTone twoToneColor={'yellow'} size={16} className="me-2" />
                                        Tạm dừng
                                    </Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};

export default AdvanceFilter;
