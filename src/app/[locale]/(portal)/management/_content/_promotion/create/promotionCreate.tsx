'use client';
import React, { useState } from 'react';
import { PercentageOutlined, PlusOutlined } from '@ant-design/icons';
import {
    Button,
    Col,
    DatePicker,
    Divider,
    Drawer,
    Form,
    Input,
    InputNumber,
    Radio,
    Row,
    Space,
    Upload,
} from 'antd';
import { useTranslations } from 'next-intl';
import type { RadioChangeEvent } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { TravelBooContext } from '@/app/[locale]/(portal)/layout';
import { promotionRepository } from '../promotionRepository';
import useValidate from '@/utilities/validate';

interface PromotionCreateProps {
    idSelected?: number | string;
    openCreate: any;
    setOpenCreate: (b: any) => void;
    setReload: (b: any) => void;
}

const PromotionCreate = (props: PromotionCreateProps): React.ReactNode => {
    const { idSelected, openCreate, setOpenCreate, setReload } = props;

    const {
        codeRule
    } = useValidate()

    const listScreen = useTranslations('Management.Promotion.listScreen');
    const translate = useTranslations('Management.Promotion.addScreen');

    const showDrawer = () => {
        setOpenCreate((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = () => {
        setOpenCreate({
            isCreate: false,
            isOpen: false,
        });
    };

    const { RangePicker } = DatePicker;
    const [valueLimitUse, setValueLimitUse] = useState(1);

    const onChangeLimitUse = (e: RadioChangeEvent) => {
        setValueLimitUse(e.target.value);
    };

    const [valueTypeSale, setValueTypeSale] = useState(1);

    const onChangeTypeSale = (e: RadioChangeEvent) => {
        setValueTypeSale(e.target.value);
    };

    // submit form
    const [form] = Form.useForm();
    const { openNotification } = React.useContext(TravelBooContext)

    const onFinish = React.useCallback((values: any) => {

        const formData = new FormData();
        formData.append("code", values.code);
        formData.append("name", values.name);
        formData.append("description", values.description);

        let inputDate = new Date(values.time[0]);
        let year = inputDate.getFullYear();
        let month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed, so add 1.
        let day = inputDate.getDate().toString().padStart(2, '0');
        let formattedDate = `${year}-${month}-${day}`;
        formData.append("startDate", formattedDate);

        inputDate = new Date(values.time[1]);
        year = inputDate.getFullYear();
        month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed, so add 1.
        day = inputDate.getDate().toString().padStart(2, '0');
        formattedDate = `${year}-${month}-${day}`;
        formData.append("endDate", formattedDate);

        formData.append("typePromotion", values.typePromotion);
        formData.append("discountPercent", values.discountPercent);
        formData.append("maxDiscount", values.maxDiscount);
        formData.append("fixMoneyDiscount", values.fixMoneyDiscount);
        formData.append("maxUse", values.maxUse);
        formData.append("typeMaxUse", values.typeMaxUse);
        formData.append("image", values.image)

        return promotionRepository.createPromotion(formData).then((result: any) => {
            if (result?.code === 200) {
                openNotification('success', "Thành công", "Thêm mới khuyến mãi thành công")
            } else {
                openNotification('error', "Thất bại", "Thêm mới khuyến mãi thất bại")
            }
        })
    }, [openNotification, translate])

    const handleSubmit = React.useCallback((e: any) => {

        form.validateFields().then((values) => {
            onFinish(values).then(() => {
                setReload(true);
                onClose()
            });
        })
            .catch((error) => {
                console.log(error)
            });

    }, [form, onClose, onFinish])

    const getFile = (e: any) => {
        return e.file;
    };

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1230}
                onClose={onClose}
                open={openCreate?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button htmlType="submit" form="myForm" onClick={handleSubmit} type="primary">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Form
                    id="myForm"
                    labelCol={{ span: 5 }}
                    layout="horizontal"
                    hideRequiredMark
                    // onFinish={onFinish}
                    // onFinishFailed={onFinishFailed}
                    form={form}
                >
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                            <Divider orientation="left" orientationMargin={5}>
                                {translate('info.title')}
                            </Divider>
                            <Col>
                                <Form.Item
                                    colon={false}
                                    name="code"
                                    label={translate('info.code')}
                                    rules={codeRule}
                                >
                                    <Input />
                                </Form.Item>
                                <Form.Item
                                    colon={false}
                                    name="name"
                                    label={translate('info.name')}
                                    rules={[{ required: true, message: 'Vui lòng nhập tên khuyến mãi' }]}
                                >
                                    <Input />
                                </Form.Item>
                                <Form.Item
                                    colon={false}
                                    name="image"
                                    label={translate('info.image')}
                                    rules={[{ required: true, message: 'Vui lòng tải lên hình ảnh' }]}
                                    getValueFromEvent={getFile}
                                >
                                    <Upload
                                        beforeUpload={() => false}
                                        // onChange={onChangeFile}
                                        // fileList={fileList}
                                        accept="image/png, image/jpeg"
                                        listType="picture-card"
                                        maxCount={1}
                                    >
                                        <div>
                                            <PlusOutlined />
                                            <div style={{ marginTop: 8 }}>Tải lên</div>
                                        </div>
                                    </Upload>
                                </Form.Item>
                                <Form.Item
                                    colon={false}
                                    name="description"
                                    label={translate('info.description')}
                                    rules={[{ required: true, message: 'Vui lòng nhập mô tả khuyến mãi' }]}
                                >
                                    <TextArea rows={4} />
                                </Form.Item>
                                <Form.Item
                                    colon={false}
                                    name="time"
                                    label={translate('info.date')}
                                //rules={[{ required: true, message: 'Vui lòng lựa chọn ngày' }]}
                                >
                                    <RangePicker />
                                </Form.Item>
                            </Col>
                        </Col>
                        <Col span={16}>
                            <Col>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('mandatoryCondition.title')}
                                </Divider>
                                <Col>
                                    <p className="fw-bolder">{translate('mandatoryCondition.subtitle1')}</p>
                                    <Form.Item name="typePromotion" initialValue={1}>
                                        <Radio.Group onChange={onChangeTypeSale} value={valueTypeSale}>
                                            <Space direction="vertical">
                                                <span className="d-flex align-items-start" style={{ height: '50px' }}>
                                                    <Radio value={1} checked={true} className="align-seft-start">
                                                        <span className="d-flex" style={{ height: '30px' }}>
                                                            <span className="align-self-center" style={{ width: 120 }}>
                                                                Tỷ lệ phần trăm
                                                            </span>

                                                            {valueTypeSale === 1 ? (
                                                                <span
                                                                    className="align-self-center d-flex"
                                                                    style={{ height: '30px' }}
                                                                >
                                                                    <Form.Item
                                                                        name="discountPercent"
                                                                        rules={[
                                                                            {
                                                                                required: true,
                                                                                message: 'Vui lòng nhập phần trăm giảm',
                                                                            },
                                                                        ]}
                                                                        initialValue={1}
                                                                    >
                                                                        <span className="align-self-center">
                                                                            <InputNumber
                                                                                min={1}
                                                                                max={100}
                                                                                defaultValue={1}
                                                                                addonAfter={<PercentageOutlined />}
                                                                                className="align-self-center"
                                                                                name="discountPercent"
                                                                                placeholder="Phần trăm giảm"
                                                                                style={{ width: 200, marginLeft: 10 }}
                                                                            />
                                                                        </span>
                                                                    </Form.Item>
                                                                    <Form.Item name="maxDiscount">
                                                                        <span className="align-self-center">
                                                                            <InputNumber
                                                                                min={1000}
                                                                                addonAfter="VND"
                                                                                className="ml-5"
                                                                                placeholder="Giảm tối đa"
                                                                                style={{ width: 200, marginLeft: 10 }}
                                                                            />
                                                                        </span>
                                                                    </Form.Item>
                                                                </span>
                                                            ) : (
                                                                <span
                                                                    className="align-self-center d-flex"
                                                                    style={{ height: '33px' }}
                                                                >
                                                                    <InputNumber
                                                                        disabled
                                                                        addonAfter={<PercentageOutlined />}
                                                                        className="align-self-center"
                                                                        name="discountPercent"
                                                                        placeholder="Phần trăm giảm"
                                                                        style={{ width: 200, marginLeft: 10 }}
                                                                    />
                                                                    <InputNumber
                                                                        disabled
                                                                        addonAfter="VND"
                                                                        className="ml-5"
                                                                        placeholder="Giảm tối đa"
                                                                        style={{ width: 200, marginLeft: 10 }}
                                                                    />
                                                                </span>
                                                            )}
                                                        </span>
                                                    </Radio>
                                                </span>

                                                <Radio value={2}>
                                                    <span className="d-flex">
                                                        <span className="align-self-center" style={{ width: 120 }}>
                                                            Số tiền cố định
                                                        </span>
                                                        {valueTypeSale === 2 ? (
                                                            <span
                                                                className="align-self-center d-flex"
                                                                style={{ height: '30px' }}
                                                            >
                                                                <Form.Item
                                                                    name="fixMoneyDiscount"
                                                                    rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Vui lòng nhập số tiền được giảm',
                                                                        },
                                                                    ]}
                                                                >
                                                                    <InputNumber
                                                                        addonAfter="VND"
                                                                        placeholder="Số tiền tối đa"
                                                                        style={{ width: 410, marginLeft: 10 }}
                                                                    />
                                                                </Form.Item>
                                                            </span>
                                                        ) : (
                                                            <InputNumber
                                                                addonAfter="VND"
                                                                placeholder="Số tiền tối đa"
                                                                disabled
                                                                style={{ width: 410, marginLeft: 10 }}
                                                            />
                                                        )}
                                                    </span>
                                                </Radio>
                                            </Space>
                                        </Radio.Group>
                                    </Form.Item>

                                    <p className="fw-bolder mt-5">{translate('mandatoryCondition.subtitle2')}</p>
                                    <Form.Item name="typeMaxUse" initialValue={1}>
                                        <Radio.Group onChange={onChangeLimitUse} value={valueLimitUse}>
                                            <Space direction="vertical">
                                                <span className="d-flex align-items-start" style={{ height: '40px' }}>
                                                    <Radio value={1} checked>
                                                        <span style={{ width: 200, display: 'inline-block' }}>
                                                            Không giới hạn số lượt sử dụng
                                                        </span>
                                                    </Radio>
                                                </span>
                                                <span className="d-flex" style={{ height: '30px' }}>
                                                    <Radio value={2}>
                                                        <span className="d-flex ">
                                                            <span
                                                                className="align-items-center"
                                                                style={{ width: 200, lineHeight: '30px' }}
                                                            >
                                                                Giới hạn số lượt sử dụng
                                                            </span>

                                                            {valueLimitUse === 2 ? (
                                                                <Form.Item
                                                                    name="maxUse"
                                                                    rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Vui lòng nhập số lượt sử dụng',
                                                                        },
                                                                    ]}
                                                                    initialValue={1}
                                                                >
                                                                    <InputNumber
                                                                        min={1}
                                                                        defaultValue={1}
                                                                        placeholder="Số lượt sử dụng"
                                                                        style={{ width: 330, marginLeft: 10 }}
                                                                    />
                                                                </Form.Item>
                                                            ) : (
                                                                <InputNumber
                                                                    disabled
                                                                    placeholder="Số lượt sử dụng"
                                                                    style={{ width: 330, marginLeft: 10 }}
                                                                />
                                                            )}
                                                        </span>
                                                    </Radio>
                                                </span>
                                            </Space>
                                        </Radio.Group>
                                    </Form.Item>
                                </Col>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
        </div>
    );
};

export default PromotionCreate;
