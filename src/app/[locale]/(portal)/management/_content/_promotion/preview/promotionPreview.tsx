'use client';
import React from 'react';
import dayjs from 'dayjs';
import { Button, Col, DatePicker, Divider, Drawer, Form, Image, Input, Radio, Row, Space } from 'antd';
import { useTranslations } from 'next-intl';
import { Typography } from 'antd';
import { promotionRepository } from '../promotionRepository';
import useSWR from 'swr';
import { PercentageOutlined } from '@ant-design/icons';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);
const dateFormat = 'YYYY-MM-DD';
const { Text } = Typography;

interface PromotionPreviewProps {
    idSelected?: number | string;
    openPreview?: boolean;
    setOpenPreview: (b: boolean) => void;
}

interface DataType {
    key: React.Key;
    id: number | string;
    code: string;
    name: string | null;
    imageUrl: string | undefined;
    description: string | null;
    startDate: string | null;
    endDate: string | null;
    typePromotion: any;
    discountPercent: any;
    maxDiscount: any;
    fixMoneyDiscount: any
    typeMaxUse: any;
    maxUse: any;
    status: string | null;
}

const PromotionPreview = (props: PromotionPreviewProps): React.ReactNode => {
    const { idSelected, openPreview, setOpenPreview } = props;
    const translate = useTranslations('Management.Promotion.previewScreen');
    const { RangePicker } = DatePicker;

    const [data, setData] = React.useState<any>({})
    const [form] = Form.useForm();
    React.useEffect(() => {
        const loadingList = new Promise((resolve, reject) => {

            resolve(promotionRepository.getPromotion(idSelected).then((result) => {
                setData(result)
            }))

            form.setFieldsValue({
                time: [dayjs(data.startDate), dayjs(data.endDate)],
            })

        })
    }, [idSelected, openPreview])


    return (
        <div>
            <Drawer
                title={translate('title')}
                onClose={() => setOpenPreview(false)}
                open={openPreview}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                width={1230}
                extra={
                    <Space>
                        <Button onClick={() => setOpenPreview(false)}>{translate('button.close')}</Button>
                    </Space>
                }
            >
                <Form labelCol={{ span: 5 }} layout="horizontal" hideRequiredMark form={form}>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                            <Divider orientation="left" orientationMargin={5}>
                                {translate('info.title')}
                            </Divider>
                            <Col>
                                <Form.Item name="code" label={translate('info.code')} colon={false}>
                                    <Text mark>{data ? data.code : ''}</Text>
                                </Form.Item>
                                <Form.Item name="name" label={translate('info.name')} colon={false}>
                                    <Text mark>{data ? data.name : ''}</Text>
                                </Form.Item>
                                <Form.Item name="image" label={translate('info.image')} colon={false}>
                                    <Image
                                        style={{ borderRadius: '15px' }}
                                        width={'100%'}
                                        preview={false}
                                        src={data ? data.imageUrl : ''}
                                    />
                                </Form.Item>

                                <Form.Item name="description" label={translate('info.description')} colon={false}>
                                    <Text mark>
                                        {data ? data.description : ''}
                                    </Text>
                                </Form.Item>

                                <Form.Item name="time" label={translate('info.date')} colon={false}>
                                    <RangePicker
                                        defaultValue={[
                                            dayjs(data ? data.startDate : "2023/06/30", dateFormat),
                                            dayjs(data ? data.endDate : "2023/11/26", dateFormat)
                                        ]}
                                        disabled={true}
                                        format={dateFormat} />
                                </Form.Item>
                            </Col>
                        </Col>
                        <Col span={16}>
                            <Col>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('mandatoryCondition.title')}
                                </Divider>
                                <Col>
                                    <p className="fw-bolder">{translate('mandatoryCondition.subtitle1')}</p>
                                    <Radio.Group value={data ? data.typePromotion : null}>
                                        <Space direction="vertical">
                                            <Radio value={1} disabled>
                                                <span style={{ width: 120, display: 'inline-block' }}>
                                                    Tỷ lệ phần trăm
                                                </span>

                                                {(data ? data.typePromotion : null) === 1 ? (
                                                    <>
                                                        <Input
                                                            disabled
                                                            value={data ? data.discountPercent : null}
                                                            placeholder="Phần trăm giảm"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                            addonAfter={<PercentageOutlined />}
                                                        />
                                                        <Input
                                                            disabled
                                                            value={data ? data.maxDiscount : null}
                                                            className="ml-5"
                                                            placeholder="Giảm tối đa"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                            addonAfter="VND"
                                                        />
                                                    </>
                                                ) : (
                                                    <>
                                                        <Input
                                                            disabled
                                                            placeholder="Phần trăm giảm"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                            addonAfter={<PercentageOutlined />}
                                                        />
                                                        <Input
                                                            disabled
                                                            className="ml-5"
                                                            placeholder="Giảm tối đa"
                                                            style={{ width: 200, marginLeft: 10 }}
                                                            addonAfter="VND"
                                                        />
                                                    </>
                                                )}
                                            </Radio>
                                            <Radio value={2} disabled>
                                                <span style={{ width: 120, display: 'inline-block' }}>
                                                    Số tiền cố định
                                                </span>
                                                {(data ? data.typePromotion : null) === 2 ? (
                                                    <Input
                                                        disabled
                                                        value={data ? data.fixMoneyDiscount : null}
                                                        style={{ width: 410, marginLeft: 10 }}
                                                        addonAfter="VND"
                                                    />
                                                ) : (
                                                    <Input
                                                        placeholder="Số tiền tối đa"
                                                        disabled
                                                        style={{ width: 410, marginLeft: 10 }}
                                                        addonAfter="VND"
                                                    />
                                                )}
                                            </Radio>
                                        </Space>
                                    </Radio.Group>
                                    <p className="fw-bolder mt-5">{translate('mandatoryCondition.subtitle2')}</p>

                                    <Radio.Group value={data ? data.typeMaxUse : null}>
                                        <Space direction="vertical">
                                            <Radio value={1} disabled>
                                                <span style={{ width: 200, display: 'inline-block' }}>
                                                    Không giới hạn số lượt sử dụng
                                                </span>
                                            </Radio>
                                            <Radio value={2} disabled>
                                                <span style={{ width: 200, display: 'inline-block' }}>
                                                    Giới hạn số lượt sử dụng
                                                </span>
                                                {(data ? data.typeMaxUse : null) === 2 ? (
                                                    <Input
                                                        disabled
                                                        value={data ? data.maxUse : null}
                                                        placeholder="Số lượt sử dụng"
                                                        style={{ width: 330, marginLeft: 10 }}
                                                    />
                                                ) : (
                                                    <Input
                                                        disabled
                                                        placeholder="Số lượt sử dụng"
                                                        style={{ width: 330, marginLeft: 10 }}
                                                    />
                                                )}
                                            </Radio>
                                        </Space>
                                    </Radio.Group>
                                </Col>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
        </div>
    );
};

export default PromotionPreview;
