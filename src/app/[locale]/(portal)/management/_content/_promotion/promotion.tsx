'use client';
import React from 'react';
import { Badge, Button, Col, Pagination, Row, Table } from 'antd';
import { Input } from 'antd';
import PromotionPreview from './preview/promotionPreview';
import { useTranslations } from 'next-intl';
import { FilterOutlined } from '@ant-design/icons';
import PromotionCreate from './create/promotionCreate';
import AdvanceFilter from './advanceFilter/advanceFilter';
import usePromotionHook from './promotionHook';
import './promotion.scss';
import PromotionUpdate from './update/promotionUpdate';

const { Search } = Input;

const PromotionContent = () => {
    const {
        columns,
        data,
        //isTableLoading,
        // loading,
        // rowSelection,
        // hasSelected,
        // start,
        onSearch,
        openPreview,
        setOpenPreview,
        openUpdate,
        setOpenUpdate,
        idSelected,
        isLoading,
        meta,
        onChangePagination,
        setReload
    } = usePromotionHook();

    const translate = useTranslations('Management.Menu');

    const [openFilter, setOpenFilter] = React.useState<boolean>(false);

    React.useEffect(() => { }, []);

    const [openCreate, setOpenCreate] = React.useState<any>({ isCreate: false, isOpen: false });

    return (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="promotion m-0 p-0 w-100 h-100 overflow-auto">
            <Col span={24} className="d-flex flex-row mb-5 justify-content-between">
                <h2>{translate('promotion')}</h2>

                <PromotionCreate openCreate={openCreate} setOpenCreate={setOpenCreate} idSelected={idSelected} setReload={setReload} />
            </Col>

            <Col span={16} offset={3} className="d-flex flex-row">
                <Search
                    placeholder="Tìm kiếm theo mã, tên ưu đãi"
                    allowClear
                    onChange={onSearch}
                    style={{ width: '100%' }}
                    addonAfter={
                        <Badge count={4}>
                            <Button
                                className="ant-btn-icon-only ant-input-filter-button"
                                onClick={() => setOpenFilter(true)}
                            >
                                <span className="ant-btn-icon">
                                    <FilterOutlined />
                                </span>
                            </Button>{' '}
                        </Badge>
                    }
                />
            </Col>

            <Col span={24} className="d-flex flex-column">
                <div className="d-flex flex-row mb-5">
                    {/* <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                        Reload
                    </Button> */}
                </div>

                <Table
                    className="flex-fill"
                    size="small"
                    // rowSelection={rowSelection}
                    columns={columns}
                    dataSource={Array.isArray(data) ? data : []}
                    loading={isLoading}
                    //scroll={{ x: 'auto', y: 'auto' }}
                    pagination={false}
                />
            </Col>

            <Col span={24} className='p-0 mt-2 d-flex flex-row justify-content-end'>
                <Pagination
                    defaultCurrent={1}
                    current={meta?.pageNum}
                    onChange={onChangePagination}
                    defaultPageSize={meta?.pageSize}
                    total={meta?.total}
                    size='small'
                />
            </Col>

            <PromotionUpdate openUpdate={openUpdate} setOpenUpdate={setOpenUpdate} idSelected={idSelected} setReload={setReload} />

            <PromotionPreview openPreview={openPreview} setOpenPreview={setOpenPreview} idSelected={idSelected} />

            <AdvanceFilter openFilter={openFilter} setOpenFilter={setOpenFilter} />
        </Row>
    );
};

export default PromotionContent;
