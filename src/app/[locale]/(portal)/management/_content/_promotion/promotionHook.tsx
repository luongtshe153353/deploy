import React, { useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
import { useTranslations } from 'next-intl';
import { Dropdown, MenuProps, Modal, Popconfirm, Space, Switch, Tooltip, Typography, message } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { promotionRepository } from './promotionRepository';
import useSWR from 'swr';

interface DataType {
    key: React.Key;
    id: number | string;
    code: string;
    name: string;
    startDate: Date;
    endDate: Date;
    status: string;
}

const usePromotionHook: any = () => {
    //const [selectedRowKeys, setSelectedRowKeys] = React.useState<React.Key[]>([]);
    //const [loading, setLoading] = React.useState(false);
    const translate = useTranslations('Management.Promotion.listScreen');

    const initialPayload = {
        "searchPaging": {
            "pageNum": 1,
            "pageSize": 10,
        },
        "searchParams": ""
    }
    const [payload, setPayload] = React.useState(initialPayload)
    const [resp, setResp] = React.useState<any>({})
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const [reload, setReload] = React.useState<boolean>(false)

    React.useEffect(() => {
        const loadingList = new Promise((resolve, reject) => {

            setIsLoading(true)
            resolve(promotionRepository.searchPromotion(payload).then((result) => {
                setIsLoading(false)
                setResp(result)
                setReload(false)
            }))

            reject(setIsLoading(false))
        })

    }, [payload, reload])

    const meta = React.useMemo(() => {
        if (resp?.meta) {
            return resp.meta
        }
    }, [resp])

    const data: DataType[] = React.useMemo(() => {
        if (resp?.data) {
            return resp.data
        }
    }, [resp])

    const onChangePagination = (page: number) => {
        setPayload((prev: any) => ({
            ...prev, searchPaging: {
                ...prev?.searchPaging,
                pageNum: page
            }
        }))
    }

    const [switchStates, setSwitchStates]: [any, any] = useState({});

    React.useEffect(() => {
        if (!isLoading && data) {

            const newSwitchStateContainer: any = {};
            data.forEach((item) => {
                if (item.status == "Đang hoạt động") {
                    newSwitchStateContainer[`switch${item.id}`] = true;
                } else {
                    newSwitchStateContainer[`switch${item.id}`] = false;
                }
            });

            setSwitchStates(newSwitchStateContainer);
        }
    }, [isLoading, data]);

    const handleSwitchChange = (switchName: string, id: any, status: any) => {
        const onConfirm = () => {

            const newSwitchStates: any = { ...switchStates };
            newSwitchStates[switchName] = status == "Đang hoạt động" ? false : true;
            setSwitchStates(newSwitchStates);

            const payload = {
                id: id,
                status: status == "Đang hoạt động" ? "Tạm dừng" : "Đang hoạt động"
            }

            promotionRepository.updateStatus(payload);

            data.forEach(item => {
                if (item.id == id) {
                    item.status = item.status == "Đang hoạt động" ? "Tạm dừng" : "Đang hoạt động"
                }
            })

            message.success('Đã thay đổi trạng thái');
        };

        const onCancel = () => {
            message.info(`Đã hủy thay đổi trạng thái`);
        };

        const confirmContent = `Bạn có chắc chắn muốn thay đổi trạng thái?`;

        return (
            <Popconfirm
                title={confirmContent}
                onConfirm={onConfirm}
                onCancel={onCancel}
                okText="Xác nhận"
                cancelText="Hủy bỏ"
            >
                <Switch size="small" checked={switchStates[switchName]} />
            </Popconfirm>
        );
    };


    // const start = () => {
    //     setLoading(true);
    //     // ajax request after empty completing
    //     setTimeout(() => {
    //         setSelectedRowKeys([]);
    //         setLoading(false);
    //     }, 1000);
    // };

    // const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    //     setSelectedRowKeys(newSelectedRowKeys);
    // };

    // const rowSelection = {
    //     selectedRowKeys,
    //     onChange: onSelectChange,
    //     columnWidth: 40,
    // };
    // const hasSelected = selectedRowKeys.length > 0;

    // Search
    const onSearch = (event: any) => {
        setPayload((prev: any) => ({ ...prev, searchParams: event?.target?.value }))
    }

    // Drawer
    //const [openCreate, setOpenCreate] = React.useState<any>({ isCreate: false, isOpen: false });
    const [openUpdate, setOpenUpdate] = React.useState<any>({ isUpdate: false, isOpen: false });
    const [openPreview, setOpenPreview] = React.useState<boolean>(false);
    const [idSelected, setIdSelected] = React.useState<number | string>(0);

    // Table

    const Action = React.useCallback((id: number | string, isUpdate: boolean) => {
        let items: MenuProps['items'] = [
            {
                key: '1',
                label: translate('preview'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenPreview(true);
                },
            },
            {
                key: '2',
                label: translate('update'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenUpdate({ isUpdate: true, isOpen: true });
                },
            },
        ];

        if (!isUpdate) {
            items = [
                {
                    key: '1',
                    label: translate('preview'),
                    onClick: () => {
                        setIdSelected(id);
                        setOpenPreview(true);
                    },
                },
            ]
        }

        return (
            <Dropdown
                menu={{
                    items,
                    selectable: true,
                    defaultSelectedKeys: undefined,
                }}
                trigger={['click']}
            >
                <Typography.Link>
                    <Space>
                        {translate('selectable')}
                        <DownOutlined />
                    </Space>
                </Typography.Link>
            </Dropdown>
        );
    }, []);

    const columns: ColumnsType<DataType> = [
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('code')}</div>;
            },
            key: 'code',
            dataIndex: 'code',
            ellipsis: true,
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('promotionName')}</div>;
            },
            key: 'name',
            dataIndex: 'name',
            ellipsis: true,
            width: 200,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('startDate')}</div>;
            },
            key: 'startDate',
            dataIndex: 'startDate',
            ellipsis: true,
            width: 150,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('endDate')}</div>;
            },
            key: 'endDate',
            dataIndex: 'endDate',
            ellipsis: true,
            width: 150,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('status')}</div>;
            },
            key: 'status',
            dataIndex: 'status',
            ellipsis: true,
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span style={params[0] == 'Đang hoạt động'
                            ? { color: '#00B96B' }
                            : params[0] == 'Tạm dừng'
                                ? { color: "#F2BD27" }
                                : { color: "#E0282E" }}>
                            {params[0]}
                        </span>
                    </Tooltip>
                );
            },
        },
        {
            title: '',
            key: 'toggleStatus',
            width: 50,
            render: (...params: [string, DataType, number]) => {
                if (params[1].status == "Hết hiệu lực") {
                    return <div></div>
                } else {
                    return <div className="text-center">{handleSwitchChange(`switch${params[1].id}`, params[1].id, params[1].status)}</div>;
                }
            },
        },
        {
            title: '',
            key: 'Actions',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return Action(params[1]?.id, params[1]?.status === "Hết hiệu lực" ? false : true);
            },
        }
    ];

    return {
        columns,
        data,
        //loading,
        // isTableLoading,
        // rowSelection,
        // hasSelected,
        // selectedRowKeys,
        // start,
        onSearch,
        openPreview,
        setOpenPreview,
        openUpdate,
        setOpenUpdate,
        isLoading,
        idSelected,
        meta,
        onChangePagination,
        setReload
    };
};

export default usePromotionHook;
