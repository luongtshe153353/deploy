import { Repository } from '@/config/repository';
import { kebabCase, result } from 'lodash';

export const API_PROMOTION_PREIX: string = 'promotion/';

export class PromotionRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_PROMOTION_PREIX;
    }

    public searchPromotion = (payload?: any) =>
        this.post(kebabCase('list'), payload).then((result) => {
            return result;
        });

    public createPromotion = (payload?: any) =>
        this.postFormData(kebabCase('create'), payload).then((result) => {
            return result;
        });

    public getPromotion = (payload?: any) =>
        this.get('' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });

    public updatePromotion = (payload?: any) =>
        this.post('update', payload).then((result) => {
            return result;
        });
}
export const promotionRepository = new PromotionRepository();
