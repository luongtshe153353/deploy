'use client';
import React, { useState } from 'react';
import { PercentageOutlined } from '@ant-design/icons';
import {
    Button,
    Col,
    DatePicker,
    Divider,
    Drawer,
    Form,
    Image,
    Input,
    InputNumber,
    Radio,
    Row,
    Space,
} from 'antd';
import { useTranslations } from 'next-intl';
import type { RadioChangeEvent } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { TravelBooContext } from '@/app/[locale]/(portal)/layout';
import { promotionRepository } from '../promotionRepository';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);
const dateFormat = 'YYYY-MM-DD';

interface PromotionUpdateProps {
    idSelected?: number | string;
    openUpdate: any;
    setOpenUpdate: (b: any) => void;
    setReload: (b: any) => void;
}

interface DataType {
    key: React.Key;
    id: number | string;
    code: string;
    name: string | null;
    imageUrl: string | undefined;
    description: string | null;
    startDate: string | null;
    endDate: string | null;
    typePromotion: any;
    discountPercent: any;
    maxDiscount: any;
    fixMoneyDiscount: any
    typeMaxUse: any;
    maxUse: any;
    status: string | null;
}

const PromotionUpdate = (props: PromotionUpdateProps): React.ReactNode => {
    const { idSelected, openUpdate, setOpenUpdate, setReload } = props;

    const translate = useTranslations('Management.Promotion.updateScreen');

    const onClose = () => {
        setOpenUpdate({
            isUpdate: false,
            isOpen: false,
        });
    };

    const { RangePicker } = DatePicker;

    // submit form
    const [form] = Form.useForm();
    const { openNotification } = React.useContext(TravelBooContext)

    const onFinishUpdate = (values: any) => {

        let inputDate = new Date(values.time[0]);
        let year = inputDate.getFullYear();
        let month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed, so add 1.
        let day = inputDate.getDate().toString().padStart(2, '0');
        let formattedStartDate = `${year}-${month}-${day}`;

        inputDate = new Date(values.time[1]);
        year = inputDate.getFullYear();
        month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed, so add 1.
        day = inputDate.getDate().toString().padStart(2, '0');
        let formattedEndDate = `${year}-${month}-${day}`;

        const payload = {
            id: idSelected,
            name: values.name,
            description: values.description,
            startDate: formattedStartDate,
            endDate: formattedEndDate,
            typePromotion: values.typePromotion,
            discountPercent: values.discountPercent,
            maxDiscount: values.maxDiscount,
            fixMoneyDiscount: values.fixMoneyDiscount,
            typeMaxUse: values.typeMaxUse,
            maxUse: values.maxUse
        }

        return promotionRepository.updatePromotion(payload).then((result: any) => {
            if (result?.code === 200) {
                openNotification('success', "Thành công", "Cập nhật thành công")
            } else {
                openNotification('error', "Thất bại", "Cập nhật thất bại")
            }
        })
    }

    const handleSubmit = React.useCallback(() => {

        form.validateFields().then((values) => {
            onFinishUpdate(values).then(() => {
                setReload(true);
                onClose()
            });
        })
            .catch((error) => {
                console.log(error)
            });

    }, [form, onClose, onFinishUpdate])

    const [data, setData] = React.useState<any>(null);
    const [isLoading, setIsLoading] = React.useState(false);

    const fetchPromotionData = async () => {
        setIsLoading(true);
        try {
            const result = await promotionRepository.getPromotion(idSelected);
            setData(result);
            form.setFieldsValue({  // Set form field values with data
                code: result.code,
                name: result.name,
                description: result.description,
                time: [dayjs(result.startDate), dayjs(result.endDate)],
                // date in here
                typePromotion: result.typePromotion,
                discountPercent: result.discountPercent,
                maxDiscount: result.maxDiscount,
                fixMoneyDiscount: result.fixMoneyDiscount,
                typeMaxUse: result.typeMaxUse,
                maxUse: result.maxUse
            });
            setValueLimitUse(result.typePromotion);
            setValueTypeSale(result.typeMaxUse);
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoading(false);
        }
    };

    React.useEffect(() => {
        if (openUpdate?.isOpen) {
            fetchPromotionData();
        }
    }, [idSelected]);

    const [valueLimitUse, setValueLimitUse] = useState<number | null>(null);
    const [valueTypeSale, setValueTypeSale] = useState<number | null>(null);

    const onChangeLimitUse = (e: RadioChangeEvent) => {
        setValueLimitUse(e.target.value);
    };

    const onChangeTypeSale = (e: RadioChangeEvent) => {
        setValueTypeSale(e.target.value);
    };

    return (
        <div>
            <Drawer
                title={translate('title')}
                width={1230}
                onClose={onClose}
                open={openUpdate?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button htmlType="submit" form="myForm" onClick={handleSubmit} type="primary">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                {isLoading ? (
                    // Display a loading indicator here
                    <div>Loading...</div>
                ) : (
                    <Form
                        id="myForm"
                        labelCol={{ span: 5 }}
                        layout="horizontal"
                        hideRequiredMark
                        // onFinish={onFinish}
                        // onFinishFailed={onFinishFailed}
                        form={form}
                    >
                        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col span={8}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col>
                                    <Form.Item
                                        colon={false}
                                        name="code"
                                        label={translate('info.code')}
                                    >
                                        <Input disabled defaultValue={data ? data.code : ""} />
                                    </Form.Item>
                                    <Form.Item
                                        colon={false}
                                        name="name"
                                        label={translate('info.name')}
                                        rules={[{ required: true, message: 'Vui lòng nhập tên khuyến mãi' }]}
                                    >
                                        <Input defaultValue={data ? data.name : "asd"} />
                                    </Form.Item>
                                    <Form.Item label={translate('info.image')} colon={false}>
                                        <Image
                                            style={{ borderRadius: '15px' }}
                                            width={'100%'}
                                            preview={false}
                                            src={data ? data.imageUrl : ''}

                                        />
                                    </Form.Item>

                                    <Form.Item
                                        colon={false}
                                        name="description"
                                        label={translate('info.description')}
                                        rules={[{ required: true, message: 'Vui lòng nhập mô tả khuyến mãi' }]}
                                    >
                                        <TextArea rows={4} defaultValue={data ? data.description : ""} />
                                    </Form.Item>
                                    <Form.Item
                                        colon={false}
                                        name="time"
                                        label={translate('info.date')}
                                    >
                                        <RangePicker
                                            defaultValue={[
                                                dayjs(data ? data.startDate : "2001/06/30", dateFormat),
                                                dayjs(data ? data.endDate : "2001/11/26", dateFormat)
                                            ]}
                                            format={dateFormat} />
                                    </Form.Item>
                                </Col>
                            </Col>
                            <Col span={16}>
                                <Col>
                                    <Divider orientation="left" orientationMargin={5}>
                                        {translate('mandatoryCondition.title')}
                                    </Divider>
                                    <Col>
                                        <p className="fw-bolder">{translate('mandatoryCondition.subtitle1')}</p>
                                        <Form.Item name="typePromotion">
                                            <Radio.Group onChange={onChangeTypeSale} value={valueTypeSale}>
                                                <Space direction="vertical">
                                                    <span className="d-flex align-items-start" style={{ height: '50px' }}>
                                                        <Radio value={1} defaultChecked={valueTypeSale === 1} className="align-self-start">
                                                            <span className="d-flex" style={{ height: '30px' }}>
                                                                <span className="align-self-center" style={{ width: 120 }}>
                                                                    Tỷ lệ phần trăm
                                                                </span>
                                                                {valueTypeSale === 1 ? (
                                                                    <span className="align-self-center d-flex" style={{ height: '30px' }}>
                                                                        <Form.Item
                                                                            name="discountPercent"
                                                                            rules={[
                                                                                {
                                                                                    required: true,
                                                                                    message: 'Vui lòng nhập phần trăm giảm',
                                                                                },
                                                                            ]}
                                                                        >
                                                                            <span className="align-self-center">
                                                                                <InputNumber
                                                                                    min={1}
                                                                                    max={100}
                                                                                    defaultValue={data ? data.discountPercent : null}
                                                                                    addonAfter={<PercentageOutlined />}
                                                                                    className="align-self-center"
                                                                                    name="discountPercent"
                                                                                    placeholder="Phần trăm giảm"
                                                                                    style={{ width: 200, marginLeft: 10 }}
                                                                                />
                                                                            </span>
                                                                        </Form.Item>
                                                                        <Form.Item name="maxDiscount">
                                                                            <span className="align-self-center">
                                                                                <InputNumber
                                                                                    min={0}
                                                                                    addonAfter="VND"
                                                                                    className="ml-5"
                                                                                    placeholder="Giảm tối đa"
                                                                                    defaultValue={data ? data.maxDiscount : null}
                                                                                    style={{ width: 200, marginLeft: 10 }}
                                                                                />
                                                                            </span>
                                                                        </Form.Item>
                                                                    </span>
                                                                ) : (
                                                                    <span className="align-self-center d-flex" style={{ height: '33px' }}>
                                                                        <InputNumber
                                                                            disabled
                                                                            addonAfter={<PercentageOutlined />}
                                                                            className="align-self-center"
                                                                            placeholder="Phần trăm giảm"
                                                                            style={{ width: 200, marginLeft: 10 }}
                                                                        />
                                                                        <InputNumber
                                                                            disabled
                                                                            addonAfter="VND"
                                                                            className="ml-5"
                                                                            placeholder="Giảm tối đa"
                                                                            style={{ width: 200, marginLeft: 10 }}
                                                                        />
                                                                    </span>
                                                                )}
                                                            </span>
                                                        </Radio>
                                                    </span>

                                                    <Radio value={2} checked={valueTypeSale == 2}>
                                                        <span className="d-flex">
                                                            <span className="align-self-center" style={{ width: 120 }}>
                                                                Số tiền cố định
                                                            </span>
                                                            {valueTypeSale === 2 ? (
                                                                <span
                                                                    className="align-self-center d-flex"
                                                                    style={{ height: '30px' }}
                                                                >
                                                                    <Form.Item
                                                                        name="fixMoneyDiscount"
                                                                        rules={[
                                                                            {
                                                                                required: true,
                                                                                message: 'Vui lòng nhập số tiền được giảm',
                                                                            },
                                                                        ]}
                                                                    >
                                                                        <InputNumber
                                                                            defaultValue={data ? data.fixMoneyDiscount : null}
                                                                            addonAfter="VND"
                                                                            placeholder="Số tiền tối đa"
                                                                            style={{ width: 410, marginLeft: 10 }}
                                                                        />
                                                                    </Form.Item>
                                                                </span>
                                                            ) : (
                                                                <InputNumber
                                                                    addonAfter="VND"
                                                                    placeholder="Số tiền tối đa"
                                                                    disabled
                                                                    style={{ width: 410, marginLeft: 10 }}
                                                                />
                                                            )}
                                                        </span>
                                                    </Radio>
                                                </Space>
                                            </Radio.Group>
                                        </Form.Item>

                                        <p className="fw-bolder mt-5">{translate('mandatoryCondition.subtitle2')}</p>
                                        <Form.Item name="typeMaxUse">
                                            <Radio.Group onChange={onChangeLimitUse} value={valueLimitUse}>
                                                <Space direction="vertical">
                                                    <span className="d-flex align-items-start" style={{ height: '40px' }}>
                                                        <Radio value={1} checked={valueLimitUse == 1}>
                                                            <span style={{ width: 200, display: 'inline-block' }}>
                                                                Không giới hạn số lượt sử dụng
                                                            </span>
                                                        </Radio>
                                                    </span>
                                                    <span className="d-flex" style={{ height: '30px' }}>
                                                        <Radio value={2} checked={valueLimitUse == 2}>
                                                            <span className="d-flex ">
                                                                <span
                                                                    className="align-items-center"
                                                                    style={{ width: 200, lineHeight: '30px' }}
                                                                >
                                                                    Giới hạn số lượt sử dụng
                                                                </span>

                                                                {valueLimitUse === 2 ? (
                                                                    <Form.Item
                                                                        name="maxUse"
                                                                        rules={[
                                                                            {
                                                                                required: true,
                                                                                message: 'Vui lòng nhập số lượt sử dụng',
                                                                            },
                                                                        ]}
                                                                        initialValue={1}
                                                                    >
                                                                        <InputNumber
                                                                            min={1}
                                                                            defaultValue={data.number}
                                                                            placeholder="Số lượt sử dụng"
                                                                            style={{ width: 330, marginLeft: 10 }}
                                                                        />
                                                                    </Form.Item>
                                                                ) : (
                                                                    <InputNumber
                                                                        disabled
                                                                        placeholder="Số lượt sử dụng"
                                                                        style={{ width: 330, marginLeft: 10 }}
                                                                    />
                                                                )}
                                                            </span>
                                                        </Radio>
                                                    </span>
                                                </Space>
                                            </Radio.Group>
                                        </Form.Item>

                                    </Col>
                                </Col>
                            </Col>
                        </Row>
                    </Form>)}
            </Drawer>
        </div>
    );
};

export default PromotionUpdate;
