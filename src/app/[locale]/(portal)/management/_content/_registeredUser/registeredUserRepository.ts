import { Repository } from '@/config/repository';

export const API_REGISTERED_USER_PREIX: string = 'registered-users/';

export class RegisteredUserRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_REGISTERED_USER_PREIX;
    }

    public searchRegisteredUser = (payload?: any) => this.post('get-registered-user', payload);

    public getRegisteredUser = (payload?: any) =>
        this.get('get-registered-user/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const registeredUserRepository = new RegisteredUserRepository();
