'use client';
import React, { useState } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import { LoadingOutlined, MinusCircleOutlined, PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Drawer, Form, Input, Row, Select, Space, Rate, Checkbox, message, Upload, Spin, InputNumber, TreeSelect } from 'antd';
import { useTranslations } from 'next-intl';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import 'react-quill/dist/quill.snow.css';
import TextArea from 'antd/es/input/TextArea';
import useValidate from '@/utilities/validate';
import useRoomCreateHook from './roomCreateHook';
import unidecode from 'unidecode';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const { Option, OptGroup } = Select;

interface RoomCreateProps {
    idSelected?: number | string;
    openCreate: any;
    setOpenCreate: (b: any) => void;
    setIsReload: (b: any) => void;
}

interface InputValues {
    [key: string]: number | null;
}

interface Item {
    value: number;
    name: string;
}

const RoomCreate = (props: RoomCreateProps): React.ReactNode => {
    const { idSelected, openCreate, setOpenCreate, setIsReload } = props;
    const [selectedItem, setSelectedItem] = useState<Item | null>(null);
    const [inputValues, setInputValues] = useState<InputValues>({});
    const [showInput, setShowInput] = useState<boolean>(false);
    React.useEffect(() => {
        handleBedTypes();
        handleRoomServices();
    }, [])
    const [form] = Form.useForm();
    const {
        onFinish,
        handleBedTypes,
        bedTypes,
        handleRoomTypes,
        roomTypes,
        handleRoomServices,
        roomServices
    } = useRoomCreateHook({ form })
    // const {
    //     provinceRule,
    //     districtRule,
    //     wardRule,
    //     hotelNameRule,
    //     starRatingRule,
    //     boRule,
    //     taxCodeRule,
    //     descriptionRule,
    //     servicesRule,
    //     imagesRule,
    // } = useValidate()

    const [valueTreeSelect, setValueTreeSelect] = useState([]);

    const handleAdd = () => {
        if (selectedItem !== null) {
            setInputValues({ ...inputValues, [`${selectedItem.value}`]: null });
            setSelectedItem(null);
            setShowInput(false);
        }
    };

    const handleRemove = (item: number) => {
        const updatedInputValues = { ...inputValues };
        delete updatedInputValues[`${item}`];
        setInputValues(updatedInputValues);
    };

    const items: Item[] = [
        { value: 3, name: 'Giường King' },
        { value: 1, name: 'Giường đơn' },
        { value: 2, name: 'Giường đôi' },
    ];

    const availableItems = items.filter((item) => !Object.keys(inputValues).includes(`${item.value}`));

    const onChangeTreeSelect = (newValue: any) => {
        setValueTreeSelect(newValue);
    };

    const normalizeString = (str: any) => {
        return unidecode(str).toLowerCase();
    };

    const filterTreeNode = (input: any, treeNode: any) => {
        const normalizedInput = normalizeString(input);
        const normalizedNodeTitle = normalizeString(treeNode.props.title);

        return normalizedNodeTitle.includes(normalizedInput);
    };


    const [fileList, setFileList] = useState([]);

    const listScreen = useTranslations('Management.Room.listScreen');
    const translate = useTranslations('Management.Room.addScreen');

    const showDrawer = () => {
        setOpenCreate((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = React.useCallback(() => {
        setOpenCreate({
            isDetail: false,
            isOpen: false,
        });
    }, [setOpenCreate]);

    const handleUpload = () => {
        if (fileList.length !== 3) {
            message.error('Vui lòng tải lên đủ 3 ảnh.');
            return;
        }
        // Thực hiện xử lý tải lên ở đây nếu có 3 ảnh
        // Nếu muốn thực hiện tải lên, bạn có thể gọi một hàm API hoặc làm gì đó với các file trong fileList.
    };

    const onChange = ({ file, fileList }: { file: any; fileList: any }) => {
        setFileList(fileList);
    };

    const handleSubmit = React.useCallback(() => {
        form.validateFields().then((values) => {
            onFinish(values).then(() => {
                onClose()
            });
        });

    }, [form, onClose, onFinish])

    const AllBedTypes = () => bedTypes.map((item: any) => {
        return (
            <Col key={item.id} span={24} >
                <Form.Item
                    labelAlign="right"
                    name={`giuong_${item.id}`}
                    label={item.name}
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập số người ở tối đa',
                        },
                    ]}
                >
                    <InputNumber
                        addonAfter='giường'
                        min={0}
                        defaultValue={0}
                        placeholder="Nhập số lượng giường"
                        style={{ width: '100%' }}
                    />
                </Form.Item>
            </Col>
        )
    })

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={onClose}
                open={openCreate?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button onClick={handleSubmit} type="primary" htmlType="submit" className="login-form-button">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form
                            form={form}
                            layout="vertical"
                            hideRequiredMark
                        >
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item
                                        name="name"
                                        label={translate('info.roomName')}
                                        rules={[{ required: true, message: 'Vui lòng nhập tên phòng' }]}
                                    >
                                        <Input placeholder="Nhập tên phòng" />
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item
                                        name="roomCode"
                                        label={translate('info.roomCode')}
                                        rules={[{ required: true, message: 'Vui lòng nhập mã phòng' }]}
                                    >
                                        <Input placeholder="Nhập mã phòng" />
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item
                                        name="roomCount"
                                        label={translate('info.roomQuantity')}
                                        rules={[{ required: true, message: 'Vui lòng nhập số lượng phòng' }]}
                                    >
                                        <InputNumber
                                            min={1}
                                            defaultValue={1}
                                            placeholder="Nhập số lượng phòng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item
                                        name="roomSize"
                                        label={translate('info.roomSize')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập kích thước phòng',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='m&sup2;'
                                            min={1}
                                            defaultValue={1}
                                            placeholder="Nhập kích thước phòng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={13}>
                                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                        <Col span={15}>
                                            <Form.Item
                                                labelAlign="right"
                                                name="roomTypeId"
                                                label={translate('info.roomType')}
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Vui lòng chọn 1 loại phòng',
                                                    },
                                                ]}
                                            >
                                                <Select
                                                    showSearch
                                                    placeholder="Chọn 1 loại phòng"
                                                    optionFilterProp="children"
                                                    onClick={handleRoomTypes}
                                                    options={roomTypes?.length > 0 ? roomTypes?.map((item: any) => ({
                                                        value: item?.id,
                                                        label: item?.name
                                                    })) : [{
                                                        value: null,
                                                        label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                                    }]}
                                                    virtual={true}
                                                    placement='bottomLeft'
                                                />
                                            </Form.Item>
                                        </Col>
                                        <Col span={9}>
                                            <Form.Item
                                                labelAlign="right"
                                                name="maxPeopleStay"
                                                label={translate('info.maxPeople')}
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Vui lòng nhập số người ở tối đa',
                                                    },
                                                ]}
                                            >
                                                <InputNumber
                                                    min={1}
                                                    defaultValue={1}
                                                    placeholder="Nhập số lượng người ở tối đa"
                                                    style={{ width: '100%' }}
                                                />
                                            </Form.Item>
                                        </Col>
                                        {/* <AllBedTypes></AllBedTypes> */}
                                        <Col span={24}>
                                            <Form.Item label="Loại giường" name="item">
                                                <Select
                                                    onChange={(value) => setSelectedItem(items.find((item) => item.value === value) || null)}
                                                    value={selectedItem?.value}
                                                >
                                                    {items.map((item) => (
                                                        <Option key={item.value} value={item.value} disabled={Object.keys(inputValues).includes(`${item.value}`)}>
                                                            {item.name}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            </Form.Item>
                                            <Form.Item>
                                                <Button type="primary" onClick={handleAdd} disabled={selectedItem === null}>
                                                    Thêm
                                                </Button>
                                            </Form.Item>
                                            {showInput && selectedItem !== null && (
                                                <Form.Item label={selectedItem.name} name={`${selectedItem.value}`}>
                                                    <InputNumber
                                                        onChange={(value) => setInputValues({ ...inputValues, [`${selectedItem.value}`]: value })}
                                                        value={inputValues[`${selectedItem.value}`] ?? undefined}
                                                    />
                                                    {/* <MinusCircleOutlined
                                                        className="dynamic-delete-button"
                                                        onClick={() => handleRemove(selectedItem.value)}
                                                    /> */}
                                                </Form.Item>
                                            )}
                                            {Object.entries(inputValues).map(([item, value]) => {
                                                const itemValue = parseInt(item, 10);
                                                return (
                                                    <Form.Item key={itemValue} label={items.find((i) => i.value === itemValue)?.name} name={item + ''}>
                                                        <InputNumber
                                                            onChange={(newValue) => setInputValues({ ...inputValues, [item]: newValue })}
                                                            value={value ?? undefined}
                                                        />
                                                        {/* <MinusCircleOutlined
                                                            className="dynamic-delete-button"
                                                            onClick={() => handleRemove(itemValue)}
                                                        /> */}
                                                    </Form.Item>
                                                );
                                            })}
                                        </Col>
                                    </Row>
                                </Col>
                                <Col span={11}>
                                    <Form.Item name="images" label={translate('info.image')} valuePropName="fileList">
                                        <Upload
                                            action="/"
                                            beforeUpload={() => false}
                                            onChange={onChange}
                                            fileList={fileList}
                                            listType="picture-card"
                                            accept="image/png, image/jpeg"
                                            maxCount={3}
                                        >
                                            {fileList.length != 3 ? (
                                                <div>
                                                    <PlusOutlined />
                                                    <div style={{ marginTop: 8 }}>Thêm ảnh</div>
                                                </div>
                                            ) : (
                                                ''
                                            )}
                                        </Upload>
                                        <Button type="primary" onClick={handleUpload}>
                                            Kiểm tra và Tải lên
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24}>
                                    <Form.Item
                                        name="listService"
                                    // rules={servicesRule}
                                    >
                                        <TreeSelect
                                            treeData={roomServices?.map((item: any) => ({
                                                title: item?.roomServiceTypeName,
                                                value: 'parentValue' + item?.roomServiceTypeId,
                                                key: 'parentKey' + item?.roomServiceTypeId,
                                                children: item?.listRoomService?.map((itemChild: any) => ({
                                                    title: itemChild?.name,
                                                    value: itemChild?.id,
                                                    key: itemChild?.id,
                                                }))
                                            }))}
                                            value={valueTreeSelect}
                                            filterTreeNode={filterTreeNode}
                                            onChange={onChangeTreeSelect}
                                            treeCheckable={true}
                                            placeholder='Chọn dịch vụ của phòng'
                                            style={{
                                                width: '100%',
                                            }}
                                        ></TreeSelect>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('price.title')}
                                </Divider>
                                <Col span={12}>
                                    <Form.Item
                                        name="defaultPrice"
                                        label={translate('price.defaultPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá mặc định',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá mặc định"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="minPrice"
                                        label={translate('price.minPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá thấp nhất',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá thấp nhất"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="weekPrice"
                                        label={translate('price.weekPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá theo tuần',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá theo tuần"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="monthPrice"
                                        label={translate('price.monthPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá theo tháng',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá theo tháng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalAdultFee"
                                        label={translate('price.additionalAdultFee')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập phụ phí thêm 1 người lớn',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập phụ phí thêm 1 người lớn"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalChildFee"
                                        label={translate('price.additionalChildFee')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập phụ phí thêm 1 trẻ em',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập phụ phí thêm 1 trẻ em"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row >
            </Drawer >
        </div >
    );
};

export default RoomCreate;
