import { Repository } from "@/config/repository"
import React from "react"
import { FormInstance } from "antd"
import { TravelBooContext } from "@/app/[locale]/(portal)/layout"
import { useTranslations } from "next-intl"
import { roomRepository } from "../roomRepository"

interface useRoomCreateHookProps {
    form?: FormInstance<any>
}

const useRoomCreateHook = (props: useRoomCreateHookProps) => {

    const { form } = props
    const { openNotification } = React.useContext(TravelBooContext)

    const translate = useTranslations('General.notification.create')

    const onFinish = React.useCallback((values: any) => {
        const payload = values;

        console.log(values);

        return roomRepository.createRoom(payload).then((result: any) => {
            if (result?.code === 200) {
                openNotification('success', translate('success'))
            } else {
                openNotification('error', translate('error'), translate('errorDescription'))
            }
        })
    }, [openNotification, translate])

    // handle bed type
    const [bedTypes, setBedTypes] = React.useState([])
    const handleBedTypes = React.useCallback(() => {
        new Repository().post("room/get-all-bed-type", null).then((result) => {
            if (Array.isArray(result?.listData)) {
                setBedTypes(result?.listData)
            }
        })
    }, [])

    // handle room type
    const [roomTypes, setRoomTypes] = React.useState([])
    const handleRoomTypes = React.useCallback(() => {
        new Repository().post("room/get-all-room-type", null).then((result) => {
            if (Array.isArray(result?.listData)) {
                setRoomTypes(result?.listData)
            }
        })
    }, [])

    // handle room services
    const [roomServices, setRoomServices] = React.useState([])
    const handleRoomServices = React.useCallback(() => {
        new Repository().get("service/list-service-room").then((result) => {
            if (Array.isArray(result?.data)) {
                setRoomServices(result?.data)
            }
        })
    }, [])

    return {
        onFinish,
        handleBedTypes,
        bedTypes,
        handleRoomTypes,
        roomTypes,
        handleRoomServices,
        roomServices
    }
}

export default useRoomCreateHook