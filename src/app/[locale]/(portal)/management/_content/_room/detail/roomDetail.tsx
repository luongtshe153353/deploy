'use client';
import React, { useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Drawer, Form, Input, Row, Select, Space, Checkbox, message, Upload, InputNumber, TreeSelect } from 'antd';
import { useTranslations } from 'next-intl';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import 'react-quill/dist/quill.snow.css';
import Unidecode from 'unidecode';

interface RoomDetailProps {
    idSelected?: number | string;
    openDetail: any;
    setOpenDetail: (b: any) => void;
}

const { SHOW_PARENT } = TreeSelect;

const treeData = [
    {
        title: 'Giải trí',
        value: '1',
        key: '1',
        children: [
            {
                title: 'Phim theo yêu cầu',
                value: '1-1',
                key: '1-1',
            },
            {
                title: 'Truy cập Internet - không dây',
                value: '1-2',
                key: '1-2',
            },
            {
                title: 'Thiết bị chơi điện tử',
                value: '1-3',
                key: '1-3',
            },
        ],
    },
    {
        title: 'Bài trí và đồ đạc',
        value: '2',
        key: '2',
        children: [
            {
                title: 'Ghế sofa',
                value: '2-1',
                key: '2-1',
            },
            {
                title: 'Khu vực tiếp khách',
                value: '2-2',
                key: '2-2',
            },
            {
                title: 'Lò sưởi',
                value: '2-3',
                key: '2-3',
            },
        ],
    },
];

const TreeServices: React.FC = () => {
    const [value, setValue] = useState([]);

    const onChange = (newValue: any) => {
        console.log('onChange ', value);
        setValue(newValue);
    };

    const normalizeString = (str: any) => {
        return Unidecode(str).toLowerCase();
    };

    const filterTreeNode = (input: any, treeNode: any) => {
        const normalizedInput = normalizeString(input);
        const normalizedNodeTitle = normalizeString(treeNode.props.title);

        return normalizedNodeTitle.includes(normalizedInput);
    };

    const tProps = {
        treeData,
        value,
        filterTreeNode,
        onChange,
        treeCheckable: true,
        showCheckedStrategy: SHOW_PARENT,
        placeholder: 'Chọn dịch vụ của phòng',
        style: {
            width: '100%',
        },
    };

    return <TreeSelect {...tProps} />;
};

const RoomDetail = (props: RoomDetailProps): React.ReactNode => {
    var plainOptions = [
        { label: 'Dọn phòng hàng ngày', value: 'donphong' },
        { label: 'Đưa đón đến sân bay', value: 'duadon' },
        { label: 'Giặt là', value: 'giat' },
        { label: 'Wifi riêng mỗi phòng', value: 'wifi' },
    ];

    const [checkedList, setCheckedList] = useState<CheckboxValueType[]>([]);
    const { idSelected, openDetail, setOpenDetail } = props;
    const [valueContent, setValueContent] = useState('');
    const [note, setNote] = useState('');
    const [fileList, setFileList] = useState([]);

    const listScreen = useTranslations('Management.Room.listScreen');
    const translate = useTranslations(
        openDetail?.isDetail
            ? 'Management.Room.detailScreen'
            : 'Management.Room.addScreen',
    );

    const showDrawer = () => {
        setOpenDetail((prev: any) => ({
            ...prev,
            isOpen: true,
        }));
    };

    const onClose = () => {
        setOpenDetail({
            isDetail: false,
            isOpen: false,
        });
    };

    const handleUpload = () => {
        if (fileList.length !== 3) {
            message.error('Vui lòng tải lên đủ 3 ảnh.');
            return;
        }
        // Thực hiện xử lý tải lên ở đây nếu có 3 ảnh
        // Nếu muốn thực hiện tải lên, bạn có thể gọi một hàm API hoặc làm gì đó với các file trong fileList.
    };

    const onChange = ({ file, fileList }: { file: any; fileList: any }) => {
        setFileList(fileList);
    };

    const handleTextareaChange = (event: any) => {
        setNote(event.target.value);
    };

    let data: any;
    const onFinish = (values: any) => {
        data = {
            hotelName: values.hotelName,
            starRating: values.starRating,
            bo: values.bo,
            taxCode: values.taxCode,
            images: fileList,
            province: values.province,
            district: values.district,
            ward: values.ward,
            houseNumber: values.houseNumber,
            alleyNumber: values.alleyNumber,
            services: checkedList,
            description: valueContent,
            note: note,
        };
        console.log(data);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
                {listScreen('button.add')}
            </Button>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={onClose}
                open={openDetail?.isOpen}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={onClose}>{translate('button.cancel')}</Button>
                        <Button htmlType="submit" form="myForm" onClick={onClose} type="primary">
                            {translate('button.submit')}
                        </Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form
                            id="myForm"
                            layout="vertical"
                            hideRequiredMark
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item
                                        name="roomName"
                                        label={translate('info.roomName')}
                                        rules={[{ required: true, message: 'Vui lòng nhập tên phòng' }]}
                                    >
                                        <Input placeholder="Nhập tên phòng" />
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item
                                        name="roomCode"
                                        label={translate('info.roomCode')}
                                        rules={[{ required: true, message: 'Vui lòng nhập mã phòng' }]}
                                    >
                                        <Input placeholder="Nhập mã phòng" />
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item
                                        name="roomQuantity"
                                        label={translate('info.roomQuantity')}
                                        rules={[{ required: true, message: 'Vui lòng nhập số lượng phòng' }]}
                                    >
                                        <InputNumber
                                            min={1}
                                            defaultValue={1}
                                            placeholder="Nhập số lượng phòng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item
                                        name="roomSize"
                                        label={translate('info.roomSize')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập kích thước phòng',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='m&sup2;'
                                            min={1}
                                            defaultValue={1}
                                            placeholder="Nhập kích thước phòng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="roomType"
                                        label={translate('info.roomType')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng chọn 1 loại phòng',
                                            },
                                        ]}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn 1 loại phòng"
                                            optionFilterProp="children"
                                            options={[
                                                {
                                                    value: 'room1',
                                                    label: 'Phòng đơn',
                                                },
                                                {
                                                    value: 'room2',
                                                    label: 'Phòng đôi',
                                                }
                                            ]}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item
                                        labelAlign="right"
                                        name="maxPeople"
                                        label={translate('info.maxPeople')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập số người ở tối đa',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            min={1}
                                            defaultValue={1}
                                            placeholder="Nhập số lượng người ở tối đa"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={11}>
                                    <Form.Item name="images" label={translate('info.image')} valuePropName="fileList">
                                        <Upload
                                            action="/"
                                            beforeUpload={() => false}
                                            onChange={onChange}
                                            fileList={fileList}
                                            listType="picture-card"
                                            accept="image/png, image/jpeg"
                                            maxCount={3}
                                        >
                                            {fileList.length != 3 ? (
                                                <div>
                                                    <PlusOutlined />
                                                    <div style={{ marginTop: 8 }}>Thêm ảnh</div>
                                                </div>
                                            ) : (
                                                ''
                                            )}
                                        </Upload>
                                        <Button type="primary" onClick={handleUpload}>
                                            Kiểm tra và Tải lên
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24}>
                                    <TreeServices></TreeServices>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('price.title')}
                                </Divider>
                                <Col span={12}>
                                    <Form.Item
                                        name="defaultPrice"
                                        label={translate('price.defaultPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá mặc định',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá mặc định"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="minPrice"
                                        label={translate('price.minPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá thấp nhất',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá thấp nhất"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="weekPrice"
                                        label={translate('price.weekPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá theo tuần',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá theo tuần"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="monthPrice"
                                        label={translate('price.monthPrice')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập giá theo tháng',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập giá theo tháng"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalAdultFee"
                                        label={translate('price.additionalAdultFee')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập phụ phí thêm 1 người lớn',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập phụ phí thêm 1 người lớn"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalChildFee"
                                        label={translate('price.additionalChildFee')}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập phụ phí thêm 1 trẻ em',
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            addonAfter='VNĐ'
                                            min={0}
                                            defaultValue={0}
                                            placeholder="Nhập phụ phí thêm 1 trẻ em"
                                            style={{ width: '100%' }}
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default RoomDetail;
