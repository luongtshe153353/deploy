'use client';
import React from 'react';
import { AntDesignOutlined, PlusOutlined } from '@ant-design/icons';
import {
    Avatar,
    Button,
    Checkbox,
    Col,
    DatePicker,
    Divider,
    Drawer,
    Form,
    Image,
    Input,
    Rate,
    Row,
    Select,
    Space,
    Tag,
} from 'antd';
import { useTranslations } from 'next-intl';
import type { DatePickerProps } from 'antd';
import { Typography } from 'antd';

const { Text } = Typography;

const { Option } = Select;

interface BusinessEstablishmentPreviewProps {
    idSelected?: number | string;
    openPreview?: boolean;
    setOpenPreview: (b: boolean) => void;
}

const BusinessEstablishmentPreview = (props: BusinessEstablishmentPreviewProps): React.ReactNode => {
    const { openPreview, setOpenPreview } = props;
    const listScreen = useTranslations('Management.Room.listScreen');
    const translate = useTranslations('Management.Room.previewScreen');

    return (
        <div>
            <Drawer
                title={translate('title')}
                width={1024}
                onClose={() => setOpenPreview(false)}
                open={openPreview}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
                extra={
                    <Space>
                        <Button onClick={() => setOpenPreview(false)}>{translate('button.close')}</Button>
                    </Space>
                }
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col span={24}>
                        <Form layout="vertical">
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('info.title')}
                                </Divider>
                                <Col span={8}>
                                    <Form.Item name="roomName" label={translate('info.roomName')}>
                                        <mark>Phòng tổng thống</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item name="roomCode" label={translate('info.roomCode')}>
                                        <mark>PTT</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item labelAlign="right" name="roomQuantity" label={translate('info.roomQuantity')}>
                                        <mark>12</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={4}>
                                    <Form.Item name="roomSize" label={translate('info.roomSize')}>
                                        <mark>300m&sup2;</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item name="roomType" label={translate('info.roomType')}>
                                        <mark>Phòng đôi</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item name="maxPeople" label={translate('info.maxPeople')}>
                                        <mark>6</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={11}>
                                    <Form.Item name="image" label={translate('info.image')}>
                                        <Row align="middle" gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://go2joy.s3.ap-southeast-1.amazonaws.com/blog/wp-content/uploads/2022/10/26151709/park-hyatt-5-sao-sai-gon.jpg"
                                                />
                                            </Col>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://vungtau.viashotels.com/wp-content/uploads/2022/03/KHACH-SAN-VUNG-TAU-FINAL-11122021_Page_012-3-1.jpg"
                                                />
                                            </Col>
                                            <Col span={8}>
                                                <Image
                                                    alt="test image"
                                                    src="https://cdn.alongwalk.info/vn/wp-content/uploads/2022/02/16093757/image-top-6-khach-san-5-sao-sapa-chat-luong-tot-nhat-cho-ky-nghi-duong-164495387794491.jpg"
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('services.title')}
                                </Divider>
                                <Col span={24} className="tag-group-custom">
                                    <Tag color="blue">Dọn phòng hàng ngày</Tag>
                                    <Tag color="blue">Đưa đón đến sân bay</Tag>
                                    <Tag color="blue">Giặt là</Tag>
                                    <Tag color="blue">Wifi riêng mỗi phòng</Tag>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Divider orientation="left" orientationMargin={5}>
                                    {translate('price.title')}
                                </Divider>
                                <Col span={12}>
                                    <Form.Item
                                        name="defaultPrice"
                                        label={translate('price.defaultPrice')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="minPrice"
                                        label={translate('price.minPrice')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="weekPrice"
                                        label={translate('price.weekPrice')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="monthPrice"
                                        label={translate('price.monthPrice')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalAdultFee"
                                        label={translate('price.additionalAdultFee')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        name="additionalChildFee"
                                        label={translate('price.additionalChildFee')}
                                    >
                                        <mark>5.000.000 VNĐ</mark>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
};

export default BusinessEstablishmentPreview;
