import React, { useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
import { useTranslations } from 'next-intl';
import { Dropdown, MenuProps, Modal, Popconfirm, Rate, Space, Switch, Tag, Tooltip, Typography, message } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { roomRepository } from './roomRepository';

interface DataType {
    id: number | string;
    key: React.Key;
    name: string;
    roomCode: string;
    roomType: string;
    roomCount: number;
    roomSize: number;
    maxPeopleStay: number;
    defaultPrice: number;
    status: boolean;
}

const useRoomHook: any = () => {
    const translate = useTranslations('Management.Room.listScreen');

    const initialPayload = {
        "searchPaging": {
            "pageNum": 1,
            "pageSize": 10,
        },
        "searchParams": ""
    }

    const [payload, setPayload] = React.useState(initialPayload)
    const [resp, setResp] = React.useState<any>({})
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const [isReload, setIsReload] = React.useState<boolean>(false)

    React.useEffect(() => {
        const loadingList = new Promise((resolve, reject) => {

            setIsLoading(true)
            resolve(roomRepository.searchRoom(payload).then((result) => {
                setIsLoading(false)
                setResp(result)
                setIsReload(false)
            }))

            reject(setIsLoading(false))
        })

    }, [payload, isReload])

    const meta = React.useMemo(() => {
        if (resp?.meta) {
            return resp.meta
        }
    }, [resp])

    const data: DataType[] = React.useMemo(() => {
        if (resp?.data) {
            return resp.data
        }
    }, [resp])

    const onChangePagination = (page: number) => {
        setPayload((prev: any) => ({
            ...prev, searchPaging: {
                ...prev?.searchPaging,
                pageNum: page
            }
        }))
    }

    const [switchStates, setSwitchStates]: [any, any] = useState({});

    React.useEffect(() => {
        if (!isLoading && data) {

            const newSwitchStateContainer: any = {};
            data.forEach((item) => {
                newSwitchStateContainer[`switch${item.id}`] = item.status;
            });

            setSwitchStates(newSwitchStateContainer);
        }
    }, [isLoading, data]);

    const handleSwitchChange = (switchName: string, id: any, status: any) => {
        const onConfirm = () => {

            const newSwitchStates: any = { ...switchStates };
            newSwitchStates[switchName] = !status;
            setSwitchStates(newSwitchStates);

            const payload = {
                id: id,
                status: !status
            }

            roomRepository.updateStatus(payload);

            data.forEach(item => {
                if (item.id == id) {
                    item.status = !status;
                }
            })

            message.success('Đã thay đổi trạng thái');
        };

        const onCancel = () => {
            message.info(`Đã hủy thay đổi trạng thái`);
        };

        const confirmContent = `Bạn có chắc chắn muốn thay đổi trạng thái?`;

        return (
            <Popconfirm
                title={confirmContent}
                onConfirm={onConfirm}
                onCancel={onCancel}
                okText="Xác nhận"
                cancelText="Hủy bỏ"
            >
                <Switch size="small" checked={switchStates[switchName]} />
            </Popconfirm>
        );
    };

    const onSearch = (event: any) => {
        setPayload((prev: any) => ({ ...prev, searchParams: event?.target?.value }))
    }

    // Drawer
    const [openCreate, setOpenCreate] = React.useState<any>({ isCreate: false, isOpen: false });
    const [openPreview, setOpenPreview] = React.useState<boolean>(false);
    const [idSelected, setIdSelected] = React.useState<number | string>(0);

    // Table

    const Action = React.useCallback((id: number | string) => {
        const items: MenuProps['items'] = [
            {
                key: '1',
                label: translate('preview'),
                onClick: () => {
                    setIdSelected(id);
                    setOpenPreview(true);
                },
            },
            // {
            //     key: '2',
            //     label: translate('update'),
            //     onClick: () => {
            //         setIdSelected(id);
            //         setOpenDetail({ isDetail: true, isOpen: true });
            //     },
            // },
        ];

        return (
            <Dropdown
                menu={{
                    items,
                    selectable: true,
                    defaultSelectedKeys: undefined,
                }}
                trigger={['click']}
            >
                <Typography.Link>
                    <Space>
                        {translate('selectable')}
                        <DownOutlined />
                    </Space>
                </Typography.Link>
            </Dropdown>
        );
    }, []);

    const columns: ColumnsType<DataType> = [
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('roomName')}</div>;
            },
            key: 'name',
            dataIndex: 'name',
            ellipsis: false,
            align: 'left',
            width: 200,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('roomCode')}</div>;
            },
            key: 'roomCode',
            dataIndex: 'roomCode',
            ellipsis: true,
            align: 'left',
            width: 70,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('roomType')}</div>;
            },
            key: 'roomType',
            dataIndex: 'roomType',
            ellipsis: true,
            align: 'left',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('roomQuantity')}</div>;
            },
            key: 'roomCount',
            dataIndex: 'roomCount',
            ellipsis: true,
            align: 'left',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('roomSize')}</div>;
            },
            key: 'roomSize',
            dataIndex: 'roomSize',
            ellipsis: false,
            align: 'left',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}m&sup2;</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('maxPeople')}</div>;
            },
            key: 'maxPeopleStay',
            dataIndex: 'maxPeopleStay',
            ellipsis: true,
            align: 'left',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]}</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('defaultPrice')}</div>;
            },
            key: 'defaultPrice',
            dataIndex: 'defaultPrice',
            ellipsis: true,
            align: 'left',
            width: 100,
            render: (...params: [string, DataType, number]) => {
                return (
                    <Tooltip title={params[0]}>
                        <span>{params[0]} VNĐ</span>
                    </Tooltip>
                );
            },
        },
        {
            title: () => {
                return <div className="d-flex flex-row">{translate('status')}</div>;
            },
            key: 'toggleStatus',
            width: 55,
            render: (...params: [string, DataType, number]) => {
                return <div className="text-center">{handleSwitchChange(`switch${params[1].id}`, params[1].id, params[0])}</div>;
            },
        },
        {
            title: '',
            key: 'Actions',
            width: 65,
            render: (...params: [string, DataType, number]) => {
                return Action(params[1]?.id);
            },
        },
    ];

    return {
        columns,
        data,
        //loading,
        //selectedRowKeys,
        //start,
        onSearch,
        openPreview,
        setOpenPreview,
        openCreate,
        setOpenCreate,
        idSelected,
        isLoading,
        meta,
        onChangePagination,
        setIsReload
    };
};

export default useRoomHook;
