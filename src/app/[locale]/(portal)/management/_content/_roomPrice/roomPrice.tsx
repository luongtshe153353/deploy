'use client';

import React, { useEffect, useRef, useState } from 'react';
import FullCalendar from '@fullcalendar/react';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import { DateSelectArg, EventSourceInput } from '@fullcalendar/core/index.js';
import { Button, Col, Form, Input, Modal, Row, Select, Spin } from 'antd';
import useRoomPriceHook from './roomPriceHook';
import { LoadingOutlined } from '@ant-design/icons';
import { format, addDays, isBefore, addMonths, getMonth } from 'date-fns';
import { Repository } from "@/config/repository"
import './roomPrice.scss';

interface priceType {
    title: string;
    start: string
}

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const RoomPriceContent: React.FC = () => {
    const [selectedDates, setSelectedDates] = useState<priceType[]>([]);
    const {
        onFinish,
        handleRoom,
        room,
    } = useRoomPriceHook();
    const [price, setPrice] = useState(0);
    const [date, setDate] = useState({ start: '', end: '' });
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [currentRoom, setCurrentRoom] = useState(-1);
    const showModal = () => {
        setIsModalOpen(true);
        setPrice(0);
    };

    const handleOk = () => {
        setSelectedDates([...selectedDates, {
            title: price.toString(),
            ...date
        }]);
        setIsModalOpen(false);
        setPrice(0);
        // setTimeout(() => {
        //     const calendarApi = calendarRef.current.getApi();
        //     const events = calendarApi.getEvents();

        //     // Lặp qua danh sách sự kiện và trích xuất thông tin bạn cần
        //     const eventInfoList = events.map((event: any) => ({
        //         title: event.title,
        //         start: event.startStr,
        //         end: event.endStr,
        //     }));

        //     // In danh sách thông tin sự kiện ra console
        //     console.log(eventInfoList);
        // }, 500);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
        setPrice(0);
    };

    const getInfoAllPrice = () => {
        // if (calendarRef.current) {
        //     const calendarApi = calendarRef.current.getApi();
        //     const events = calendarApi.getEvents();

        //     // Lặp qua danh sách sự kiện và trích xuất thông tin bạn cần
        //     const eventInfoList = events.map((event: any) => ({
        //         title: event.title,
        //         start: event.startStr,
        //         end: event.endStr,
        //     }));

        //     // In danh sách thông tin sự kiện ra console
        //     // console.log(eventInfoList);
        //     return eventInfoList;
        // }
        return null;
    }

    const addPrice = (arg: DateSelectArg) => {
        setDate({ start: arg.startStr, end: arg.endStr })
        showModal();
    }

    React.useEffect(() => {
        handleRoom();
    }, [])

    const handleChangeRoom = (value: any) => {
        setCurrentRoom(value);
    }

    const calendarRef = useRef<FullCalendar>(null);

    useEffect(() => {
        const calendarApi = calendarRef.current?.getApi();
        if (calendarApi) {
            const updateDisplayedDates = () => {

                // Lấy danh sách tất cả các ngày hiển thị trên màn hình
                const view = calendarApi.view;
                const start = view.activeStart;
                const end = view.activeEnd;

                const displayedDates: any[] = [];
                let current = start;
                let pricesCurrentMonth: any;

                while (isBefore(current, end)) {
                    displayedDates.push(format(current, 'yyyy-MM-dd'));
                    current = addDays(current, 1);
                }

                if (currentRoom != -1) {
                    new Repository().post("room/search-room-price-calender", {
                        "dateFrom": displayedDates[0],
                        "dateTo": displayedDates[displayedDates.length - 1],
                        "roomId": currentRoom
                    }).then((result) => {
                        pricesCurrentMonth = result?.data;

                        let pricesInCalendar: priceType[] = [];
                        displayedDates.forEach(item => {
                            pricesInCalendar.push({
                                title: pricesCurrentMonth?.defaultPrice + 'VNĐ', start: item
                            });
                        })
                        pricesInCalendar.forEach(item => {
                            for (let key in pricesCurrentMonth?.mapPriceByDate) {
                                if (key.slice(0, 10) == item.start) {
                                    item.title = pricesCurrentMonth?.mapPriceByDate[key];
                                }
                            }
                        });

                        setSelectedDates(pricesInCalendar);
                        // calendarApi.refetchEvents();
                    });
                }
            };

            // Sự kiện khi render các ngày trên màn hình
            calendarApi.on('datesSet', updateDisplayedDates);

            // Gọi một lần để log ngày hiển thị ban đầu và tháng hiện tại
            updateDisplayedDates();
        }
    }, [currentRoom]);

    return (
        <>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col span={24} style={{ textAlign: 'left', marginBottom: '15px' }}>
                    <Select
                        showSearch
                        onChange={handleChangeRoom}
                        placeholder="Chọn 1 loại phòng"
                        optionFilterProp="children"
                        options={room?.length > 0 ? room?.map((item: any) => ({
                            value: item?.id,
                            label: item?.name
                        })) : [{
                            value: null,
                            label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                        }]}
                        virtual={true}
                        placement='bottomLeft'
                    />
                </Col>
            </Row>
            <FullCalendar
                ref={calendarRef}
                plugins={[dayGridPlugin, interactionPlugin]}
                droppable={true}
                height={"80vh"}
                editable={true}
                selectable={true}
                eventDisplay='list-item'
                eventClick={function (arg) {
                    console.log(arg);
                }}
                // eventChange={() => { setSelectedDates(getInfoAllPrice) }}
                select={(arg) => {
                    addPrice(arg);
                }}
                initialView='dayGridMonth'
                customButtons={{
                    myCustomButton: {
                        text: 'Custom Button!',
                        click: function () {
                            alert('clicked the custom button!');
                        },
                    },
                }}
                headerToolbar={{
                    start: 'title', // will normally be on the left. if RTL, will be on the right
                    center: 'today myCustomButton',
                    end: 'prev,next', // will normally be on the right. if RTL, will be on the left
                }}
                events={selectedDates}

            />
            <Modal
                title="Custom Modal"
                open={isModalOpen} onOk={handleOk} onCancel={handleCancel}
            >
                <Input
                    placeholder="Nhập giá"
                    value={price}
                    onChange={(e) => setPrice(+e.target.value)}
                />
            </Modal>
        </>
    );
};

export default RoomPriceContent;
