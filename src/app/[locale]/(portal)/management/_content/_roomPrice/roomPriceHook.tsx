import { Repository } from "@/config/repository"
import React from "react"
import { FormInstance } from "antd"
import { TravelBooContext } from "@/app/[locale]/(portal)/layout"
import { useTranslations } from "next-intl"
// import { roomRepository } from "../roomRepository"

interface useRoomPriceHookProps {
    form?: FormInstance<any>
}

interface price {
    defaultPrice: number;
    mapPriceByDate: object;
}

// const useRoomPriceHook = (props: useRoomPriceHookProps) => {
const useRoomPriceHook = () => {

    // const { form } = props
    const { openNotification } = React.useContext(TravelBooContext)

    const translate = useTranslations('General.notification.create')

    const onFinish = React.useCallback((values: any) => {
        const payload = values;

        console.log(values);

        // return roomRepository.createRoom(payload).then((result: any) => {
        //     if (result?.code === 200) {
        //         openNotification('success', translate('success'))
        //     } else {
        //         openNotification('error', translate('error'), translate('errorDescription'))
        //     }
        // })
    }, [openNotification, translate])

    // handle room 
    const [room, setRoom] = React.useState([])
    const handleRoom = React.useCallback(() => {
        new Repository().post("room/search-room-management", {
            "baseSearchPagingDTO": {
                "pageNum": null,
                "pageSize": null,
                "sortType": "ASC"
            },
            "searchParams": ""
        }).then((result) => {
            if (Array.isArray(result?.data)) {
                setRoom(result?.data)
            }
        })
    }, [])

    return {
        onFinish,
        handleRoom,
        room,
    }
}

export default useRoomPriceHook