import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_ROOM_PREIX: string = 'room/';

export class RoomPriceRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_ROOM_PREIX;
    }

    public searchRoom = (payload?: any) => this.post(kebabCase('searchRoomManagement'), payload);

    public getRoom = (payload?: any) =>
        this.get('get-hotel/' + payload).then((result) => {
            return result?.data;
        });

    public updateStatus = (payload?: any) =>
        this.post('update-status', payload).then((result) => {
            return result?.code;
        });
}

export const roomRepository = new RoomPriceRepository();
