'use client';
import React, { useState } from 'react';

import { Menu, MenuTheme, Switch } from 'antd';
import useMenuHook from './menuHook';

interface MenuManagementProps {
    setMenuSelectedKey?: React.Dispatch<React.SetStateAction<string>>;
}

const MenuManagement = (props: MenuManagementProps) => {
    const { setMenuSelectedKey } = props;

    const [theme, setTheme] = useState<MenuTheme>('dark');

    const { items } = useMenuHook();
    const changeTheme = (value: boolean) => {
        setTheme(value ? 'light' : 'dark');
    };

    return (
        <div className="position-relative" style={{ background: '#ffff', width: '100%', height: '100%' }}>
            <div className="position-absolute top-0 start-0 px-2 pt-2">
                <Switch onChange={changeTheme} style={{ background: theme === 'dark' ? '#212529' : '#108EE9' }} />{' '}
                <span style={{ color: theme === 'light' ? '#212529' : '#ffff' }}>Change Style</span>
            </div>
            <Menu
                className="pt-5"
                style={{ width: '100%', height: '100%' }}
                defaultSelectedKeys={['businessEstablishment']}
                mode={'vertical'}
                theme={theme}
                items={items}
                onSelect={(v: any) => {
                    setMenuSelectedKey!(v?.key);
                }}
            />
        </div>
    );
};

export default MenuManagement;
