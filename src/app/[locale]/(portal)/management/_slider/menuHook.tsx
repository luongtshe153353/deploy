'use client';
import {
    HomeOutlined,
    AppstoreOutlined,
    GiftOutlined,
    LinkOutlined,
    MailOutlined,
    SlidersOutlined,
    UserOutlined,
    DashboardOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd/es/menu';
import { useTranslations } from 'next-intl';
import React from 'react';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key?: React.Key | null,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem;
}

const useMenuHook = () => {
    const translate = useTranslations('Management.Menu');

    const items: MenuItem[] = React.useMemo(
        () => [
            getItem(translate('dashboard'), 'dashboard', <DashboardOutlined />, [
                getItem('Option 1.1', '1.1'),
                getItem('Option 1.2', '1.2'),
                getItem('Submenu', 'sub1-2', null, [getItem('Option 5', '5'), getItem('Option 6', '6')]),
            ]),
            getItem(translate('businessEstablishment'), 'businessEstablishment', <HomeOutlined />),
            getItem(translate('businessAdmin'), 'businessAdmin', <UserOutlined />),
            getItem(translate('businessOwner'), 'businessOwner', <UserOutlined />),
            getItem(translate('room'), 'room', <AppstoreOutlined />),
            getItem(translate('roomPrice'), 'roomPrice', <AppstoreOutlined />),
            getItem(translate('marketingAccount'), 'marketingAccount', <UserOutlined />),
            getItem(translate('registeredUser'), 'registeredUser', <UserOutlined />),
            getItem(translate('promotion'), 'promotion', <GiftOutlined />),
            getItem(translate('emailMarketing'), 'emailMarketing', <MailOutlined />),
            getItem(translate('banner'), 'banner', <SlidersOutlined />, [
                getItem(translate('bannerSlider'), 'bannerSlider'),
            ]),
            getItem(
                <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
                    Ant Design
                </a>,
                'link',
                <LinkOutlined />,
            ),
        ],
        [translate],
    );

    return {
        items,
    };
};

export default useMenuHook;
