'use client';
import dynamic from 'next/dynamic';
import UseLoading from '@/components/loading';

const PageLayout = dynamic(() => import('./page'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const ManagementLayout = () => {
    return <PageLayout />;
};

export default ManagementLayout;
