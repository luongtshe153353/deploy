'use client';
import React, { CSSProperties } from 'react';
import { Layout, Space } from 'antd';
import MenuManagement from './_slider/menu';
import ManagementContent from './pageManamgent';

const { Header, Footer, Sider, Content } = Layout;

const headerStyle: CSSProperties = {
    textAlign: 'center',
    color: 'black',
    height: '7%',
    backgroundColor: '#7dbcea',
};

const contentStyle: CSSProperties = {
    textAlign: 'center',
};

const footerStyle: CSSProperties = {
    textAlign: 'center',
    height: '50px',
};

export default function ManagementLayout() {
    const [menuSelectedKey, setMenuSelectedKey] = React.useState<string>('businessEstablishment');
    return (
        <Space direction="vertical" style={{ width: '100%', height: '100%' }} size={[0, 48]}>
            <Layout style={{ width: '100vw', height: '100vh' }}>
                <Header style={headerStyle} className="border">
                    Header
                </Header>
                <Layout style={{ width: '100vw', height: '100%' }}>
                    <Sider width={'300px'}>
                        <MenuManagement setMenuSelectedKey={setMenuSelectedKey} />
                    </Sider>
                    <Layout>
                        <Content style={contentStyle} className="m-3">
                            <ManagementContent menuSelectedKey={menuSelectedKey} />
                        </Content>
                        <Footer style={footerStyle}>BooTravel ©2023 Created by FPT Student</Footer>
                    </Layout>
                </Layout>
            </Layout>
        </Space>
    );
}
