'use client';

import dynamic from 'next/dynamic';
import UseLoading from '@/components/loading';
import React from 'react';
import { useRouter } from "next/navigation"

interface AppRouterProps {
    menuSelectedKey?: string;
}

interface UserResponse {
    user: string | null
    error: any
}

const BusinessAdminComponent = dynamic(() => import('./_content/_businessAdmin/businessAdmin'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});


const BusinessOwnerComponent = dynamic(() => import('./_content/_businessOwner/businessOwner'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const BusinessEstablishmentComponent = dynamic(
    () => import('./_content/_businessEstablishment/businessEstablishment'),
    {
        loading: () => {
            const { GeneralLoading } = UseLoading();
            return <GeneralLoading />;
        },
        ssr: false,
    },
);

const RoomComponent = dynamic(
    () => import('./_content/_room/room'),
    {
        loading: () => {
            const { GeneralLoading } = UseLoading();
            return <GeneralLoading />;
        },
        ssr: false,
    },
);

const RoomPriceComponent = dynamic(
    () => import('./_content/_roomPrice/roomPrice'),
    {
        loading: () => {
            const { GeneralLoading } = UseLoading();
            return <GeneralLoading />;
        },
        ssr: false,
    },
);

const MarketingAccountComponent = dynamic(() => import('./_content/_marketingAccount/marketingAccount'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});
const RegisteredUserComponent = dynamic(() => import('./_content/_registeredUser/registeredUser'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const PromotionComponent = dynamic(() => import('./_content/_promotion/promotion'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const EmailMarketingComponent = dynamic(() => import('./_content/_emailMarketing/emailMarketing'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const BannerSliderComponent = dynamic(() => import('./_content/_bannerSlider/bannerSlider'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

export default function ManagementContent({ menuSelectedKey }: AppRouterProps) {

    const router = useRouter()

    // React.useEffect(() => {
    //     (async () => {
    //         const { user, error } = getUser()

    //         if (error) {
    //             router.push("/")
    //             return
    //         }
    //     })()
    // }, [])

    return (
        <>
            {menuSelectedKey === 'businessAdmin' ? <BusinessAdminComponent /> : null}
            {menuSelectedKey === 'businessOwner' ? <BusinessOwnerComponent /> : null}
            {menuSelectedKey === 'businessEstablishment' ? <BusinessEstablishmentComponent /> : null}
            {menuSelectedKey === 'room' ? <RoomComponent /> : null}
            {menuSelectedKey === 'roomPrice' ? <RoomPriceComponent /> : null}
            {menuSelectedKey === 'registeredUser' ? <RegisteredUserComponent /> : null}
            {menuSelectedKey === 'marketingAccount' ? <MarketingAccountComponent /> : null}
            {menuSelectedKey === 'promotion' ? <PromotionComponent /> : null}
            {menuSelectedKey === 'emailMarketing' ? <EmailMarketingComponent /> : null}
            {menuSelectedKey === 'bannerSlider' ? <BannerSliderComponent /> : null}
        </>
    );
}


// const getUser = async (): Promise<UserResponse> => {
//     try {
//         const { data } = await fetch('/api/auth/me', {
//             method: 'GET',
//             cache: 'no-cache',
//             // credentials: "same-origin",
//             headers: {
//                 Accept: '*/*',
//                 'Content-Type': 'application/json',
//                 'Accept-Encoding': 'gzip, deflate, br',
//                 Connection: 'keep-alive',
//             },
//         })

//         return {
//             user: data,
//             error: null
//         }
//     } catch (e) {
//         const error = e

//         return {
//             user: null,
//             error: e
//         }
//     }
// }
