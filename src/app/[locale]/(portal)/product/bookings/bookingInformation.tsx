/* eslint-disable @next/next/no-img-element */
'use client'
import useValidate from "@/utilities/validate";
import { Col, Divider, Form, Input } from "antd";
import type { NextPage } from "next";
import { useCallback } from "react";

const BookingInformation: NextPage = () => {

    const [form] = Form.useForm();
    const rules = useValidate()
    const onBtnPrimaryClick = useCallback(() => {
        // Please sync "Booking Page 2" to the project
    }, []);


    return (
        <div className="w-full flex flex-col justify-start items-center">
            <div className="w-[70%] flex flex-col items-center justify-center gap-[3px] text-center text-gray">
                <div className="w-full h-max flex flex-col items-center gap-3 text-lg text-darkgray-200">
                    <div className="w-max text-5xl block font-semibold text-[#152C5B]">
                        Thông tin thanh toán
                    </div>
                    <div className="font-light inline-block w-max text-[#152C5B]">
                        Quý khách vui lòng điền đầy đủ thông tin phía bên dưới!
                    </div>
                </div>

                <Divider type="horizontal" className="min-w-0 max-w-[200px]" />
            </div>
            <Form
                layout={"vertical"}
                form={form}
                className="w-[80%] grid grid-cols-12 gap-10"
            >
                <div className="col-span-6 flex flex-col justify-center items-end">
                    <img
                        className="rounded-xl w-[420px] h-[270px] object-cover"
                        alt=""
                        src="/temp/rectangle-34@2x.png"
                    />
                    <div className="w-[420px] flex flex-row justify-between items-center">
                        <div className="w-max flex flex-col text-left text-xl">
                            <div className="text-[#152C5B] font-medium">Podo Wae</div>
                            <div className="text-sm font-light text-[#B0B0B0]">
                                Madiun, Indonesia
                            </div>
                        </div>
                        <div className="inline-block w-[180px]">
                            <span className="font-medium">$480 USD</span>
                            <span className="font-light text-darkgray-200">{` per `}</span>
                            <span className="font-medium">2 nights</span>
                        </div>
                    </div>
                </div>
                <div className="col-span-6 flex flex-col max-w-[350px]">
                    <div className="grid grid-cols-3 gap-2">
                        <Form.Item label="Họ" name="layout" className="col-span-1" rules={rules.firstNameRule}>
                            <Input placeholder="input name" />
                        </Form.Item>
                        <Form.Item label="Tên" name="layout" className="col-span-2" rules={rules.lastNameRule}>
                            <Input placeholder="input name" />
                        </Form.Item>
                    </div>
                    <Form.Item label="Email" name="layout" className="w-full" rules={rules.emailRule}>
                        <Input placeholder="input name" />
                    </Form.Item>
                    <Form.Item label="Số điện thoại" name="layout" className="w-full" rules={rules.phoneNumberRule}>
                        <Input placeholder="input name" />
                    </Form.Item>
                    <Form.Item label="Địa chỉ" name="layout" className="w-full" rules={rules.addressRule}>
                        <Input placeholder="input name" />
                    </Form.Item>
                    <Form.Item label="Code" name="layout" className="w-full">
                        <Input placeholder="input name" />
                    </Form.Item>
                </div>


            </Form>
            <div className="flex flex-col flex-wrap items-center justify-center gap-[15px]">
                <button
                    className="cursor-pointer [border:none] py-[9px] px-0 bg-[#3252DF] rounded hover:shadow-[0px_8px_15px_rgba(50,_82,_223,_0.3)] w-[300px] h-[50px] flex flex-col items-center justify-start box-border"
                    onClick={onBtnPrimaryClick}
                >
                    <div className="relative text-lg leading-[170%] font-medium font-poppins text-white text-center inline-block w-[155px]">
                        Continue to Book
                    </div>
                </button>
                <button
                    className="cursor-pointer [border:none] p-0 bg-[#F5F6F8] flex flex-col justify-center items-center relative rounded w-[300px] h-[50px]"
                >
                    <div className="text-lg leading-[170%] font-poppins text-darkgray-100 text-center inline-block">
                        Cancel
                    </div>
                </button>
            </div>
        </div>
    );
};

export default BookingInformation;
