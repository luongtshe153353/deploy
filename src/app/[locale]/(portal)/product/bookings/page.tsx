'use client'
import { NextPage } from "next";
import BookingInformation from "./bookingInformation";
import { Steps } from "antd";
import React from "react";
import PaymentInfomation from "./paymentInfomation";

type StepType = 0 | 1 | 2
type StatusStep = 'wait' | 'process' | 'finish' | 'error';


const BookingPage: NextPage = () => {
    const description = 'This is a description.';

    const [status1, setStatus1] = React.useState<StatusStep>('process')
    const [status2, setStatus2] = React.useState<StatusStep>('wait')
    const [status3, setStatus3] = React.useState<StatusStep>('wait')
    const [step, setStep] = React.useState<StepType>(0)
    console.log(step)
    return (
        <div className="w-full relative flex flex-col items-center justify-center py-px px-0 gap-[5px]">
            <Steps
                current={-1}
                items={[
                    {
                        title: 'Thông tin thanh toán',
                        description,
                        status: status1,
                        onClick: () => {
                            setStep(0)
                            setStatus1('process')
                            setStatus2('wait')
                            setStatus3('wait')
                        }
                    },
                    {
                        title: 'Phương thức thanh toán',
                        description,
                        status: status2,
                        onClick: () => {
                            setStep(1)
                            setStatus1('wait')
                            setStatus2('process')
                            setStatus3('wait')
                        }
                    },
                    {
                        title: 'Thanh toán thành công',
                        description,
                        status: status3,
                        onClick: () => {
                            setStep(1)
                            setStatus1('wait')
                            setStatus2('wait')
                            setStatus3('process')
                        }
                    },
                ]}
                className="mb-4 mt-[-1px] border-0 border-bottom w-full h-[100px] px-36 pt-3 justify-center items-center sticky top-0 bg-white z-1"
            />
            {step === 0 ? <BookingInformation /> : null}
            {step === 1 ? <PaymentInfomation /> : null}
            {/* {step === 2 ? <BookingInformation /> : null} */}
        </div>
    )
}

export default BookingPage