/* eslint-disable @next/next/no-img-element */
'use client'
import { NextPage } from "next";
import { Divider, Form, Input, Radio } from "antd";
import useValidate from "@/utilities/validate";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQrcode } from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";

const PaymentInfomation: NextPage = () => {
    const [form] = Form.useForm();
    const rules = useValidate()


    const onBtnPrimaryClick = React.useCallback(() => {
        // Please sync "Booking Page 2" to the project
    }, []);
    return (
        <div className="w-full flex flex-col justify-start items-center">
            <div className="w-[70%] flex flex-col items-center justify-center gap-[3px] text-center text-gray">
                <div className="w-full h-max flex flex-col items-center gap-3 text-lg text-darkgray-200">
                    <div className="w-max text-5xl block font-semibold text-[#152C5B]">
                        Phương thức thanh toán
                    </div>
                    <div className="font-light inline-block w-max text-[#152C5B]">
                        Quý khách vui lòng chọn phương thức thanh toán bên dưới!
                    </div>
                </div>

                <Divider type="horizontal" className="min-w-0 max-w-[200px]" />
            </div>
            <Form
                layout={"vertical"}
                form={form}
                className="w-[80%] grid grid-cols-12 gap-8"
            >
                <div className="col-span-6 flex flex-col justify-center items-end h-max ">
                    <img
                        className="rounded-xl w-[420px] h-[270px] object-cover"
                        alt=""
                        src="/temp/rectangle-34@2x.png"
                    />
                    <div className="flex flex-row justify-between items-center w-[420px]">
                        <div className="w-max flex flex-col text-left text-xl">
                            <div className="text-[#152C5B] font-medium">Podo Wae</div>
                            <div className="text-sm font-light text-[#B0B0B0]">
                                Madiun, Indonesia
                            </div>
                        </div>
                        <div className="inline-block w-[180px]">
                            <span className="font-medium">$480 USD</span>
                            <span className="font-light text-darkgray-200">{` per `}</span>
                            <span className="font-medium">2 nights</span>
                        </div>
                    </div>
                </div>
                <Form.Item name="paymentMethod" className="col-span-6 flex flex-row justify-start overflow-hidden">
                    <Radio.Group
                        className="flex flex-col gap-3 max-w-[400px]"
                        onChange={(e: any) => {
                            console.log(e)
                        }}
                    >
                        <Radio value={{
                            key: 1,
                            value: "public"
                        }} className="border w-full p-2 rounded-lg">
                            <div className="w-full min-h-[50px] flex flex-row gap-3 items-center">
                                <div className="">
                                    Thanh toán qua VNPay
                                </div>
                                {/* <FontAwesomeIcon icon={faQrcode} style={{ color: "#6d95d9", }} size="2x" /> */}
                                <Image
                                    src='/temp/Logo-VNPAY-QR-1.webp'
                                    alt="Logo VNPAY"
                                    width={165}
                                    height={50}
                                />
                            </div>

                        </Radio>

                        <Radio value={{
                            key: 2,
                            value: "private"
                        }} className="border w-full p-2 rounded-lg" >
                            <div className="w-full min-h-[50px] flex flex-col gap-2">
                                <div className="flex flex-row gap-3 items-center">
                                    <div className="">
                                        Chuyển khoản
                                    </div>
                                    <FontAwesomeIcon icon={faQrcode} style={{ color: "#3d73e1", width: 50, height: 50 }} />
                                </div>
                            </div>
                            {/* <Divider type="horizontal" className="min-w-0 w-full" />
                                    <div>
                                        Quý khách sau khi thực hiện việc chuyển khoản vui lòng gửi email đến contactcenter@vietravel.com hoặc gọi tổng đài 19001839 để được xác nhận từ công ty chúng tôi.

                                        Tên Tài Khoản : Công ty CP Du lịch và Tiếp thị GTVT Việt Nam – Vietravel
                                        Tên tài khoản viết tắt : VIETRAVEL
                                        Số Tài khoản : 111 6977 27979
                                        Ngân hàng : Vietinbank - Chi nhánh 7
                                    </div> */}
                        </Radio>
                    </Radio.Group>
                </Form.Item>


            </Form>
            <div className="flex flex-col flex-wrap items-center justify-center gap-[15px]">
                <button
                    className="cursor-pointer [border:none] py-[9px] px-0 bg-[#3252DF] rounded hover:shadow-[0px_8px_15px_rgba(50,_82,_223,_0.3)] w-[300px] h-[50px] flex flex-col items-center justify-start box-border"
                    onClick={onBtnPrimaryClick}
                >
                    <div className="relative text-lg leading-[170%] font-medium font-poppins text-white text-center inline-block w-[155px]">
                        Continue to Book
                    </div>
                </button>
                <button
                    className="cursor-pointer [border:none] p-0 bg-[#F5F6F8] flex flex-col justify-center items-center relative rounded w-[300px] h-[50px]"
                >
                    <div className="text-lg leading-[170%] font-poppins text-darkgray-100 text-center inline-block">
                        Cancel
                    </div>
                </button>
            </div>
        </div>
    )
}

export default PaymentInfomation