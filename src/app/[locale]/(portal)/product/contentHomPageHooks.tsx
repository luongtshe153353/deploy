'use client'
import { Col } from "antd"
import styles from "./homePage.module.css";
import React from "react";
import Image from "next/image";

const useContentHomePageHooks = () => {

    const promotionItems: React.ReactNode[] = React.useMemo(() => {
        let result: React.ReactNode[] = []

        for (let i = 0; i < 10; i++) {
            let item: React.ReactNode =
                <Col key={i} className={`item ${styles.houseBackyard}`} data-value={i}>
                    <Image
                        className={styles.houseBackyardChild}
                        alt=""
                        src={`/temp/rectangle-31${i}@2x.png`}
                        draggable={false}
                        width={263}
                        height={180}
                    />
                    <div className={styles.badge5}>
                        <div className={styles.perNight}>
                            <span className={styles.span}>Popular</span>
                            <span className={styles.choice}> Choice</span>
                        </div>
                    </div>
                    <div className={styles.houseBackyardText}>Tabby Town</div>
                    <div className={styles.houseBackyardSubText}>
                        Gunung Batu, Indonesia
                    </div>
                </Col>

            result.push(item)
        }

        return result
    }, [])

    const hotelItems: React.ReactNode[] = React.useMemo(() => {
        let result: React.ReactNode[] = []

        for (let i = 0; i < 10; i++) {
            let item: React.ReactNode =
                <Col key={i} className={`item ${styles.houseBackyard}`} data-value={i}>
                    <Image
                        className={styles.houseBackyardChild}
                        alt=""
                        src="/temp/rectangle-310@2x.png"
                        draggable={false}
                        width={263}
                        height={180}
                    />
                    <div className={styles.badge5}>
                        <div className={styles.perNight}>
                            <span className={styles.span}>Popular</span>
                            <span className={styles.choice}> Choice</span>
                        </div>
                    </div>
                    <div className={styles.houseBackyardText}>Tabby Town</div>
                    <div className={styles.houseBackyardSubText}>
                        Gunung Batu, Indonesia
                    </div>
                </Col>

            result.push(item)
        }

        return result
    }, [])

    const Items: React.ReactNode[] = React.useMemo(() => {
        let result: React.ReactNode[] = []

        for (let i = 0; i < 10; i++) {
            let item: React.ReactNode =
                <Col key={i} className={`item ${styles.houseBackyard}`} data-value={i}>
                    <Image
                        className={styles.houseBackyardChild}
                        alt=""
                        src="/temp/rectangle-310@2x.png"
                        draggable={false}
                        width={263}
                        height={180}
                    />
                    <div className={styles.badge5}>
                        <div className={styles.perNight}>
                            <span className={styles.span}>Popular</span>
                            <span className={styles.choice}> Choice</span>
                        </div>
                    </div>
                    <div className={styles.houseBackyardText}>Tabby Town</div>
                    <div className={styles.houseBackyardSubText}>
                        Gunung Batu, Indonesia
                    </div>
                </Col>

            result.push(item)
        }

        return result
    }, [])



    return {
        promotionItems,
        hotelItems,
    }
}

export default useContentHomePageHooks