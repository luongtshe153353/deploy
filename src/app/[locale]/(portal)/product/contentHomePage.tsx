/* eslint-disable @next/next/no-img-element */
'use client'
import type { NextPage } from "next";
import React, { useCallback } from "react";
import styles from "./homePage.module.css";
import { Col, Row } from "antd";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import useContentHomePageHooks from "./contentHomPageHooks";

const ContentHomePage: NextPage = () => {
  const onBtnPrimaryClick = useCallback(() => {
    // Please sync "Home Page Scrolled" to the project
  }, []);

  const onBtnPrimary1Click = useCallback(() => {
    // Please sync "Home Page Scrolled" to the project
  }, []);

  const responsive = {
    0: { items: 1 },
    568: { items: 2 },
    1024: { items: 3 },
    1600: { items: 5 },
  };

  const { promotionItems } = useContentHomePageHooks()

  return (
    <Row className={`${styles.homePage}`}>
      <Col span={24} className={`${styles.frame} d-flex flex-row align-items-center`}>
        <div className={`${styles.hero}`}>
          <div className={`${styles.content}`}>
            <b className={styles.forgetBusyWorkContainer}>
              <p className={styles.forgetBusyWork}>Forget Busy Work,</p>
              <p className={styles.forgetBusyWork}>Start Next Vacation</p>
            </b>
            <div className={styles.weProvideWhatContainer}>
              <p
                className={styles.forgetBusyWork}
              >{`We provide what you need to enjoy your `}</p>
              <p
                className={styles.forgetBusyWork}
              >{`holiday with family. Time to make another `}</p>
              <p className={styles.forgetBusyWork}>memorable moments.</p>
            </div>
            <button className={styles.btnPrimary} onClick={onBtnPrimaryClick}>
              <div className={styles.showMeNow}>Show Me Now</div>
            </button>
            <div className={styles.item}>
              <img
                className={styles.icCities1Icon}
                alt=""
                src="/temp/ic-traveler-1-1@2x.png"
              />
              <div className={styles.travelers}>
                <span className={styles.span}>80,409</span>
                <span className={styles.travelers1}> travelers</span>
              </div>
            </div>
            <div className={styles.item1}>
              <img
                className={styles.icCities1Icon}
                alt=""
                src="/temp/ic-cities-1@2x.png"
              />
              <div className={styles.travelers}>
                <span className={styles.span}>1,492</span>
                <span className={styles.travelers1}> cities</span>
              </div>
            </div>
            <div className={styles.item2}>
              <img
                className={styles.icCities1Icon}
                alt=""
                src="/temp/ic-treasure-1@2x.png"
              />
              <div className={styles.travelers}>
                <span className={styles.span}>862</span>
                <span className={styles.travelers1}> treasure</span>
              </div>
            </div>
          </div>
          <img className={styles.bannerIcon} alt="" src="/temp/banner@2x.png" />
        </div>
      </Col>
      <Col span={24} className={`${styles.frame1} w-100 d-flex flex-row align-items-center justify-content-center`}>
        <Row className={`${styles.mostPicked} d-flex flex-row`}>
          <Col span={24}>
            <div className={`${styles.mostPicked1}`}>Most Picked</div>
          </Col>
          <Col span={8} className="d-flex flex-column">
            <div className={styles.featured}>
              <img
                className={styles.featuredChild}
                alt=""
                src="/temp/rectangle-35@2x.png"
              />
              <div className={`${styles.featuredItem}`} />
              <div className={`${styles.badge}`}>
                <div className={styles.perNight}>
                  <span className={styles.span}>$50</span>
                  <span className={styles.choice}> per night</span>
                </div>
              </div>
              <div className={styles.featuredLeftText}>Blue Origin Fams</div>
              <div className={styles.featuredLeftSubText}>Jakarta, Indonesia</div>
            </div>
          </Col>

          <Col span={16} className="d-flex flex-row flex-wrap gap-3">
            <div className={styles.featured1}>
              <img
                className={styles.featuredInner}
                alt=""
                src="/temp/rectangle-36@2x.png"
              />
              <div className={styles.rectangleDiv} />
              <div className={styles.featuredRightText}>Ocean Land</div>
              <div className={styles.featuredRightSubText}>Bandung, Indonesia</div>
              <div className={styles.badge1}>
                <div className={styles.perNight}>
                  <span className={styles.span}>$22</span>
                  <span className={styles.choice}> per night</span>
                </div>
              </div>
            </div>
            <div className={styles.featured2}>
              <img
                className={styles.featuredInner}
                alt=""
                src="/temp/rectangle-37@2x.png"
              />
              <div className={styles.rectangleDiv} />
              <div className={styles.badge2}>
                <div className={styles.perNight}>
                  <span className={styles.span}>$856</span>
                  <span className={styles.choice}> per night</span>
                </div>
              </div>
              <div className={styles.featuredRightText}>Stark House</div>
              <div className={styles.featuredRightSubText}>Malang, Indonesia</div>
            </div>
            <div className={styles.featured3}>
              <img
                className={styles.featuredInner}
                alt=""
                src="/temp/rectangle-38@2x.png"
              />
              <div className={styles.rectangleDiv} />
              <div className={styles.featuredRightText}>Vinna Vill</div>
              <div className={styles.badge1}>
                <div className={styles.perNight}>
                  <span className={styles.span}>$62</span>
                  <span className={styles.choice}> per night</span>
                </div>
              </div>
              <div className={styles.featuredRightSubText}>Malang, Indonesia</div>
            </div>
            <div className={styles.featured4}>
              <img
                className={styles.featuredInner}
                alt=""
                src="/temp/rectangle-39@2x.png"
              />
              <div className={styles.rectangleDiv} />
              <div className={styles.featuredRightText}>Bobox</div>
              <div className={styles.badge1}>
                <div className={styles.perNight}>
                  <span className={styles.span}>$72</span>
                  <span className={styles.choice}> per night</span>
                </div>
              </div>
              <div className={styles.featuredRightSubText}>Medan, Indonesia</div>
            </div>
          </Col>
        </Row>
      </Col>

      <Row className={`w-100 ${styles.frame2}`}>
        <Col span={24} className={`${styles.housesWithBeauty} mb-3`}>
          Promotions
        </Col>
        <Col span={24}>
          <AliceCarousel
            mouseTracking
            items={promotionItems}
            responsive={responsive}
            disableButtonsControls
          />
        </Col>
      </Row>

      <Row className={`w-100 ${styles.frame3}`}>
        <Col span={24} className={`${styles.housesWithBeauty} mb-3`}>
          Hotel List
        </Col>
        <Col span={24}>
          <AliceCarousel
            mouseTracking
            items={promotionItems}
            responsive={responsive}
            disableButtonsControls
          />
        </Col>
      </Row>

      <Row className={`w-100 ${styles.frame4}`}>
        <Col span={24} className={`${styles.housesWithBeauty} mb-3`}>
          Categories Room
        </Col>
        <Col span={24}>
          <AliceCarousel
            mouseTracking
            items={promotionItems}
            responsive={responsive}
            disableButtonsControls
          />
        </Col>
      </Row>

      <Col span={24} className={styles.frame5}>
        <div className={styles.happyStory}>
          <img
            className={styles.fotoKeluargaIcon}
            alt=""
            src="/temp/foto-keluarga1@2x.png"
          />
          <div className={styles.happyFamilyParent}>
            <div className={styles.happyFamily}>Happy Family</div>
            <div className={styles.whatAGreatContainer}>
              <p
                className={styles.forgetBusyWork}
              >{`What a great trip with my family and `}</p>
              <p className={styles.forgetBusyWork}>
                I should try again next time soon ...
              </p>
            </div>
            <div className={styles.anggaProductDesigner}>
              Angga, Product Designer
            </div>
            <button className={styles.btnPrimary1} onClick={onBtnPrimary1Click}>
              <div className={styles.showMeNow}>Read Their Story</div>
            </button>
            <img className={styles.frameChild} alt="" src="/temp/group-1.svg" />
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default ContentHomePage;
