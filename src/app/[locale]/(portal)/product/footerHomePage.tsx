'use client'
import { NextPage } from "next";
import styles from "./homePage.module.css";
import { Col, Row } from "antd";


const FooterHomePage: NextPage = () => {
    return (
        <div className={`p-5 pb-1 w-full`}>
            <div className={`${styles.navigation}`}>
                <Row>
                    <Col span={6} className="d-flex flex-column">
                        <div className={styles.staycation1}>
                            <span>Travel</span>
                            <span className={styles.cation}>Boo.</span>
                        </div>
                        <div className={styles.weKaboomYourContainer}>
                            <p className={styles.forgetBusyWork}>We kaboom your beauty holiday</p>
                            <p className={styles.forgetBusyWork}>instantly and memorable.</p>
                        </div>
                    </Col>

                    <Col span={12}>
                        <div className={`${styles.forBeginnersParent} d-flex flex-row justify-content-between`}>
                            <div className={`d-flex flex-column align-items-start`}>
                                <div className={styles.forBeginners}>For Beginners</div>
                                <div>New Account</div>
                                <div>Start Booking a Room</div>
                                <div>Use Payments</div>
                            </div>
                            <div className={`d-flex flex-column align-items-start`}>
                                <div className={styles.forBeginners}>Explore Us</div>
                                <div>Our Careers</div>
                                <div>Privacy</div>
                                <div>{`Terms & Conditions`}</div>
                            </div>
                            <div className={`d-flex flex-column align-items-start`}>
                                <div className={styles.forBeginners}>Connect Us</div>
                                <div>support@staycation.id</div>
                                <div>021 - 2208 - 1996</div>
                                <div>Staycation, Kemang, Jakarta</div>
                            </div>
                        </div>
                    </Col>

                    <Col span={6}>
                    </Col>
                </Row>



                <div className={`${styles.copyright} mt-5 d-flex flex-row justify-content-center`}>
                    Copyright 2023 • SEP490-G80 • TravelBoo
                </div>
            </div>
        </div>
    )
}

export default FooterHomePage