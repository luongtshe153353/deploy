'use client'
import { NextPage } from "next";
import styles from "./homePage.module.css";

const HeaderHomePage: NextPage = () => {
    return (
        <div className={`${styles.navigationHeader} w-100 border`}>
            <div className="d-flex flex-row justify-content-between">
                <div className={`${styles.staycation}`}>
                    <span>Travel</span>
                    <span className={styles.cation}>Boo.</span>
                </div>
                <div className={`${styles.homeBrowseByContainer} d-flex flex-row w-25 justify-content-between`}>
                    <span>Home</span>
                    <span className={styles.cation}>Browse by</span>
                    <span className={styles.cation}>Stories</span>
                    <span className={styles.cation}>Agents</span>
                </div>
            </div>
        </div>
    )
}

export default HeaderHomePage