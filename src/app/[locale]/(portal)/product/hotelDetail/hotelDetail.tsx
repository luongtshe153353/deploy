/* eslint-disable @next/next/no-img-element */
'use client'
import { Breadcrumb } from "antd";
import type { NextPage } from "next";
import Image from "next/image";
import { useCallback } from "react";

const HotelDetail: NextPage = () => {
    const onBtnPrimaryClick = useCallback(() => {
        // Please sync "Booking Page 1" to the project
    }, []);

    const onBtnPrimary1Click = useCallback(() => {
        // Please sync "Home Page Scrolled" to the project
    }, []);

    return (
        <div className="py-[50px] px-[120px] w-full h-full relative bg-white flex flex-col items-center gap-[53px] text-left text-base text-darkslateblue font-poppins">
            <div className="w-full px-5 flex flex-row items-center justify-between text-lg text-darkgray-200">
                <Breadcrumb
                    className="grow inline-block shrink-0 z-[0]"
                    items={[
                        {
                            title: 'Home',
                            href: '/product'
                        },
                        {
                            title: <span className="font-medium text-darkslateblue">House Details</span>,
                        },
                    ]}
                />
                <div className="grow-0 my-0 flex flex-col items-center text-center">
                    <div className="text-2xl font-semibold text-darkslateblue inline-block w-max">
                        Village Angga
                    </div>
                    <div className="font-light inline-block">
                        Bogor, Indonesia
                    </div>
                </div>

                <div className="grow">

                </div>
            </div>

            <div className="flex flex-row flex-wrap w-full items-center justify-center gap-[10px]">
                <Image
                    className="relative rounded-mini object-cover"
                    alt=""
                    src="/temp/rectangle-5@2x.png"
                    width={643}
                    height={500}
                />
                <div className="flex flex-col items-center justify-center gap-[10px]">
                    <Image
                        className="relative rounded-mini object-cover"
                        alt=""
                        src="/temp/rectangle-6@2x.png"
                        width={487}
                        height={245}
                    />
                    <img
                        className="relative rounded-mini object-cover"
                        alt=""
                        src="/temp/rectangle-61@2x.png"
                        width={487}
                        height={245}
                    />
                </div>
            </div>

            <div className="flex flex-row flex-wrap items-start justify-center gap-[53px]">
                <div className="flex flex-col items-start justify-start gap-[20px]">
                    <div className="text-xl font-medium">About the place</div>
                    <div className="flex flex-col w-[600px] gap-[20px] text-darkgray-200">
                        <div className="leading-[170%] font-light inline-block w-[600px]">
                            Minimal techno is a minimalist subgenre of techno music. It is
                            characterized by a stripped-down aesthetic that exploits the use
                            of repetition and understated development. Minimal techno is
                            thought to have been originally developed in the early 1990s by
                            Detroit-based producers Robert Hood and Daniel Bell.
                        </div>
                        <div className="leading-[170%] font-light inline-block w-[600px]">
                            Such trends saw the demise of the soul-infused techno that
                            typified the original Detroit sound. Robert Hood has noted that he
                            and Daniel Bell both realized something was missing from techno in
                            the post-rave era.
                        </div>
                        <div className="leading-[170%] font-light inline-block w-[600px]">
                            Design is a plan or specification for the construction of an
                            object or system or for the implementation of an activity or
                            process, or the result of that plan or specification in the form
                            of a prototype, product or process. The national agency for
                            design: enabling Singapore to use design for economic growth and
                            to make lives better.
                        </div>
                    </div>
                    <div className="flex flex-row w-[531px] justify-between">
                        <div className="flex flex-col gap-[20px]">
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-bedroom@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">5</span>
                                    <span className="font-light text-darkgray-200"> bedroom</span>
                                </div>
                            </div>
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-wifi-1@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">10</span>
                                    <span className="font-light text-darkgray-200"> mbp/s</span>
                                </div>
                            </div>

                        </div>
                        <div className="flex flex-col gap-[20px]">
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-livingroom@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">1</span>
                                    <span className="font-light text-darkgray-200">
                                        {" "}
                                        living room
                                    </span>
                                </div>
                            </div>
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-ac-1@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">7</span>
                                    <span className="font-light text-darkgray-200">
                                        {" "}
                                        unit ready
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-col gap-[20px]">
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-bedroom1@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">3</span>
                                    <span className="font-light text-darkgray-200"> bathroom</span>
                                </div>
                            </div>
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-kulkas@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">2</span>
                                    <span className="font-light text-darkgray-200">
                                        {" "}
                                        refigrator
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-col gap-[20px]">
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-diningroom-1@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">1</span>
                                    <span className="font-light text-darkgray-200">
                                        {" "}
                                        dining room
                                    </span>
                                </div>
                            </div>
                            <div className="flex flex-col h-[73px]">
                                <Image
                                    className="object-cover"
                                    alt=""
                                    src="/temp/ic-tv@2x.png"
                                    width={38}
                                    height={38}
                                />
                                <div className="leading-[170%]">
                                    <span className="font-medium">4</span>
                                    <span className="font-light text-darkgray-200">
                                        {" "}
                                        television
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center w-[487px] h-[550px] p-5 rounded-xl box-border border-[1px] border-solid border-gray-200">
                    <div className="w-full text-[#2F446D] my-0 mx-[!important] flex flex-col items-start justify-start gap-[21px]">
                        <div className="w-full">
                            <div className="font-medium text-xl mb-2">Start Booking</div>
                            <div className="text-17xl text-mediumaquamarine">
                                <span className="font-semibold text-4xl text-[#1ABC9C]">$280</span>
                                <span className="font-light text-darkslateblue">{` `}</span>
                                <span className="font-extralight text-3xl text-[#B7B7B7]">
                                    per night
                                </span>
                            </div>
                        </div>
                        <div className="w-full h-max text-center text-5xl">
                            <div>
                                <div className="text-base leading-[170%] text-darkslateblue text-left inline-block w-full">
                                    How long you will stay?
                                </div>
                                <div className="relative h-max">
                                    <div className="absolute top-0 left-0 rounded bg-[#F5F6F8] w-full h-[45px]" />
                                    <div className="flex flex-row w-full">
                                        <button className="w-[45px] h-[45px] relative">
                                            <div className="absolute top-[0px] left-[0px] z-[0] rounded bg-[#E74C3C] w-full h-full" />
                                            <b className="p-0 m-0 absolute bottom-[5px] right-[12px]">
                                                -
                                            </b>
                                        </button>
                                        <input
                                            className="[border:none] font-poppins text-base bg-[transparent] leading-[170%] text-darkslateblue text-center inline-block grow z-1"
                                            placeholder="2 nights"
                                            type="text"
                                        />
                                        <button className="w-[45px] h-[45px] relative">
                                            <div className="absolute top-[0px] left-[0px] z-[0] rounded bg-[#E74C3C] w-full h-full" />
                                            <b className="p-0 m-0 absolute bottom-1 right-[5px] font-medium">
                                                +
                                            </b>
                                        </button>

                                    </div>

                                </div>
                            </div>
                            <div>
                                <div className="text-base leading-[170%] text-darkslateblue text-left inline-block w-full">
                                    Pick a Date
                                </div>
                                <div className="relative h-max">
                                    <div className="absolute top-0 left-0 rounded bg-[#F5F6F8] w-full h-[45px]" />
                                    <div className="flex flex-row w-full">
                                        <img
                                            className="w-[45px] h-[45px] z-1"
                                            alt=""
                                            src="/temp/ic-calendar.svg"
                                        />
                                        <input
                                            className="[border:none] font-poppins text-base bg-[transparent] leading-[170%] text-darkslateblue text-center inline-block grow z-1"
                                            placeholder="2 nights"
                                            type="text"
                                        />
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="flex flex-col w-full">
                            <div className="leading-[170%] inline-block w-full text-darkgray-200">
                                <span className="font-light">{`You will pay `}</span>
                                <span className="font-medium">$480 USD</span>
                                <span className="font-light">{` per `}</span>
                                <span className="font-medium">2 nights</span>
                            </div>
                            <button
                                className="cursor-pointer [border:none] py-[9px] px-0 bg-[#3252DF] rounded hover:shadow-[0px_8px_15px_rgba(50,_82,_223,_0.3)] w-full h-[50px] flex flex-col items-center justify-start box-border"
                                onClick={onBtnPrimaryClick}
                            >
                                <div className="relative text-lg leading-[170%] font-medium font-poppins text-white text-center inline-block w-[155px]">
                                    Continue to Book
                                </div>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div className="flex flex-col items-start justify-center gap-[20px] text-xl">
                <div className="relative font-medium">Treasure to Choose</div>
                <div className="flex flex-row items-center justify-start gap-[29px]">
                    <div className="relative w-[263px] h-[249px]">
                        <img
                            className="absolute top-[0px] left-[0px] rounded-mini w-[263px] h-[180px] object-cover"
                            alt=""
                            src="/temp/rectangle-3@2x.png"
                        />
                        <div className="absolute top-[196px] left-[0px]">Green Lake</div>
                        <div className="absolute top-[226px] left-[0px] text-mini font-light text-darkgray-200">
                            Nature
                        </div>
                    </div>
                    <div className="relative w-[263px] h-[249px]">
                        <img
                            className="absolute top-[0px] left-[0px] rounded-mini w-[263px] h-[180px] object-cover"
                            alt=""
                            src="/temp/rectangle-31@2x.png"
                        />
                        <div className="absolute top-[196px] left-[0px]">Dog Clubs</div>
                        <div className="absolute top-[226px] left-[0px] text-mini font-light text-darkgray-200">
                            Pool
                        </div>
                    </div>
                    <div className="relative w-[263px] h-[249px] text-base text-white">
                        <img
                            className="absolute top-[0px] left-[0px] rounded-mini w-[263px] h-[180px] object-cover"
                            alt=""
                            src="/temp/rectangle-32@2x.png"
                        />
                        <div className="absolute top-[0px] left-[83px] rounded-tl-none rounded-tr-mini rounded-br-none rounded-bl-mini bg-deeppink w-[180px] h-10 flex flex-col items-start justify-start py-1.5 px-2.5 box-border text-center">
                            <div className="relative leading-[170%] inline-block w-[156px]">
                                <span className="font-medium">Popular</span>
                                <span className="font-light"> Choice</span>
                            </div>
                        </div>
                        <div className="absolute top-[196px] left-[0px] text-xl text-darkslateblue">
                            Labour and Wait
                        </div>
                        <div className="absolute top-[226px] left-[0px] text-mini font-light text-darkgray-200">
                            Shopping
                        </div>
                    </div>
                    <div className="relative w-[263px] h-[249px]">
                        <img
                            className="absolute top-[0px] left-[0px] rounded-mini w-[263px] h-[180px] object-cover"
                            alt=""
                            src="/temp/rectangle-33@2x.png"
                        />
                        <div className="absolute top-[196px] left-[0px]">Snorkeling</div>
                        <div className="absolute top-[226px] left-[0px] text-mini font-light text-darkgray-200">
                            Beach
                        </div>
                    </div>
                </div>
            </div>
            {/* 
            <div className="flex flex-row items-center justify-center gap-[57px] text-5xl">
                <img
                    className="relative w-[405px] h-[541px] object-cover"
                    alt=""
                    src="/temp/foto-keluarga@2x.png"
                />
                <div className="relative w-[564px] h-[351px]">
                    <div className="absolute top-[0px] left-[3px] font-medium">
                        Happy Family
                    </div>
                    <div className="absolute top-[120px] left-[4px] text-13xl">
                        <p className="m-0">As a wife I can pick a great trip with</p>
                        <p className="m-0">my own lovely family ... thank you!</p>
                    </div>
                    <div className="absolute top-[224px] left-[3px] text-lg font-light text-darkgray-200">
                        Anggi, Product Designer
                    </div>
                    <button
                        className="cursor-pointer [border:none] py-[9px] px-0 bg-mediumslateblue absolute top-[301px] left-[4px] rounded shadow-[0px_8px_15px_rgba(50,_82,_223,_0.3)] w-[210px] h-[50px] flex flex-col items-center justify-start box-border"
                        onClick={onBtnPrimary1Click}
                    >
                        <div className="relative text-lg leading-[170%] font-medium font-poppins text-white text-center inline-block w-[155px]">
                            Read Their Story
                        </div>
                    </button>
                    <img
                        className="absolute top-[76px] left-[0px] w-[180px] h-9"
                        alt=""
                        src="/temp/group-1.svg"
                    />
                </div>
            </div>
            <div className="flex flex-col items-center justify-center relative gap-[10px] text-center text-darkgray-200">
                <div className="absolute my-0 mx-[!important] top-[0px] left-[0px] bg-gainsboro w-[1440px] h-px z-[0]" />
                <div className="absolute my-0 mx-[!important] top-[236px] left-[537px] font-light z-[1]">
                    Copyright 2019 • All rights reserved • Staycation
                </div>
                <div className="absolute my-0 mx-[!important] top-[51px] left-[150px] w-[1077px] h-[135px] z-[2] text-left">
                    <div className="absolute top-[0px] left-[0px] w-[257px] h-[95px] text-7xl text-mediumslateblue">
                        <div className="absolute top-[0px] left-[0px] font-medium">
                            <span>Stay</span>
                            <span className="text-darkslateblue">cation.</span>
                        </div>
                        <div className="absolute top-[47px] left-[0px] text-base font-light text-darkgray-200">
                            <p className="m-0">We kaboom your beauty holiday</p>
                            <p className="m-0">instantly and memorable.</p>
                        </div>
                    </div>
                    <div className="absolute top-[0px] left-[390px] w-[169px] h-[135px]">
                        <div className="absolute top-[0px] left-[0px] text-lg font-medium text-darkslateblue inline-block w-[137px] h-6">
                            For Beginners
                        </div>
                        <div className="absolute top-[47px] left-[0px] font-light">
                            New Account
                        </div>
                        <div className="absolute top-[79px] left-[0px] font-light">
                            Start Booking a Room
                        </div>
                        <div className="absolute top-[111px] left-[0px] font-light">
                            Use Payments
                        </div>
                    </div>
                    <div className="absolute top-[0px] left-[629px] w-[153px] h-[135px]">
                        <div className="absolute top-[0px] left-[0px] text-lg font-medium text-darkslateblue inline-block w-[137px] h-6">
                            Explore Us
                        </div>
                        <div className="absolute top-[47px] left-[0px] font-light">
                            Our Careers
                        </div>
                        <div className="absolute top-[79px] left-[0px] font-light">
                            Privacy
                        </div>
                        <div className="absolute top-[111px] left-[0px] font-light">{`Terms & Conditions`}</div>
                    </div>
                    <div className="absolute top-[0px] left-[852px] w-[225px] h-[135px]">
                        <div className="absolute top-[0px] left-[0px] text-lg font-medium text-darkslateblue inline-block w-[137px] h-6">
                            Connect Us
                        </div>
                        <div className="absolute top-[47px] left-[0px] font-light">
                            support@staycation.id
                        </div>
                        <div className="absolute top-[79px] left-[0px] font-light">
                            021 - 2208 - 1996
                        </div>
                        <div className="absolute top-[111px] left-[0px] font-light">
                            Staycation, Kemang, Jakarta
                        </div>
                    </div>
                </div>
                <div className="relative w-[1440px] h-[310px] z-[3]" />
            </div> */}
        </div>
    );
};

export default HotelDetail;
