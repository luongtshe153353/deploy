'use client'
import { Carousel, Col, Row, Typography } from 'antd';
import { HeartOutlined } from '@ant-design/icons';
import { Rate } from 'antd';
import './hotelCard.scss'
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons';
import { faHeart as fasHeart } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';

const { Text, Title } = Typography

const HotelCard = (): React.ReactNode => {

    const [isHovered, setIsHovered] = React.useState<boolean>(false)


    return (

        <Row className='w-100 p-3 border rounded-2'>

            <Col flex={'320px'} className='p-0 position-relative overflow-hidden' style={{ height: 300 }}>
                <FontAwesomeIcon icon={isHovered ? fasHeart : farHeart} color='#fe5858' size='2x'
                    className='hotelCard-heart-outline position-absolute top-0 end-0 me-3 mt-2 z-1'
                    onMouseEnter={() => setIsHovered(true)}
                    onMouseLeave={() => setIsHovered(false)} />

                <Carousel dotPosition={'bottom'} className='z-0'>
                    <div>
                        <Image className='rounded-2' alt="example" src="https://img.tripi.vn/cdn-cgi/image/width=548,height=310/https://storage.googleapis.com/hms_prod/photo/thumb/462027KDJ/amanoi-vietnam---celebrations--eventsromantic-lake-dinner_15105.jpg" width={320} height={220} style={{ objectFit: 'cover', top: 0 }} />
                    </div>
                    <div>
                        <Image className='rounded-2' alt="example" src="https://img.tripi.vn/cdn-cgi/image/width=548,height=310/https://storage.googleapis.com/hms_prod/photo/thumb/462027KDJ/amanoi-vietnam---celebrations--eventsromantic-lake-dinner_15105.jpg" width={320} height={220} style={{ objectFit: 'cover', top: 0 }} />
                    </div>
                    <div>
                        <Image className='rounded-2' alt="example" src="https://img.tripi.vn/cdn-cgi/image/width=548,height=310/https://storage.googleapis.com/hms_prod/photo/thumb/462027KDJ/amanoi-vietnam---celebrations--eventsromantic-lake-dinner_15105.jpg" width={320} height={220} style={{ objectFit: 'cover', top: 0 }} />
                    </div>
                </Carousel>

            </Col>

            <Col flex={1} className='p-3 pt-0'>
                <Col span={24} className='d-flex flex-row justify-content-start'>
                    <Title level={3}>Anoi Hotel</Title>
                </Col>

                <Col span={24} className='d-flex flex-row justify-content-start'>
                    <Rate allowHalf defaultValue={4} disabled />
                </Col>

                <Col span={24} className='d-flex flex-row justify-content-start'>

                </Col>

                <Col span={24} className='d-flex flex-row justify-content-start'>

                </Col>
            </Col>

        </Row>
    )
}

export default HotelCard