'use client'
import { getDuration } from "@/utilities/date"
import { Col, Row, Input, Typography, Divider, DatePicker, Badge, Button } from "antd"
import { useTranslations } from "next-intl"
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons"



const { RangePicker } = DatePicker
const { Text } = Typography

const TopSearch = (): React.ReactNode => {

    const [countDate, setCountDate] = React.useState(0)
    const handleCountDate = React.useCallback((dates: any) => {
        if (dates && dates.length === 2) {
            const startDate = new Date(dates[0]).getTime()
            const endDate = new Date(dates[1]).getTime();
            const mili = endDate - startDate;
            setCountDate(getDuration(mili).value);
        } else {
            setCountDate(0)
        }
    }, [])

    const translate = useTranslations('Product.hotelList')

    return (
        <Row className="w-100 h-100 z-1 position-relative p-3 border rounded-2">
            <div className="position-absolute rounded-2 top-0 bottom-0 start-0 end-0 z-0" style={{ backgroundColor: '#ffff' }}></div>

            <Col span={7} className="d-flex flex-row align-items-center">
                <div className="flex-grow-1 d-flex flex-column align-items-start">
                    <Text strong className="ps-2">{translate('topSearch.address.label:')}</Text>
                    <Input size="large" bordered={false} className="ps-2" style={{ height: 50 }} placeholder="Thành phố, khách sạn" />
                </div>

                <Divider type="vertical" className="h-50"></Divider>
            </Col>

            <Col span={9} className="d-flex flex-row justify-content-between align-items-center">
                <Badge count={countDate > 0 ? countDate : null} color="#fe5858" offset={[-7, -5]} className="flex-grow-1">
                    <RangePicker size="large" style={{ width: '100%', height: '20px' }} bordered={false} onChange={handleCountDate} />
                </Badge>
                <Divider type="vertical" className="h-50"></Divider>
            </Col>

            <Col span={6} className="d-flex flex-row align-items-center">
                <div className="flex-grow-1 d-flex flex-column align-items-start">
                    <Text strong className="ps-2">{translate('topSearch.detail.label')}</Text>
                    <Input size="large" bordered={false} className="ps-2" style={{ height: 50 }} placeholder="Thành phố, khách sạn" />
                </div>

                <Divider type="vertical" className="h-50"></Divider>
            </Col>

            <Col span={2} className="p-2">
                <Button className="h-100 w-100 border-0" style={{ backgroundColor: '#fe5858' }}><FontAwesomeIcon icon={faMagnifyingGlass} style={{ color: "#ffff", }} /></Button>
            </Col>
        </Row>
    )
}

export default TopSearch