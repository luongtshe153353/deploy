import { Col, Row } from "antd"


const HotelListLayout = ({ children }: { children: React.ReactNode }): React.ReactNode => {
    return (
        <Row className="p-0 w-100 h-100">
            <Col span={4}>
            </Col>

            <Col span={16} className="">
                {children}
            </Col>

            <Col span={4}>
            </Col>
        </Row>
    )
}

export default HotelListLayout