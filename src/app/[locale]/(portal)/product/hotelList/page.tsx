'use client'
import { Breadcrumb, Col, Divider, Row, Segmented, Slider, Space, Typography } from "antd"
import React from "react"
import TopSearch from "./_middleContent/topSearch"
import { HomeOutlined, UserOutlined } from "@ant-design/icons"
import SimpleMap from "./_middleContent/googleMap"
import './hotelList.scss'
import HotelCard from "./_middleContent/_hotelCard/card"
import { SliderTooltipProps } from "antd/es/slider"


const { Text, Title } = Typography


const railStyle: React.CSSProperties = {
    backgroundColor: '#ffff',
};

const trackStyle: React.CSSProperties = {
    backgroundColor: '#fe5858',
};

const handleStyle: React.CSSProperties = {
    boxShadow: '0 0 0 2px #fe5858',
    borderRadius: 50,

};

const tooltipProps: SliderTooltipProps = {
    placement: 'bottom',
}

const HotelListPage = () => {


    return (
        <Space direction="vertical" size={'middle'} className="product-hotel-list p-0 w-100">

            <TopSearch />

            <Row className="p-0 mt-3">
                <Col span={6}>
                    <SimpleMap />
                </Col>

                <Col span={17} className="d-flex flex-row justify-content-start ms-4">
                    <Row className="d-flex flex-column align-items-start w-100" style={{ height: 'fit-content' }}>
                        <Title level={1}>Thành phố Hà Nội</Title>
                        <Text strong style={{ marginTop: -5, fontSize: '14px' }}>100 Khách sạn tại Thành phố Hà Nội</Text>
                    </Row>


                </Col>
            </Row>

            <Row className="p-0" >
                <Col span={6} className="p-0 d-flex flex-column gap-3">

                    <div className="d-flex flex-column">
                        <div className="d-flex flex-row justify-content-between p-3 rounded" style={{ backgroundColor: '#ffff' }}>
                            <Text strong>Bộ lọc</Text>
                            <Text strong style={{ color: '#3da5f5' }}>Xóa tất cả lọc</Text>
                        </div>

                        <Divider type="horizontal" className="w-100" />

                        <div className="d-flex flex-column align-items-start">
                            <Text strong>Khoảng giá (1 đêm)</Text>
                            <Text strong>0<sup>đ</sup> - 5.000.000<sup>đ+</sup></Text>
                            <Slider max={10000} tooltip={tooltipProps} defaultValue={[300, 1000]} min={150} style={{ color: 'red' }} className="w-100" range={{ draggableTrack: true }} styles={{ rail: railStyle, track: trackStyle, handle: handleStyle }} />
                        </div>

                    </div>
                </Col>

                <Col span={18}>

                    <Col span={24} className="ms-4 p-3 d-flex flex-row position-relative">
                        <div className="position-absolute rounded-2 top-0 bottom-0 start-0 end-0 z-0" style={{ backgroundColor: '#ffff' }}></div>
                        <div className="z-1 d-flex flex-row align-items-center w-100" style={{ height: 30 }}>
                            <Text strong className="me-3">Sắp xếp: </Text>
                            <Segmented block options={[123, 456, 'longtext-longtext-longtext-longtext']} className="flex-grow-1" style={{}} />
                        </div>

                    </Col>

                    <Col span={24} className="ms-4 mt-3 d-flex flex-row">

                        <Row className="flex-gorw-1 position-relative border border-success w-100">
                            <div className="position-absolute rounded-2 top-0 bottom-0 start-0 end-0 z-0" style={{ backgroundColor: '#ffff' }}></div>
                            <HotelCard />
                        </Row>

                        {/* <Row className="position-relative border border-success w-100">
                            <div className="position-absolute rounded-2 top-0 bottom-0 start-0 end-0 z-0" style={{ backgroundColor: '#ffff' }}></div>
                            <HotelCard />
                        </Row> */}

                    </Col>
                </Col>
            </Row>
        </Space>
    )
}

export default HotelListPage