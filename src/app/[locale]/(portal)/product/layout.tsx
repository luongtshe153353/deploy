'use client';
import React from 'react';
import { Layout, Space } from 'antd';
import HeaderHomePage from './headerHomePage';
import FooterHomePage from './footerHomePage';
import { usePathname } from 'next/navigation'

const { Header, Content } = Layout;

const headerStyle: React.CSSProperties = {
    // textAlign: 'center',
    // color: 'black',
    height: 80,
    // backgroundColor: '#7dbcea',
};


export default function ProductLayout({ children }: { children: React.ReactNode }) {
    const pathName = usePathname()

    const isBookingPage: boolean = pathName === '/product/bookings'
    return (
        <Space direction="vertical" style={{ width: '100%', height: '100%' }} size={[0, 48]}>
            <Layout style={{ width: '100vw', height: '100vh' }} className="d-flex flex-column">
                {!isBookingPage ? <Header style={headerStyle} className="p-0 m-0 w-100">
                    <HeaderHomePage />
                </Header> : null}
                <Content className="flex-grow-1 w-full h-full overflow-y-scroll bg-white">
                    <div>
                        {children}
                    </div>
                    {!isBookingPage ?
                        <div>
                            <FooterHomePage />
                        </div>
                        : null
                    }
                </Content>
            </Layout>
        </Space>
    );
}
