import { Col, Row } from 'antd';
import ContentHomePage from './contentHomePage';

const ProductPage = (): React.ReactNode => {
    return (
        <Row
            gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
            justify={'center'}
            className="product-page m-0 p-0 w-100 h-100 overflow-auto position-relative"
        >
            <ContentHomePage />
        </Row>
    );
};

export default ProductPage;
