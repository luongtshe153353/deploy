'use client';
import UseLoading from '@/components/loading';
import dynamic from 'next/dynamic';
import "./profile.scss"
import { Button, DatePicker, Divider, Form, Input, Modal, Select, Spin, Upload, message } from 'antd';
import React, { useState } from 'react';
import { Repository } from '@/config/repository';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { profile } from 'console';
import { profileRepository } from './profileRepository';
import { TravelBooContext } from '../layout';

const ProfileComponent = dynamic(() => import('./page'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const ProfileLayout = (): React.ReactNode => {

    const { openNotification } = React.useContext(TravelBooContext)

    // modal change password =================================================================
    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModalChangePassword = () => {
        setIsModalOpen(true);
    };

    const [formChangePassword] = Form.useForm();

    const handleOk = () => {
        formChangePassword
            .validateFields()
            .then(() => {
                handleFinishChangePassword(formChangePassword.getFieldsValue());
            })
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const handleFinishChangePassword = (values: any) => {

        // Kiểm tra newPassword không giống oldPassword
        if (values.newPassword === values.oldPassword) {
            message.error('Mật khẩu mới không được giống mật khẩu cũ');
            return;
        }

        // Kiểm tra reNewPassword phải giống newPassword
        if (values.newPassword !== values.reNewPassword) {
            message.error('Mật khẩu mới không khớp');
            return;
        }

        const payload = {
            oldPassword: values.oldPassword,
            newPassword: values.newPassword
        }

        profileRepository?.changePassword(payload).then((resp: any) => {
            if (resp?.code === 200) {
                openNotification('success', "Thành công", "Thay đổi mật khẩu thành công")
            } else {
                openNotification('error', "Thất bại", "Thay đổi mật khẩu thất bại")
            }
        })
        setIsModalOpen(false);
    }

    // change profile =================================================================

    const [form] = Form.useForm();

    // handle province
    const [province, setProvince] = React.useState([])
    const handleProvince = React.useCallback(() => {
        new Repository().listProvince().then((result) => {
            if (Array.isArray(result?.listData)) {
                setProvince(result?.listData)
            }
        })
    }, [])

    //handle distrcit
    const [district, setDistrict] = React.useState([])
    const handleDistrict = React.useCallback(() => {
        new Repository().listDistrict(form?.getFieldValue('provinceId')).then((result) => {
            if (Array.isArray(result?.listData)) {

                setDistrict(result?.listData)
            }
        })
    }, [])

    // handle ward
    const [ward, setward] = React.useState([])
    const handleWard = React.useCallback(() => {
        new Repository().listWard(form?.getFieldValue('districtId')).then((result) => {
            if (Array.isArray(result?.listData)) {

                setward(result?.listData)
            }
        })
    }, [])

    const readURL = (input: HTMLInputElement) => {
        if (input.files && input.files[0]) {
            const reader = new FileReader();

            reader.onload = (e: ProgressEvent<FileReader>) => {
                const target = e.target as FileReader;
                const result = target.result as string;
                document.querySelector('.profile-pic')?.setAttribute('src', result);
            };
            //reader.readAsDataURL(input.files[0]);
        }
    };

    const handleFileChange = (event: any) => {
        const input = event.target;
        readURL(input);
    };

    const handleUploadButtonClick = () => {
        const fileUpload: any = document.querySelector('.file-upload');
        fileUpload.click();
    };

    // change profile
    const onFinishChangeProfile = (values: any) => {
        console.log(values);
        console.log(values.image);

    }

    const getFile = (e: any) => {
        return e.file;
    };

    return (
        <Form form={form} onFinish={onFinishChangeProfile}>
            <div className="container rounded bg-white mt-5 mb-5 w-2/3">
                <div className="row">
                    <div className="col-md-4 border-right">
                        <div className="d-flex flex-column align-items-center text-center p-3 py-5">

                            <div className="avatar-wrapper">
                                <img className="profile-pic"
                                    src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"
                                    alt=""
                                />
                                <div className="upload-button" onClick={handleUploadButtonClick}>
                                    <i className="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                </div>
                                <Form.Item name="image" noStyle>
                                    <Input type='file' className="file-upload" accept="image/*" onChange={handleFileChange} />
                                </Form.Item>
                            </div>

                            {/* <Form.Item
                                name="image"
                                getValueFromEvent={getFile}
                                noStyle
                            >
                                <Upload
                                    className="file-upload"
                                    beforeUpload={() => false}
                                    // onChange={onChangeFile}
                                    // fileList={fileList}
                                    accept="image/png, image/jpeg"
                                    listType="picture-card"
                                    maxCount={1}
                                >
                                    <div>
                                        <PlusOutlined />
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                    </div>

                                </Upload>
                            </Form.Item> */}

                            <span className="font-weight-bold">Edogaru</span>
                            <span className="text-black-50">edogaru@mail.com.my</span>

                            <div className="col-md-6 mt-2">
                                <Button type='primary' onClick={showModalChangePassword}>Đổi mật khẩu</Button>
                            </div>

                            <Modal title="Đổi mật khẩu" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} >
                                <Form name='formChangePassword'
                                    form={formChangePassword}
                                    labelCol={{ span: 7 }}
                                    wrapperCol={{ span: 17 }}
                                    style={{ maxWidth: 600 }}
                                >
                                    <Form.Item
                                        label="Mật khẩu cũ"
                                        name="oldPassword"
                                    >
                                        <Input.Password />
                                    </Form.Item>

                                    <Form.Item
                                        label="Mật khẩu mới"
                                        name="newPassword"
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                    <Form.Item
                                        label="Nhập lại mật khẩu"
                                        name="reNewPassword"
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                </Form>
                            </Modal>
                        </div>
                    </div>
                    <div className="col-md-6 border-right">
                        <div className="p-3 py-5">
                            <div className="d-flex justify-content-between align-items-center mb-3">
                                <h4 className="text-right">Cập nhật thông tin</h4>
                            </div>
                            <Divider orientation="left" orientationMargin={5}>
                                Thông tin cơ bản
                            </Divider>
                            <div className="row mt-2">
                                <div className="col-md-6">
                                    <label className="labels">Họ</label>
                                    <Form.Item name="firstName" noStyle>
                                        <Input type="text" placeholder="Nhập họ" />
                                    </Form.Item>
                                </div>
                                <div className="col-md-6">
                                    <label className="labels">Tên</label>
                                    <Form.Item name='lastName' noStyle>
                                        <Input type="text" placeholder="Nhập tên" />
                                    </Form.Item>
                                </div>

                                <div className="col-md-6 mt-2">
                                    <label className="labels">Giới tính</label>
                                    <Form.Item name="gender" noStyle>
                                        <Select
                                            placeholder="Lựa chọn giới tính"
                                            className='w-100'
                                            options={
                                                [{
                                                    label: "Nam",
                                                    value: "Nam"
                                                }, {
                                                    label: "Nữ",
                                                    value: "Nữ"
                                                }]
                                            }
                                        >
                                        </Select>
                                    </Form.Item>

                                </div>
                                <div className="col-md-6 mt-2">
                                    <label className="labels">Ngày sinh</label>
                                    <Form.Item name="dob" noStyle>
                                        <DatePicker placeholder='Lựa chọn ngày sinh' className='w-100' />
                                    </Form.Item>
                                </div>
                            </div>
                            <Divider orientation="left" orientationMargin={5}>
                                Thông tin liên hệ
                            </Divider>
                            <div className="row mt-3 d-flex">
                                <div className="col-md-12 ">
                                    <label className="labels">Số điện thoại</label>
                                    <Form.Item name="phoneNumber" noStyle>
                                        <Input type="text" placeholder="Nhập số điện thoại" />
                                    </Form.Item>
                                </div>
                                <div className="col-md-4 mt-2" >
                                    <label className="labels">Tỉnh/Thành Phố</label>
                                    <Form.Item
                                        name="provinceId"
                                        labelAlign="right"
                                        style={{ width: "100%", margin: "0px" }}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn tỉnh/thành phố"
                                            optionFilterProp="children"
                                            onClick={handleProvince}
                                            options={province?.length > 0 ? province?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                        />
                                    </Form.Item>
                                </div>
                                <div className="col-md-4 mt-2" >
                                    <label className="labels">Quận/Huyện</label>
                                    <Form.Item
                                        name="districtId"
                                        labelAlign="right"
                                        style={{ width: "100%", margin: "0px" }}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn quận/huyện"
                                            optionFilterProp="children"
                                            onClick={handleDistrict}
                                            options={district?.length > 0 ? district?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('provinceId') ? false : true}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="col-md-4 mt-2" >
                                    <label className="labels">Phường/Xã</label>
                                    <Form.Item
                                        labelAlign="right"
                                        name="wardId"
                                        style={{ width: "100%", margin: "0px" }}
                                    >
                                        <Select
                                            showSearch
                                            placeholder="Chọn xã/phường"
                                            optionFilterProp="children"
                                            onClick={handleWard}
                                            options={ward?.length > 0 ? ward?.map((item: any) => ({
                                                value: item?.id,
                                                label: item?.name
                                            })) : [{
                                                value: null,
                                                label: <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
                                            }]}
                                            virtual={true}
                                            placement='bottomLeft'
                                            disabled={form.getFieldValue('districtId') ? false : true}
                                        />
                                    </Form.Item>
                                </div>

                                <div className="col-md-6 mt-2">
                                    <label className="labels">Số nhà</label>
                                    <Form.Item name="address1" noStyle>
                                        <Input type="text" placeholder="Nhập số nhà" />
                                    </Form.Item>
                                </div>
                                <div className="col-md-6 mt-2">
                                    <label className="labels">Số ngõ</label>
                                    <Form.Item name="address2" noStyle>
                                        <Input type="text" placeholder="Nhập số ngõ" />
                                    </Form.Item>
                                </div>
                            </div>
                            <div className="mt-5 text-center">
                                <Button type="primary" htmlType="submit">Lưu</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Form>
    );
};

export default ProfileLayout;
