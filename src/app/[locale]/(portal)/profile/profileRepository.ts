import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_PROFILE_PREIX: string = 'registered-users/';

class ProfileRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_PROFILE_PREIX;
    }

    public changePassword = (payload?: any) =>
        this.post(kebabCase('changePassword'), payload).then((result) => {
            return result;
        });
}

export const profileRepository = new ProfileRepository();
