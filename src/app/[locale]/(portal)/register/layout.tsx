import UseLoading from '@/components/loading';
import dynamic from 'next/dynamic';
import './register.scss';
import './fonts/iconic/css/material-design-iconic-font.min.css';
import { HomeFilled } from '@ant-design/icons';
import Link from 'next/link';

const RegisterComponent = dynamic(() => import('./page'), {
    loading: () => {
        const { GeneralLoading } = UseLoading();
        return <GeneralLoading />;
    },
    ssr: false,
});

const RegisterLayout = (): React.ReactNode => {
    return (
        <div className="limiter">
            <span className='position-fixed' style={{ top: '50px', left: '50px' }}>
                <a href='#' style={{ fontSize: 50 }}><HomeFilled style={{ color: "white" }} /></a>
            </span>

            <div className="container-login100" style={{ backgroundImage: "url('https://images.unsplash.com/photo-1579546929518-9e396f3cc809?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8fA%3D%3D')" }}>
                <div className="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
                    <div className="login100-form validate-form">
                        <span className="login100-form-title p-b-49">
                            Đăng ký
                        </span>

                        <RegisterComponent />

                        <div className="flex-col-c p-t-35">
                            <span className="txt1 p-b-17">
                                Hoặc bạn đã có tài khoản rồi
                            </span>

                            <Link href="login" className="txt2">
                                Đăng nhập
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RegisterLayout;
