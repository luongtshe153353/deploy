"use client"
import React from 'react';
import { Button, Form, Input } from 'antd';
import { useRouter } from 'next/navigation';
import { TravelBooContext } from '../layout';
import { useTranslations } from 'next-intl';
import { registerRepository } from './registerRepository';

const RegisterForm: React.FC = () => {

    const { openNotification } = React.useContext(TravelBooContext)
    const router = useRouter()
    const translate = useTranslations('General.notification.login')

    const onFinish = React.useCallback(async (values: any) => {


        if (!values.username || !values.password || !values.repassword || !values.email) {
            openNotification('error', 'Thất bại', 'Vui lòng nhập đủ thông tin');
            return;
        }

        // Kiểm tra password và re-password giống nhau
        if (values.password !== values.repassword) {
            openNotification('error', 'Thất bại', 'Mật khẩu không khớp');
            return;
        }

        // Kiểm tra định dạng email
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(values.email)) {
            openNotification('error', 'Thất bại', 'Email không đúng định dạng');
            return;
        }

        const request = {
            username: values.username,
            password: values.password,
            email: values.email
        }

        registerRepository?.register(request).then((resp: any) => {
            if (resp == undefined) {
                openNotification('success', "Thành công", "Đăng ký tài khoản thành công")
            } else {
                openNotification('error', "Thất bại", "Đăng ký thất bại. Xin vui lòng thử lại")
            }
        })

    }, [openNotification, router, translate]);

    return (
        <Form name="normal_register" onFinish={onFinish}
        >
            <div className="wrap-input100 validate-input m-b-23" >
                <span className="label-input100">Tên đăng nhập</span>
                <Form.Item name="username" noStyle>
                    <input className="input100" type="text" placeholder="Nhập tên đăng nhập" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf206;"></span>
            </div>

            <div className="wrap-input100 validate-input m-b-23" >
                <span className="label-input100">Mật khẩu</span>
                <Form.Item name="password" noStyle>
                    <input className="input100" type="password" placeholder="Nhập mật khẩu" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf190;"></span>
            </div>

            <div className="wrap-input100 validate-input m-b-23" >
                <span className="label-input100">Nhập lại mật khẩu</span>
                <Form.Item name="repassword" noStyle>
                    <input className="input100" type="password" placeholder="Nhập lại mật khẩu" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf190;"></span>
            </div>

            <div className="wrap-input100 validate-input" >
                <span className="label-input100">Email</span>
                <Form.Item name="email" noStyle>
                    <input className="input100" type="email" placeholder="Nhập email" />
                </Form.Item>
                <span className="focus-input100" data-symbol="&#xf15a;"></span>
            </div>

            <div className="text-right p-t-8 p-b-31">

            </div>
            <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn"></div>
                    <Form.Item noStyle>
                        <Button type="primary" htmlType="submit" className="login100-form-btn">
                            Đăng ký
                        </Button>
                    </Form.Item>
                </div>
            </div>

        </Form >
    );
};

export default RegisterForm;
