import { Repository } from '@/config/repository';
import { kebabCase } from 'lodash';

export const API_REGISTER_PREIX: string = 'registered-users/';

class RegisterRepository extends Repository {
    constructor() {
        super();
        this.BasePath = API_REGISTER_PREIX;
    }

    public register = (payload?: any) =>
        this.loginPost(kebabCase('processRegister'), payload).then((result) => {
            return result;
        });
}

export const registerRepository = new RegisterRepository();
