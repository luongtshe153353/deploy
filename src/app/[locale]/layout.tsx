import React from 'react';
import NotFound from '@/components/404';
import { NextIntlClientProvider } from 'next-intl';
import ReduxProvider from '@/redux/provider';



type Props = {
    children: React.ReactNode;
    params: { locale: string };
};

export default async function LocaleLayout({ children, params: { locale } }: Props) {
    let messages;

    try {
        messages = (await import(`@/locales/${locale}.json`)).default;
    } catch (error) {
        <NotFound />;
    }



    return (

        <NextIntlClientProvider locale={locale} messages={messages}>
            <ReduxProvider>
                {children}
            </ReduxProvider>
        </NextIntlClientProvider>
    );
}
