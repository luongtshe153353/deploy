import { Repository } from '@/config/repository'
import { serialize } from 'cookie'
import {decode, sign} from 'jsonwebtoken'
import { NextResponse } from "next/server"


const MAX_AGE = 60 * 60 * 24 * 30

export const POST = async (request: Request) => {
    const body = await request.json()

    const login = await new Repository().loginPost('login', body)

    if(login?.type !== "ERROR"){
        return NextResponse.json({
            message: "Authenticated!"
        }, {
            status: 200
        })
    }

    const secret = process.env.JWT_SECRET || ''

    const token = sign({
        body,
    }, 
    secret,
    {
        expiresIn: MAX_AGE
    }
    )

    const serialized = serialize('OutSiteJWT', token, {
        httpOnly: true,
        // secure: process.env.NODE_ENV === 'production',
        sameSite: 'strict',
        maxAge: MAX_AGE,
        path: '/'
    })

    const response = {
        message: 'Authenticated!'
    }

    return new Response(JSON.stringify(response), {
        status: 200,
        headers: {'Set-Cookie': serialized}
    })
}
