import {cookies} from 'next/headers'
import { verify } from 'jsonwebtoken'
import { NextResponse } from 'next/server'

export const GET = async() => {
    const cookieStore = cookies()
    
    const token = cookieStore.get('token')
    
    if(!token){
        return NextResponse.json({
            messsage: 'Unauthorized'
        },
        {
            status: 401
        }

        )
    }

    const {value} = token

    const secret = process.env.JWT_SECRET || ''

    try{
        var decode = verify(value, secret)
        console.log("verify: ", decode)

        const response = {
            user: 'Super top secret user'
        }

        return new Response(JSON.stringify(response), {
            status: 200
        })
    } catch (e){
        return NextResponse.json({
            message: "Something went wrong"
        }, {
            status: 400
        })
    }
}