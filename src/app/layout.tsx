
import './globals.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import type { Metadata } from 'next/types';
import { Inter } from 'next/font/google';
import React from 'react';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
    title: 'TravelBoo',
    description: 'TravelBoo - Where Adventure Begins',
};



const RootLayout = ({ children }: { children: React.ReactNode }) => {



    return (
        <html>
            <body className={inter?.className}>

                {children}

            </body>
        </html>
    )
}

export default RootLayout