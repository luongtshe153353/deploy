'use client'
import React, { useState } from 'react';

type PermissionContextType = {
    permission: string;
    setPermission: (permission: string) => void;
};

export const PermissionContext = React.createContext<PermissionContextType>({
    permission: '',
    setPermission: () => { }
});

export const PermissionProvider = ({ children }: { children: React.ReactNode }): React.ReactNode => {
    const [permission, setPermission] = useState('');

    console.log("permisionRoot: ", permission)
    return (
        <PermissionContext.Provider value={{ permission, setPermission }}>
            {children}
        </PermissionContext.Provider>
    );
};

export default PermissionProvider;