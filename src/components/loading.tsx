import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const useLoading = () => {
    const GeneralLoading = (): React.JSX.Element => {
        return (
            <div className="w-100 h-100 d-flex flex-row justify-content-center align-items-center">
                <Spin indicator={antIcon} size="large" style={{ color: '#1677ff' }} />
            </div>
        );
    };

    return {
        GeneralLoading,
    };
};

export default useLoading;
