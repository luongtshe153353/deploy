import { getCookie } from 'cookies-next';
import { kebabCase } from 'lodash';
import { NextApiResponse } from 'next';
import { redirect } from 'next/dist/server/api-utils';
export class Repository {
    private basePath?: string = process.env.BASEPATH;
    private addressApi?: string = 'api/';

    constructor() {}

    get BasePath() {
        return this.basePath;
    }

    set BasePath(value: string | undefined) {
        if (this.basePath) {
            this.basePath += value;
        } else {
            this.basePath = value;
        }
    }

    async loginPost(url: any, data: any) {
        try {
            const response = await fetch(this.BasePath + url, {
                method: 'POST',
                cache: 'no-cache',
                // credentials: "same-origin",
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    Connection: 'keep-alive',
                },
                body: JSON.stringify(data),
            });

            if (response.ok) {
                // Handle successful response
                const data = await response.json();
                return data;
            } else {
                // Handle error response
                return response.status;
                // throw new Error('Request failed with status ' + response.status);
            }
        } catch (error: any) {
            console.error('Error:', error.message);
        }
    }

    async post(url: any, data: any) {
        try {
            const response = await fetch(this.BasePath + url, {
                method: 'POST',
                cache: 'no-cache',
                // credentials: "same-origin",
                headers: {
                    Authorization: getCookie('token') || '',
                    Accept: '*/*',
                    'Content-Type': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    Connection: 'keep-alive',
                },
                body: JSON.stringify(data),
            });

            if (response.ok) {
                // Handle successful response
                const data = await response.json();
                return data;
            } else if (response.status === 403) {
                redirect({} as NextApiResponse, 302, '/login');
            } else {
                // Handle error response
                return response.status;
                // throw new Error('Request failed with status ' + response.status);
            }
        } catch (error: any) {
            console.error('Error:', error.message);
        }
    }

    async postFormData(url: any, data: any) {
        try {
            const response = await fetch(this.BasePath + url, {
                method: 'POST',
                cache: 'no-cache',
                // credentials: "same-origin",
                headers: {
                    Authorization: getCookie('token') || '',
                    Accept: '*/*',
                    //'Content-Type': 'multipart/form-data',
                    'Accept-Encoding': 'gzip, deflate, br',
                    Connection: 'keep-alive',
                },
                body: data,
            });

            if (response.ok) {
                // Handle successful response
                const data = await response.json();
                return data;
            } else if (response.status === 403) {
                redirect({} as NextApiResponse, 302, '/login');
            } else {
                // Handle error response
                return response.status;
                // throw new Error('Request failed with status ' + response.status);
            }
        } catch (error: any) {
            console.error('Error:', error.message);
        }
    }

    async get(url: any) {
        try {
            const response = await fetch(this.BasePath + url, {
                method: 'GET',
                cache: 'no-cache',
                // credentials: "same-origin",
                headers: {
                    Accept: '*/*',
                    Authorization: getCookie('token') || '',
                    'Content-Type': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    Connection: 'keep-alive',
                },
            });

            if (response.ok) {
                // Handle successful response
                const data = await response.json();
                return data;
            } else {
                // Handle error response
                return response.status;
                // throw new Error('Request failed with status ' + response.status);
            }
        } catch (error: any) {
            console.error('Error:', error.message);
        }
    }

    async getWithoutAuthentication(url: any) {
        try {
            const response = await fetch(this.BasePath + url, {
                method: 'GET',
                cache: 'no-cache',
                // credentials: "same-origin",
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    Connection: 'keep-alive',
                },
            });

            if (response.ok) {
                // Handle successful response
                const data = await response.json();
                return data;
            } else {
                // Handle error response
                return response.status;
                // throw new Error('Request failed with status ' + response.status);
            }
        } catch (error: any) {
            console.error('Error:', error.message);
        }
    }

    async listProvince() {
        return await this.getWithoutAuthentication(this.addressApi + kebabCase('listProvince'));
    }

    async listDistrict(provinceId: number) {
        return await this.getWithoutAuthentication(
            this.addressApi + kebabCase('listDistrict') + '?provinceId=' + provinceId,
        );
    }

    async listWard(districtId: number) {
        return await this.getWithoutAuthentication(
            this.addressApi + kebabCase('listWard') + '?districtId=' + districtId,
        );
    }

    async getProvinceById(provinceId: number | null) {
        return await this.getWithoutAuthentication(this.addressApi + 'get-province/' + provinceId);
    }

    async getDistrictById(districtId: number | null) {
        return await this.getWithoutAuthentication(this.addressApi + 'get-district/' + districtId);
    }

    async getWardById(wardId: number | null) {
        return await this.getWithoutAuthentication(this.addressApi + 'get-ward/' + wardId);
    }
}
