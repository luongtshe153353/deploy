
export class Province {
    public id?: number           
    public province_name?: string             
    public region_id?: number 
}

export class District{
    public id?: number           
    public district_name?: string             
    public province_id?: number 
}


export class Ward{
    public id?: number           
    public ward_name?: string             
    public district_id?: number 
}

