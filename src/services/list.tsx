'use client'
import React from 'react'
import { Observable } from 'rxjs'

interface useListProps {
    payload?: any,
    setPayload?: (payload?: any) => void,
    api?: (payload?: any) => Promise<any>
}

const useList = (props: useListProps) => {

    const { payload, api } = props

    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const [resp, setResp] = React.useState<any>({})

    React.useEffect(() => {
        const sub = new Observable((subscribe) => {
            subscribe.next(async () => await api!(payload))
        }).subscribe({
            next: (value: any) => {
                setIsLoading(true)
                setResp(value)
            },
            complete: () => {
                setIsLoading(false)
            }
        })

        sub.unsubscribe()

    }, [api, payload])

    console.log(isLoading)
    return {
        resp,
        isLoading
    }
}

export default useList