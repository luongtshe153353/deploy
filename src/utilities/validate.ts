import { Rule } from 'antd/es/form';
import { rejects } from 'assert';
import { useTranslations } from 'next-intl';

const useValidate = () => {
    const translate = useTranslations('Validate');

    const firstNameRule: Rule[] = [
        {
            required: true,
            message: translate('firstName.required'),
        },
        {
            max: 50,
            message: translate('firstName.max'),
        },
    ];

    const lastNameRule: Rule[] = [
        {
            required: true,
            message: translate('lastName.required'),
        },
        {
            max: 50,
            message: translate('lastName.max'),
        },
    ];

    const genderRule: Rule[] = [
        {
            required: true,
            message: translate('gender.required'),
        },
    ];

    const dobRule: Rule[] = [
        {
            required: true,
            message: translate('dob.required'),
        },
        {
            validator(rule, value) {
                return new Promise((resolve, reject) => {
                    const dateFormatPattern = /^\d{4}-\d{2}-\d{2}$/;
                    const inputDate = new Date(value);
                    const currentDate = new Date();

                    // Convert inputDate to a string in "yyyy-mm-dd" format
                    const dateString = inputDate.toISOString().split('T')[0];

                    if (dateFormatPattern.test(dateString) && inputDate < new Date()) {
                        resolve(dateString);
                    } else if (!dateFormatPattern.test(dateString) && inputDate < new Date()) {
                        reject(translate('dob.invalidFormat'));
                    } else if (inputDate > currentDate) {
                        reject(translate('dob.outRange'));
                    }
                });
            },
        },
    ];

    const userNameRule: Rule[] = [
        {
            required: true,
            message: translate('userName.required'),
        },
        {
            pattern: /^[a-zA-Z0-9_]*$/,
            message: translate('userName.specCharacter'),
        },
    ];

    const emailRule: Rule[] = [
        {
            required: true,
            message: translate('email.required'),
        },
        {
            pattern: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
            message: translate('email.invalidFormat'),
        },
    ];

    const roleRule: Rule[] = [
        {
            required: true,
            message: translate('role.required'),
        },
    ];

    const phoneNumberRule: Rule[] = [
        {
            required: true,
            message: translate('phoneNumber.required'),
        },
        {
            max: 10,
            message: translate('phoneNumber.max'),
        },
        {
            pattern: /^\d+$/,
            message: translate('phoneNumber.invalidFormat'),
        },
    ];

    const addressRule: Rule[] = [
        {
            required: true,
            message: translate('address.required'),
        },
    ];

    const provinceRule: Rule[] = [
        {
            required: true,
            message: translate('province.required'),
        },
        {
            type: 'number',
        },
    ];

    const districtRule: Rule[] = [
        {
            required: true,
            message: translate('district.required'),
        },
        {
            type: 'number',
        },
    ];

    const wardRule: Rule[] = [
        {
            required: true,
            message: translate('ward.required'),
        },
        {
            type: 'number',
        },
    ];

    const hotelNameRule: Rule[] = [
        {
            required: true,
            message: translate('hotelName.required'),
        },
        {
            max: 100,
            message: translate('hotelName.max'),
        },
    ];

    const starRatingRule: Rule[] = [
        {
            required: true,
            message: translate('starRating.required'),
        },
    ];

    const boRule: Rule[] = [
        {
            required: true,
            message: translate('bo.required'),
        },
    ];

    const taxCodeRule: Rule[] = [
        {
            required: true,
            message: translate('taxCode.required'),
        },
        {
            max: 10,
            message: translate('taxCode.max'),
        },
        {
            pattern: /^\d+$/,
            message: translate('taxCode.specCharacter'),
        },
    ];

    const descriptionRule: Rule[] = [
        {
            required: true,
            message: translate('description.required'),
        },
    ];

    const codeRule: Rule[] = [
        {
            required: true,
            message: translate('code.required'),
        },
        {
            pattern: /^[a-zA-Z0-9_]*$/,
            message: translate('code.specCharacter'),
        },
    ];

    const servicesRule: Rule[] = [
        {
            required: true,
            message: translate('services.required'),
        },
    ];

    const imagesRule: Rule[] = [
        {
            required: true,
            message: translate('images.required'),
        },
        {
            min: 3,
            message: translate('images.min')
        }
    ];

    return {
        firstNameRule,
        lastNameRule,
        genderRule,
        dobRule,
        userNameRule,
        emailRule,
        roleRule,
        phoneNumberRule,
        addressRule,
        provinceRule,
        districtRule,
        wardRule,
        hotelNameRule,
        starRatingRule,
        boRule,
        taxCodeRule,
        descriptionRule,
        codeRule,
        servicesRule,
        imagesRule,
    };
};

export default useValidate;
